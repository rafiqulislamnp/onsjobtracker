﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.MenuDetails;

namespace ONSJobTracker.Controllers
{
    [SessionCheck]
    public class UserMenuController : Controller
    {
        #region Master Menu

        public async Task<ActionResult> MenuMasterIndex()
        {
            using (var db = new xOssContext())
            {
                ViewBag.allmastermenu = db.MenuMaster.OrderBy(x => x.ModuleSl).ToList();
                return await Task.Run(() => View());
            }
        }
        [HttpPost]
        public async Task<ActionResult> CreateMenuMaster(MenuMaster model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.MenuMaster.Any(o => o.ModuleName.ToLower() == model.ModuleName.ToLower()))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Master Menu already exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.MenuMaster.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Insert Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.MenuMaster.Any(x => x.ModuleName == model.ModuleName && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Master Menu already exists.";
                        }
                        else
                        {
                            MenuMaster selectSingle = db.MenuMaster.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "MenuMaster", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("MenuMasterIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("MenuMasterIndex"));
        }

        public async Task<ActionResult> DeleteMenuMaster(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    MenuMaster selectSingle = db.MenuMaster.Find(id);
                    if (selectSingle != null)
                    {
                        db.MenuMaster.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "MenuMaster", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("MenuMasterIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("MenuMasterIndex");
        }


        #endregion

        #region Details Menu

        public async Task<ActionResult> MenuDetailsIndex()
        {
            ViewBag.allmastermenu= new SelectList(DropDownListHelper.GetMasterMenu(), "Value", "Text");
            ViewBag.allmenutype= new SelectList(DropDownListHelper.GetMenuTypes(), "Value", "Text");
            MenuDetailsViewModel model = new MenuDetailsViewModel();
            ViewBag.allmenudetails = model.GetAllUserMenuDetails();
            return await Task.Run(() => View());
        }
        [HttpPost]
        public async Task<ActionResult> CreateMenuDetails(MenuDetails model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.MenuDetails.Any(o => o.MenuName.ToLower() == model.MenuName.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Details Menu already exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.MenuDetails.Add(model);
                            db.SaveChanges();
                            UserManager manager=new UserManager();
                            manager.AssignRoleForMenuDetails(model.Id,model.MenuMasterFk);
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Insert Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.MenuDetails.Any(x => x.MenuName == model.MenuName && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Details Menu already exists.";
                        }
                        else
                        {
                            MenuDetails selectSingle = db.MenuDetails.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "MenuDetails", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("MenuDetailsIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("MenuMasterIndex"));
        }

        public ActionResult DeleteMenuDetails(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    MenuDetails selectSingle = db.MenuDetails.Find(id);
                    if (selectSingle != null)
                    {
                        db.MenuDetails.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "MenuDetails", "Delete");
                        db.SaveChanges();
                        UserManager manager=new UserManager();
                        manager.DeleteAssignedRoleForMenuDetails(id);
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("MenuDetailsIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("MenuMasterIndex");
        }
        #endregion

        #region Role Assign

        public async Task<ActionResult> MenuRoleIndex()
        {
            ViewBag.alluser = new SelectList(DropDownListHelper.GetUsers(), "Value", "Text");
            MenuDetailsViewModel menuDetails=new MenuDetailsViewModel();
            var model = menuDetails.GetAllUserMenuDetails();
            return await Task.Run(() => View(model));      
        }


        #endregion
    }
}