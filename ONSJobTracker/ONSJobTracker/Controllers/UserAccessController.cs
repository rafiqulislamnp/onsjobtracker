﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.UserEmployee;

namespace ONSJobTracker.Controllers
{
    [SessionCheck]
    public class UserAccessController : Controller
    {
        private xOssContext _db = new xOssContext();
        #region Employee Designation

        public async Task<ActionResult> EmployeeDesignationIndex()
        {
            using (var db = new xOssContext())
            {
                ViewBag.alldesignation = db.EmployeeDesignation.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateEmployeeDesignation(EmployeeDesignation model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.EmployeeDesignation.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Employee Designation exists.";
                            return RedirectToAction("EmployeeDesignationIndex");
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.EmployeeDesignation.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.EmployeeDesignation.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Employee Designation exists.";
                        }
                        else
                        {
                            EmployeeDesignation selectSingle = db.EmployeeDesignation.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                    "EmployeeDesignation", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("EmployeeDesignationIndex");
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("EmployeeDesignationIndex"));
        }

        public async Task<ActionResult> DeleteEmployeeDesignation(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    EmployeeDesignation selectSingle = db.EmployeeDesignation.Find(id);
                    if (selectSingle != null)
                    {
                        db.EmployeeDesignation.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "EmployeeDesignation", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("EmployeeDesignationIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("EmployeeDesignationIndex");
        }

        #endregion

        #region Employee Department

        public async Task<ActionResult> EmployeeDepartmentIndex()
        {
            using (var db = new xOssContext())
            {
                ViewBag.alldepartment = db.EmployeeDepartment.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateEmployeeDepartment(EmployeeDepartment model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.EmployeeDepartment.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Employee Designation exists.";
                            return RedirectToAction("CreateEmployeeDesignation");
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.EmployeeDepartment.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.EmployeeDepartment.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Employee Designation exists.";
                        }
                        else
                        {
                            EmployeeDepartment selectSingle =
                                 db.EmployeeDepartment.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                    "EmployeeDepartment", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("EmployeeDepartmentIndex");
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("EmployeeDepartmentIndex"));
        }

        public async Task<ActionResult> DeleteEmployeeDepartment(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    EmployeeDepartment selectSingle = db.EmployeeDepartment.Find(id);
                    if (selectSingle != null)
                    {
                        db.EmployeeDepartment.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "EmployeeDesignation", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("EmployeeDepartmentIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("EmployeeDepartmentIndex");
        }

        #endregion

        #region Employee

        public async Task<ActionResult> EmployeeIndex()
        {
            ViewBag.alldesignation = new SelectList(DropDownListHelper.GetEmployeeDesignation(), "Value", "Text");
            ViewBag.alldepartment = new SelectList(DropDownListHelper.GetEmployeeDepartment(), "Value", "Text");
            return await Task.Run(() => View());
        }

        [HttpPost]
        public async Task<ActionResult> CreateEmployee(EmployeeInfo model, HttpPostedFileBase files)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    string img = "No_Image.png";

                    if (model.Action == "Save")
                    {
                        if (db.EmployeeInfo.Any(o => o.Email.ToLower() == model.Email.ToLower() || o.CId == model.CId))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Email or Employee ID already exists.";
                            return RedirectToAction("EmployeeIndex");
                        }
                        if (files != null && files.ContentLength > 0)
                        {
                            string fileExtension = Path.GetExtension(files.FileName)?.ToLower();
                            if (fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".jpeg")
                            {
                                //imagefile.FileName;
                                img = model.CId + fileExtension;
                                string fileLocation = Server.MapPath("/OnsJobTracker/Content/Image/") + img;
                                if (System.IO.File.Exists(fileLocation))
                                {
                                    System.IO.File.Delete(fileLocation);
                                }
                                files.SaveAs(fileLocation);
                            }
                            else
                            {
                                Session["warning_div"] = "true";
                                Session["warning_msg"] = "Please upload only Image type file!";
                            }
                        }
                        model.ImageLink = img;
                        model.AddReady(true, DateTime.Now);
                        db.EmployeeInfo.Add(model);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Inserst Successfully.";
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.EmployeeInfo.Any(o =>
                            (o.Email.ToLower() == model.Email.ToLower() || o.CId == model.CId) && o.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Email or Employee ID already exists.";
                            return RedirectToAction("EmployeeList");
                        }

                        EmployeeInfo selectSingle = db.EmployeeInfo.FirstOrDefault(x => x.Id == model.Id);
                        if (selectSingle != null)
                        {
                            if (files != null && files.ContentLength > 0)
                            {
                                string fileExtension = Path.GetExtension(files.FileName)?.ToLower();
                                if (fileExtension == ".jpg" || fileExtension == ".png" || fileExtension == ".jpeg")
                                {
                                    //imagefile.FileName;
                                    img = model.CId + fileExtension;
                                    string fileLocation = Server.MapPath("/OnsJobTracker/Content/Image/") + img;
                                    if (System.IO.File.Exists(fileLocation))
                                    {
                                        System.IO.File.Delete(fileLocation);
                                    }
                                    files.SaveAs(fileLocation);
                                    model.ImageLink = img;
                                }
                                else
                                {
                                    Session["warning_div"] = "true";
                                    Session["warning_msg"] = "Please upload only Image type file!";
                                }
                            }
                            else
                            {
                                model.ImageLink = selectSingle.ImageLink;
                            }
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);


                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                "EmployeeInfo", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Update Successfully.";
                            return RedirectToAction("EmployeeList");
                        }
                    }
                    return RedirectToAction("EmployeeIndex");
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People."+e.Message+"//"+e.InnerException;
            }
            return RedirectToAction("EmployeeIndex");
        }

        public async Task<ActionResult> EmployeeList()
        {
            try
            {
                var model = (from t1 in _db.EmployeeInfo
                             join t2 in _db.EmployeeDepartment on t1.EmployeeDepartmentFk equals t2.Id
                             join t3 in _db.EmployeeDesignation on t1.EmployeeDesignationFk equals t3.Id
                             select new EmployeeViewModel
                             {
                                 Id = t1.Id,
                                 Name = t1.Name,
                                 CId = t1.CId,
                                 Mobile = t1.Mobile,
                                 Email = t1.Email,
                                 Address = t1.Address,
                                 ImageLink = t1.ImageLink,
                                 EmployeeDepartment = t2.Name,
                                 EmployeeDesignation = t3.Name,
                                 Status = t1.Status
                             }).OrderBy(x => x.Status?0:1).ThenByDescending(x=>x.Id).AsEnumerable();
                return await Task.Run(() => View(model));
            }

            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
                return RedirectToAction("EmployeeList");
            }
        }

        public async Task<ActionResult> UpdateEmployee(int id)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                try
                {
                    ViewBag.alldesignation = new SelectList(DropDownListHelper.GetEmployeeDesignation(), "Value", "Text");
                    ViewBag.alldepartment = new SelectList(DropDownListHelper.GetEmployeeDepartment(), "Value", "Text");
                    var model = db.EmployeeInfo.FirstOrDefault(x => x.Id == id);
                    return await Task.Run(() => View(model));
                }
                catch (Exception e)
                {
                    //sweet alert
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";

                }

            }
            return await Task.Run(() => View());
        }

        public async Task<ActionResult> DeleteEmployee(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    EmployeeInfo selectSingle = db.EmployeeInfo.Find(id);
                    if (selectSingle != null)
                    {
                        db.EmployeeInfo.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "EmployeeInfo", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("EmployeeList");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("EmployeeList");
        }


        public async Task<ActionResult> UpdateEmployeeProfile(int id)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                //var userId = Convert.ToInt64(EncryptDecrypt.Decrypt(id));
                try
                {
                    ViewBag.EmployeeProfile = (from u in db.User
                                               join em in db.EmployeeInfo on u.EmployeeInfoFk equals em.Id
                                               join ds in db.EmployeeDesignation on em.EmployeeDesignationFk equals ds.Id
                                               join dp in db.EmployeeDepartment on em.EmployeeDepartmentFk equals dp.Id
                                               where u.Id == id
                                               select new EmployeeViewModel
                                               {
                                                   userId = u.Id,
                                                   LoginId = u.LoginId,
                                                   Id = id,
                                                   Name = em.Name,
                                                   CId = em.CId,
                                                   Mobile = em.Mobile,
                                                   Address = em.Address,
                                                   Email = em.Email,
                                                   ImageLink = em.ImageLink,
                                                   EmployeeDesignation = ds.Name,
                                                   EmployeeDepartment = dp.Name
                                               }).FirstOrDefault();
                    return await Task.Run(() => View());
                }
                catch (Exception e)
                {
                    //sweet alert
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";

                }

            }
            return await Task.Run(() => View());
        }

        [HttpPost]
        public async Task<ActionResult> UpdateEmployeeProfile(User model)
        {
            using (var db = new xOssContext())
            {
                User selectSingle = db.User.FirstOrDefault(x => x.Id == model.Id);
                // to remove  same key already exists in the ObjectStateManager
                ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);

                if (selectSingle != null)
                {
                    model.Password = EncryptDecrypt.Encrypt(model.Password);
                    model.ConfirmPassword = EncryptDecrypt.Encrypt(model.ConfirmPassword);
                    //model.LoginId = Session["L"];

                    model.EmployeeInfoFk = selectSingle.EmployeeInfoFk;
                    model.UserType = selectSingle.UserType;
                    model.InsertBy = selectSingle.InsertBy;
                    model.InsertDate = selectSingle.InsertDate;

                    model.UpdateReady(true, DateTime.Now);
                    SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                        "User", "Update");
                    db.Entry(model).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    //sweet alert
                    Session["success_div"] = "true";
                    Session["success_msg"] = " Password Updated Successfully.";
                }
            }
            return RedirectToAction("UpdateEmployeeProfile");
        }

        public async Task<ActionResult> ResignEmployee(int id)
        {
            try
            {
                using (var db = new xOssContext())
                {
                    var model = db.EmployeeInfo.Single(x => x.Id == id);
                    model.UpdateReady(false, DateTime.Now);
                    SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(model).Data),
                        "EmployeeInfo", "Update");
                    db.Entry(model).Property(r => r.Status).IsModified = true;
                    await db.SaveChangesAsync();
                }
                Session["success_div"] = "true";
                Session["success_msg"] = "Employee Status Updated Successfully.";
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }

            return RedirectToAction("EmployeeList");
        }
        #endregion

        #region User

        public async Task<ActionResult> UserIndex()
        {
            ViewBag.allusertype = new SelectList(DropDownListHelper.GetUserTypes(), "Value", "Text");
            ViewBag.allemployee = new SelectList(DropDownListHelper.GetAllEmployee(), "Value", "Text");
            return await Task.Run(() => View());
        }
        public ActionResult CreateUser(User model, UserManager manager)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.User.Any(o => o.LoginId.ToLower() == model.LoginId.ToLower() || o.EmployeeInfoFk == model.EmployeeInfoFk))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "User alreday exists.";
                            return RedirectToAction("UserIndex");
                        }
                        model.Password = EncryptDecrypt.Encrypt(model.Password);
                        model.ConfirmPassword = EncryptDecrypt.Encrypt(model.ConfirmPassword);
                        model.AddReady(true, DateTime.Now);
                        db.User.Add(model);

                        db.SaveChanges();

                        var userId = model.Id;

                        // Assign Mneu to New User
                        manager.AssignMenuForNewUser(userId);
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Inserst Successfully.";
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.User.Any(x => x.LoginId == model.LoginId && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "User alreday exists.";
                            return RedirectToAction("UserList");
                        }
                        User selectSingle = db.User.FirstOrDefault(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {
                            model.EmployeeInfoFk = selectSingle.EmployeeInfoFk;
                            model.Password = selectSingle.Password;
                            model.ConfirmPassword = selectSingle.Password;

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                "User", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                            return RedirectToAction("UserList");
                        }
                    }
                }
                return RedirectToAction("UserIndex");
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return View("UserIndex");
        }

        public async Task<ActionResult> UserList()
        {
            try
            {
                var model = (from t1 in _db.User
                             join t2 in _db.EmployeeInfo on t1.EmployeeInfoFk equals t2.Id
                             select new UserViewModel
                             {
                                 Id = t1.Id,
                                 UserType = t1.UserType,
                                 LoginId = t1.LoginId,
                                 Name = t2.Name,
                                 CId = t2.CId,
                                 ImageLink = t2.ImageLink,
                                 Status = t1.Status
                             }).OrderBy(x => x.Status?0:1).ThenByDescending(x=>x.Id).AsEnumerable();
                return await Task.Run(() => View(model));
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
                return RedirectToAction("UserList");
            }
        }

        public async Task<ActionResult> UpdateUser(int id)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                try
                {
                    ViewBag.allusertype = new SelectList(DropDownListHelper.GetUserTypes(), "Value", "Text");
                    ViewBag.allemployee = new SelectList(DropDownListHelper.GetAllEmployee(), "Value", "Text");
                    ViewBag.sidebar = (from t1 in db.User
                                       join t2 in db.EmployeeInfo on t1.EmployeeInfoFk equals t2.Id
                                       join t3 in db.EmployeeDepartment on t2.EmployeeDepartmentFk equals t3.Id
                                       join t4 in db.EmployeeDesignation on t2.EmployeeDesignationFk equals t4.Id
                                       where t1.Id == id
                                       select new EmployeeViewModel
                                       {
                                           Name = t2.Name,
                                           CId = t2.CId,
                                           Email = t2.Email,
                                           Mobile = t2.Mobile,
                                           ImageLink = t2.ImageLink,
                                           EmployeeDepartment = t3.Name,
                                           EmployeeDesignation = t4.Name,
                                       }).FirstOrDefault();
                    var model = db.User.FirstOrDefault(x => x.Id == id);
                    return await Task.Run(() => View(model));
                }
                catch (Exception e)
                {
                    //sweet alert
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
                }

            }
            return await Task.Run(() => View());
        }

        public async Task<ActionResult> DeleteUser(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    User selectSingle =  db.User.Find(id);
                    if (selectSingle != null)
                    {
                        db.User.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "User", "Delete");
                        db.SaveChanges();

                        //delete Assigned Menu
                        UserManager manager = new UserManager();
                        manager.DeleteMenuForNewUser(selectSingle.Id);

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("UserList");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("UserList");
        }

        public async Task<ActionResult> HoldUserAccount(int uid, bool status)
        {
            try
            {
                using (var db = new xOssContext())
                {
                    var model = db.User.Single(x => x.Id == uid);
                    model.ConfirmPassword = model.Password;
                    model.UpdateReady(status, DateTime.Now);
                    SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(model).Data),
                        "User", "Update");
                    db.Entry(model).Property(r => r.Status).IsModified = true;
                    await db.SaveChangesAsync();
                }
                Session["success_div"] = "true";
                Session["success_msg"] = "User Account Status Updated Successfully.";
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }

            return RedirectToAction("UserList");
        }
        
        #endregion


    }
}