﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;

namespace ONSJobTracker.Controllers
{
    [SessionCheck]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return RedirectToAction("Contact");
        }

        public ActionResult Contact()
        {
            Session["success_div"] = "true";
            Session["success_msg"] = "This is contact page.";

            return View();
        }
    }
}