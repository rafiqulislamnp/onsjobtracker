﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.Report;
using System.Data.Entity;
namespace ONSJobTracker.Controllers
{
     //[SessionCheck]
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult PoSummaryReportIndex()

        {
            using (var db = new xOssContext()) // use your DbConext
            {

                //var data = (from t1 in db.PoDetails
                //            join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                //            join t3 in db.Project on t2.ProjectFk equals t3.Id
                //            join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                //            join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                //            join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                //            join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id

                //            join t8 in db.ReportEngineerWorkStatus on t1.Id equals t8.SiteFk into jn
                //            from j in jn.DefaultIfEmpty()
                //            group j.SiteFk by new
                //            {
                //                Customer = t4.Name,
                //                Project = t3.ProjectName,
                //                WorkType = t5.Name,
                //                UnitValue = t1.UnitPrice,
                //                PaymentMilestone=t7.Name,
                //                MilestonePercent=t6.MilestonePercentage,

                //            } into grp
                //            select new
                //            {
                //                grp.Key.Customer,
                //                grp.Key.Project,
                //                grp.Key.WorkType,
                //                grp.Key.UnitValue,
                //                grp.Key.PaymentMilestone,
                //                grp.Key.MilestonePercent,
                //                WaitingAcceptance= grp.Count()
                //                //WaitingAcceptance= grp.Key.WaitingAcceptance.Count(),
                //            }).ToList();







                var main = (from t1 in db.PoDetails
                            join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                            join t3 in db.Project on t2.ProjectFk equals t3.Id
                            join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                            join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                            join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                            join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                            group new { t5 } by new
                            {
                                Customer = t4.Name,
                                Project = t3.ProjectName,
                                WorkType = t5.Name,
                                UnitValue = t1.UnitPrice,
                                PaymentMilestone = t7.Name,
                                MilestonePercent = t6.MilestonePercentage,
                                WorkTypeId = t5.Id
                            } into grp
                            select new SummaryReportVm
                            {
                                Customer = grp.Key.Customer,
                                Project = grp.Key.Project,
                                WorkType = grp.Key.WorkType,
                                UnitValue = grp.Key.UnitValue,
                                PaymentMilestone = grp.Key.PaymentMilestone,
                                MilestonePercent = grp.Key.MilestonePercent,
                                WorkTypeId = grp.Key.WorkTypeId
                            }).ToList();



                //var withoutpo = (from t1 in db.PoDetails
                //                 join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                //                 join t3 in db.Project on t2.ProjectFk equals t3.Id
                //                 join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                //                 join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                //                 join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                //                 join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                //                 join t8 in db.ReportEngineerWorkStatus on t1.Id equals t8.SiteFk
                //                 where t8.IsDelivered == true && t2.PoNo.Contains("No PO")
                //                 group new { t5 } by new
                //                 {
                //                     Customer = t4.Name,
                //                     Project = t3.ProjectName,
                //                     WorkType = t5.Name,
                //                     UnitValue = t1.UnitPrice,
                //                     PaymentMilestone = t7.Name,
                //                     MilestonePercent = t6.MilestonePercentage,
                //                     WorkTypeId = t5.Id
                //                 } into grp
                //                 select new SummaryReportVm
                //                 {
                //                     Customer = grp.Key.Customer,
                //                     Project = grp.Key.Project,
                //                     WorkType = grp.Key.WorkType,
                //                     UnitValue = grp.Key.UnitValue,
                //                     PaymentMilestone = grp.Key.PaymentMilestone,
                //                     MilestonePercent = grp.Key.MilestonePercent,
                //                     WorkTypeId = grp.Key.WorkTypeId,
                //                     WithoutPoQty = grp.Count(),
                //                     WithoutPoPrice = ((grp.Key.UnitValue * grp.Count() * grp.Key.MilestonePercent) / 100),
                //                 }).ToList();



                //var waitingacceptance = (from t1 in db.PoDetails
                //                         join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                //                         join t3 in db.Project on t2.ProjectFk equals t3.Id
                //                         join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                //                         join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                //                         join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                //                         join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                //                         join t8 in db.ReportEngineerWorkStatus on t1.Id equals t8.SiteFk
                //                         where t8.IsReadyForAcceptance == true
                //                         group new { t5 } by new
                //                         {
                //                             Customer = t4.Name,
                //                             Project = t3.ProjectName,
                //                             WorkType = t5.Name,
                //                             UnitValue = t1.UnitPrice,
                //                             PaymentMilestone = t7.Name,
                //                             MilestonePercent = t6.MilestonePercentage,
                //                             WorkTypeId = t5.Id,
                //                         } into grp
                //                         select new 
                //                         {
                //                             grp.Key.Customer,
                //                             grp.Key.Project,
                //                             grp.Key.WorkType,
                //                             grp.Key.UnitValue,
                //                             grp.Key.PaymentMilestone,
                //                             grp.Key.MilestonePercent,
                //                             grp.Key.WorkTypeId,
                //                             WaitingQuantity = grp.Count(),
                //                             WaitingQuantityPrice = ((grp.Key.UnitValue * grp.Count() * grp.Key.MilestonePercent) / 100),
                //                         }).ToList();

                List<SummaryReportVm> report = new List<SummaryReportVm>();
                foreach (var data in main)
                {

                    SummaryReportVm summary = new SummaryReportVm
                    {
                        Customer = data.Customer,
                        Project = data.Project,
                        UnitValue = data.UnitValue,
                        PaymentMilestone = data.PaymentMilestone,
                        MilestonePercent = data.MilestonePercent,
                        WorkType = data.WorkType,
                        WorkTypeId = data.WorkTypeId,
                        WithoutPoQty = (from t1 in db.PoDetails
                                        join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                        join t3 in db.Project on t2.ProjectFk equals t3.Id
                                        join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                        join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                        join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                        join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                        join t8 in db.ReportEngineerWorkStatus on t1.Id equals t8.SiteFk
                                        where t3.ProjectName == data.Project
                                        && t4.Name == data.Customer
                                        && t5.Id == data.WorkTypeId
                                        && t7.Name == data.PaymentMilestone
                                        && t1.UnitPrice == data.UnitValue
                                        && t8.Status == false && t2.PoNo.Contains("No PO")
                                        select new
                                        {
                                            t3.ProjectName
                                        })?.Count() ?? 0,

                        WaitingAcceptanceQty = (from t1 in db.PoDetails
                                                join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                                join t3 in db.Project on t2.ProjectFk equals t3.Id
                                                join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                                join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                                join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                                join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                                join t8 in db.ReportEngineerWorkStatus on t1.Id equals t8.SiteFk
                                                where t3.ProjectName == data.Project
                                                && t4.Name == data.Customer
                                                && t5.Id == data.WorkTypeId
                                                && t7.Name == data.PaymentMilestone
                                                && t1.UnitPrice == data.UnitValue
                                                && t8.IsReadyForAcceptance == true
                                                select new
                                                {
                                                    t3.ProjectName
                                                })?.Count() ?? 0,
                        AcceptedQty = (from t1 in db.PoDetails
                                       join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                       join t3 in db.Project on t2.ProjectFk equals t3.Id
                                       join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                       join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                       join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                       join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                       join t8 in db.AcceptanceStatus on t1.Id equals t8.SiteFk
                                       where t3.ProjectName == data.Project
                                       && t4.Name == data.Customer
                                       && t5.Id == data.WorkTypeId
                                       && t7.Name == data.PaymentMilestone
                                       && t1.UnitPrice == data.UnitValue
                                       select new
                                       {
                                           t3.ProjectName
                                       })?.Count() ?? 0,
                        InvoicedQty = (from t1 in db.PoDetails
                                       join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                       join t3 in db.Project on t2.ProjectFk equals t3.Id
                                       join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                       join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                       join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                       join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                       join t8 in db.InvoiceDetail on t1.Id equals t8.SiteFk
                                       join t9 in db.InvoiceMaster on t8.InvoiceMasterFk equals t9.Id
                                       where t3.ProjectName == data.Project
                                       && t4.Name == data.Customer
                                       && t5.Id == data.WorkTypeId
                                       && t7.Name == data.PaymentMilestone
                                       && t1.UnitPrice == data.UnitValue
                                       && t9.IsPayment == false
                                       select new
                                       {
                                           t3.ProjectName,
                                       })?.Count() ?? 0,
                        InvoicedAmount = (from t1 in db.PoDetails
                                          join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                          join t3 in db.Project on t2.ProjectFk equals t3.Id
                                          join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                          join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                          join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                          join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                          join t8 in db.InvoiceDetail on t1.Id equals t8.SiteFk
                                          join t9 in db.InvoiceMaster on t8.InvoiceMasterFk equals t9.Id
                                          where t3.ProjectName == data.Project
                                          && t4.Name == data.Customer
                                          && t5.Id == data.WorkTypeId
                                          && t7.Name == data.PaymentMilestone
                                          && t1.UnitPrice == data.UnitValue
                                          && t9.IsPayment == false
                                          select new
                                          {
                                              t8.FinalAmount,
                                          }).ToList().Select(x => x.FinalAmount).DefaultIfEmpty(0).Sum(),

                        PaymetRecievedQty = (from t1 in db.PoDetails
                                             join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                             join t3 in db.Project on t2.ProjectFk equals t3.Id
                                             join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                             join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                             join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                             join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                             join t8 in db.InvoiceDetail on t1.Id equals t8.SiteFk
                                             join t9 in db.InvoiceMaster on t8.InvoiceMasterFk equals t9.Id
                                             where t3.ProjectName == data.Project
                                             && t4.Name == data.Customer
                                             && t5.Id == data.WorkTypeId
                                             && t7.Name == data.PaymentMilestone
                                             && t1.UnitPrice == data.UnitValue
                                             && t9.IsPayment == true
                                             select new
                                             {
                                                 t3.ProjectName,
                                             })?.Count() ?? 0,
                        PaidAmount = (from t1 in db.PoDetails
                                      join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                      join t3 in db.Project on t2.ProjectFk equals t3.Id
                                      join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                      join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                      join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                      join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                      join t8 in db.InvoiceDetail on t1.Id equals t8.SiteFk
                                      join t9 in db.InvoiceMaster on t8.InvoiceMasterFk equals t9.Id
                                      where t3.ProjectName == data.Project
                                      && t4.Name == data.Customer
                                      && t5.Id == data.WorkTypeId
                                      && t7.Name == data.PaymentMilestone
                                      && t1.UnitPrice == data.UnitValue
                                      && t9.IsPayment == true
                                      select new
                                      {
                                          t9.RecieveAmount,
                                      }).ToList().Select(x => x.RecieveAmount).DefaultIfEmpty(0).Sum(),
                        NotCompletedQty = (from t1 in db.PoDetails
                                           join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                           join t3 in db.Project on t2.ProjectFk equals t3.Id
                                           join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                                           join t5 in db.WorkType on t1.WorkTypeFk equals t5.Id
                                           join t6 in db.SiteMilestone on t1.Id equals t6.PoDetailsFk
                                           join t7 in db.PayementMilestone on t6.PayementMilestoneFk equals t7.Id
                                           where t3.ProjectName == data.Project
                                           && t4.Name == data.Customer
                                           && t5.Id == data.WorkTypeId
                                           && t7.Name == data.PaymentMilestone
                                           && t1.UnitPrice == data.UnitValue && !(from o in db.WorkUpdate
                                                                                  select o.SiteFk).Contains(t1.Id)
                                           select new
                                           {
                                               t3.ProjectName
                                           })?.Count() ?? 0
                    };
                    report.Add(summary);
                }

                return View(report);
                //ViewBag.WithoutPo = withoutpo;


                //var q3 = (from t1 in db.PoDetails
                //          join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                //          join t3 in db.Project on t2.ProjectFk equals t3.Id
                //          join t4 in db.WorkType on t1.WorkTypeFk equals t4.Id
                //          join t5 in db.ClientInfo on t2.ClientInfoFk equals t5.Id
                //          join t6 in db.ClientInfo on t2.EndClientInfoFk equals t6.Id
                //          join t7 in db.PayementMilestone on t1.PayementMilestoneFk equals t7.Id
                //          join t8 in db.AcceptanceStatus on t1.Id equals t8.SiteFk
                //          where t8.Status
                //          group t7 by new
                //          {
                //              Customer = t5.Name,
                //              Project = t6.Name,
                //              WorkType = t4.Name,
                //              WorkTypeId = t4.Id,
                //              PaymentMilestone = t7.Name,
                //              Percentage = t1.MilestonePercentage,
                //              Price = t1.UnitPrice
                //          }
                //    into grp
                //          select new
                //          {
                //              WorkTypeId = grp.Key.WorkTypeId,
                //              AcceptanceTotal = ((grp.Key.Price * grp.Count() * grp.Key.Percentage) / 100),
                //              AcceptanceQuantity = grp.Count()
                //          }).ToList();
                //var q4 = (from t1 in db.PoDetails
                //          join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                //          join t3 in db.Project on t2.ProjectFk equals t3.Id
                //          join t4 in db.WorkType on t1.WorkTypeFk equals t4.Id
                //          join t5 in db.ClientInfo on t2.ClientInfoFk equals t5.Id
                //          join t6 in db.ClientInfo on t2.EndClientInfoFk equals t6.Id
                //          join t7 in db.PayementMilestone on t1.PayementMilestoneFk equals t7.Id
                //          join t8 in db.InvoiceDetail on t1.Id equals t8.SiteFk
                //          where t8.Status
                //          group t7 by new
                //          {
                //              Customer = t5.Name,
                //              Project = t6.Name,
                //              WorkType = t4.Name,
                //              WorkTypeId = t4.Id,
                //              PaymentMilestone = t7.Name,
                //              Percentage = t1.MilestonePercentage,
                //              Price = t1.UnitPrice
                //          }
                //    into grp
                //          select new
                //          {
                //              WorkTypeId = grp.Key.WorkTypeId,
                //              InvoiceTotal = ((grp.Key.Price * grp.Count() * grp.Key.Percentage) / 100),
                //              InvoiceQuantity = grp.Count()
                //          }).ToList();

                //var q5 = (from t1 in db.PoDetails
                //          join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                //          join t3 in db.Project on t2.ProjectFk equals t3.Id
                //          join t4 in db.WorkType on t1.WorkTypeFk equals t4.Id
                //          join t5 in db.ClientInfo on t2.ClientInfoFk equals t5.Id
                //          join t6 in db.ClientInfo on t2.EndClientInfoFk equals t6.Id
                //          join t7 in db.PayementMilestone on t1.PayementMilestoneFk equals t7.Id
                //          join t8 in db.AcceptanceStatus on t1.Id equals t8.SiteFk
                //          where t8.Status
                //          group t7 by new
                //          {
                //              Customer = t5.Name,
                //              Project = t6.Name,
                //              WorkType = t4.Name,
                //              WorkTypeId = t4.Id,
                //              PaymentMilestone = t7.Name,
                //              Percentage = t1.MilestonePercentage,
                //              Price = t1.UnitPrice
                //          }
                //    into grp
                //          select new
                //          {
                //              WorkTypeId = grp.Key.WorkTypeId,
                //              InvoiceTotal = ((grp.Key.Price * grp.Count() * grp.Key.Percentage) / 100),
                //              InvoiceQuantity = grp.Count()
                //          }).ToList();
            }
        }

        public ActionResult ExpenseClaimPayment()
        {
            using (var db = new xOssContext())
            {
                var data = (from t1 in db.EmployeeInfo
                            join t2 in db.EmployeeDesignation on t1.EmployeeDesignationFk equals t2.Id
                            where t1.EmployeeDepartmentFk == 3 && t1.Status == true
                            select new ExpensePaymentVm
                            {
                                Employee = t1.Name + " (" + t1.CId + " )",
                                EmployeeId = t1.Id,
                                Expense = db.DailyCost.Where(x => x.EmployeeFk == t1.Id).Select(x => x.Amount).DefaultIfEmpty(0).Sum(),
                                PaidAmount = db.Payment.Where(x => x.EmployeeInfoFk == t1.Id).Select(x => x.PaidAmount).DefaultIfEmpty(0).Sum()
                            }).ToList();

                return View(data);
            }
        }

        public ActionResult ExpenseDetailsByEmployee(Int64 employeeId)
        {
            using (var db = new xOssContext())
            {
                var data = (from t1 in db.DailyCost
                            where t1.EmployeeFk == employeeId
                            group t1 by DbFunctions.TruncateTime(t1.InsertDate) into grp

                            select new EmployeeExpense
                            {
                                ExpenseDate = grp.Key,
                                ExpenseDetails = (from t2 in db.DailyCost
                                                  join t3 in db.WorkUpdate on t2.WorkUpdateFk equals t3.Id
                                                  join t4 in db.PoDetails on t3.SiteFk equals t4.Id
                                                  join t5 in db.PoMaster on t4.PoMasterFk equals t5.Id
                                                  join t6 in db.Project on t5.ProjectFk equals t6.Id
                                                  where DbFunctions.TruncateTime(t2.InsertDate) == grp.Key && t2.EmployeeFk == employeeId
                                                  select new ExpenseDetails
                                                  {
                                                      CostTitle = t2.CostTitle,
                                                      Expense = t2.Amount,
                                                      ProjectName = t6.ProjectName,
                                                      PoNo = t5.PoNo,
                                                      SiteName = t4.Name
                                                  }).ToList()
                            }).OrderByDescending(x => x.ExpenseDate).ToList();

                var employeedetails = db.EmployeeInfo.FirstOrDefault(x => x.Id == employeeId);
                ViewBag.employee = employeedetails;
                return View(data);
            }
        }

        public ActionResult PaymentRecievedByEmployee(Int64 employeeId)
        {
            using (var db = new xOssContext())
            {
                var data = db.Payment.Where(x => x.EmployeeInfoFk == employeeId).ToList();
                var employeedetails = db.EmployeeInfo.FirstOrDefault(x => x.Id == employeeId);
                ViewBag.employee = employeedetails;
                return View(data);
            }
        }

        [HttpGet]
        public ActionResult CarStatus()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CarStatus(DateTime StartDate, DateTime FinishDate)
        {

            using (var db = new xOssContext())
            {
                var data = (from t1 in db.WorkUpdate
                            join t2 in db.Transport on t1.PlanningMasterFk equals t2.PlanningMasterFk
                            join t3 in db.CarInfo on t2.CarFk equals t3.Id
                            join t4 in db.VendorInfo on t3.VendorInfoFk equals t4.Id
                            where t1.Date >= StartDate && t1.Date <= FinishDate
                            select new
                            {
                                t4.Name,
                                t3.CarNo,
                                CarId = t3.Id,
                                Milage = t1.EndMileage - t1.StartMileage
                            }).ToList();
                var carstatus = (from t1 in data
                                 group t1 by new { t1.CarNo, t1.Name, t1.CarId }
                              into grp
                                 select new CarStatusReport
                                 {
                                     VendorName = grp.Key.Name,
                                     CarNo = grp.Key.CarNo,
                                     CarId = grp.Key.CarId,
                                     Milage = grp.Sum(x => x.Milage)
                                 }).ToList();
                ViewBag.CarStatuses = carstatus;
                ViewBag.StartDate = StartDate;
                ViewBag.FinishDate = FinishDate;
                return View();
            }
        }

        public ActionResult SpecificCarInformation(Int64 carid, DateTime startdate, DateTime finishdate, string carno)
        {
            using (var db = new xOssContext())
            {
                var data = (from t1 in db.WorkUpdate
                            join t2 in db.Transport on t1.PlanningMasterFk equals t2.PlanningMasterFk
                            join t3 in db.CarInfo on t2.CarFk equals t3.Id
                            join t4 in db.VendorInfo on t3.VendorInfoFk equals t4.Id
                            join t5 in db.PoDetails on t1.SiteFk equals t5.Id
                            join t6 in db.PoMaster on t5.PoMasterFk equals t6.Id
                            join t7 in db.Project on t6.ProjectFk equals t7.Id
                            where t2.CarFk == carid && t1.Date >= startdate && t1.Date <= finishdate
                            select new SpecificCarInfo
                            {
                                Project = t7.ProjectName,
                                Milage = t1.EndMileage - t1.StartMileage,
                                Location = t1.Location
                            }).ToList();

                ViewBag.CarStatuses = data;
                ViewBag.StartDate = startdate;
                ViewBag.FinishDate = finishdate;
                ViewBag.CarNo = carno;
                return View();
            }
        }
        public ActionResult SpecificCarMilageInformation(Int64 carid, DateTime startdate, DateTime finishdate, string carno)
        {
            using (var db = new xOssContext())
            {
                var data = (from t1 in db.WorkUpdate
                            join t2 in db.Transport on t1.PlanningMasterFk equals t2.PlanningMasterFk
                            join t3 in db.CarInfo on t2.CarFk equals t3.Id
                            join t4 in db.VendorInfo on t3.VendorInfoFk equals t4.Id
                            join t5 in db.PoDetails on t1.SiteFk equals t5.Id
                            join t6 in db.PoMaster on t5.PoMasterFk equals t6.Id
                            join t7 in db.Project on t6.ProjectFk equals t7.Id
                            where t2.CarFk == carid && t1.Date >= startdate && t1.Date <= finishdate
                            select new SpecificCarMilageInfo
                            {
                                Project = t7.ProjectName,
                                Milage = t1.EndMileage - t1.StartMileage,
                                Location = t1.Location,
                                Date = t1.Date,
                                Engineer = (from t8 in db.WorkTeamMember
                                            join t9 in db.EmployeeInfo on t8.EmployeeFk equals t9.Id
                                            where t8.WorkUpdateFk == t1.Id
                                            select t9.Name + " (" + t9.CId + ")").ToList()
                            }).ToList();

                ViewBag.CarStatuses = data;
                ViewBag.StartDate = startdate;
                ViewBag.FinishDate = finishdate;
                ViewBag.CarNo = carno;
                return View();
            }

        }

        public ActionResult OverallWorkStatus()
        {
            return View();
        }
        [HttpPost]
        public ActionResult OverallWorkStatus(DateTime StartDate, DateTime FinishDate)
        {
            using (var db = new xOssContext())
            {
                var master = (from t1 in db.WorkUpdate
                              join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                              join t4 in db.Cluster on t2.ClusterFk equals t4.Id
                              join t5 in db.WorkType on t2.WorkTypeFk equals t5.Id
                              join t6 in db.ClientInfo on t3.ClientInfoFk equals t6.Id
                              join t7 in db.ClientInfo on t3.EndClientInfoFk equals t7.Id
                              where t1.Date >= StartDate && t1.Date <= FinishDate
                              select new
                              {
                                  PoNo = t3.PoNo,
                                  WorkType = t5.Name,
                                  SiteId = t2.Id,
                                  SiteCId = t2.Cid,
                                  SiteName = t2.Name,
                                  ClusterId = t4.Name,
                                  ClientName = t6.Name,
                                  EndClientName = t7.Name,

                              }).Distinct().ToList();

                List<OverallPMWorkStatus> report = new List<OverallPMWorkStatus>();
                foreach (var data in master)
                {
                    var overallPMWorkStatus = new OverallPMWorkStatus
                    {
                        PoNo = data.PoNo,
                        WorkType = data.WorkType,
                        SiteId = data.SiteId,
                        SiteCId = data.SiteCId,
                        SiteName = data.SiteName,
                        ClusterId = data.ClusterId,
                        ClientName = data.ClientName,
                        EndClientName = data.EndClientName,
                        FieldEngineer = (from t1 in db.WorkTeamMember
                                         join t2 in db.WorkUpdate on t1.WorkUpdateFk equals t2.Id
                                         join t3 in db.PoDetails on t2.SiteFk equals t3.Id
                                         join t4 in db.EmployeeInfo on t1.EmployeeFk equals t4.Id
                                         where t3.Id == data.SiteId
                                         select t4.Name + " (" + t4.CId + ")").ToList(),
                        FieldWorkStatus = (from t1 in db.WorkUpdate
                                           join t2 in db.WorkStatus on t1.WorkStatusFk equals t2.Id
                                           where t1.SiteFk == data.SiteId
                                           orderby t1.Id descending
                                           select t2.Name).FirstOrDefault(),
                        ReportEngineer = (from t1 in db.ReportEngineerWorkStatus
                                          join t2 in db.User on t1.InsertBy equals t2.Id
                                          join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id into tr
                                          from t3 in tr.DefaultIfEmpty()
                                          where t1.SiteFk == data.SiteId
                                          orderby t1.Id descending
                                          select t3.Name + " (" + t3.CId + ")").FirstOrDefault(),
                        ReportStatus = (from t1 in db.ReportEngineerWorkStatus
                                        join t2 in db.ReportStatus on t1.ReportStatusFk equals t2.Id
                                        where t1.SiteFk == data.SiteId
                                        orderby t1.Id descending
                                        select t2.Name).FirstOrDefault(),
                        DeliveryStatus = (db.ReportEngineerWorkStatus.Where(x => x.SiteFk == data.SiteId && x.IsReadyForAcceptance == true).OrderByDescending(x => x.Id).Select(x => x.IsReadyForAcceptance).FirstOrDefault()),
                        TotalMilestones = db.SiteMilestone.Where(x => x.PoDetailsFk == data.SiteId).Count(),
                        AcceptanceMilestones = (from t1 in db.AcceptanceStatus
                                                join t2 in db.AccetanceMilestone on t1.Id equals t2.AcceptanceStatussFk
                                                where t1.SiteFk == data.SiteId
                                                select t2.Id).Count(),
                        AcceptanceDate = db.AcceptanceStatus.Where(x => x.SiteFk == data.SiteId).OrderByDescending(x => x.Id).Select(x => x.InsertDate ?? DateTime.Now).FirstOrDefault()


                    };
                    report.Add(overallPMWorkStatus);
                }
                ViewBag.PmOverallWorkStatus = report;
                ViewBag.StartDate = StartDate;
                ViewBag.FinishDate = FinishDate;
                return View();
            }
        }
        public ActionResult AccountStatusOfProject()
        {
            using (var db = new xOssContext())
            {
                var main = (from t1 in db.PoDetails
                            join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                            join t3 in db.Project on t2.ProjectFk equals t3.Id
                            select new
                            {
                                ProjectName=t3.ProjectName,
                                Po=t2.PoNo,
                                SiteCId=t1.Cid,
                                SiteName=t1.Name,
                                SiteId=t1.Id
                            }).ToList();
                List<AccountStatusOfProject> list = new List<AccountStatusOfProject>();
                foreach (var data in main)
                {
                    var report = new AccountStatusOfProject
                    {
                        Project = data.ProjectName,
                        PoNo = data.Po,
                        SiteCId = data.SiteCId,
                        SiteName = data.SiteName,
                        InvoiceStatus = db.InvoiceDetail.Where(x => x.SiteFk == data.SiteId).Select(x => x.Id).FirstOrDefault(),
                        InvoiceDate = (from t1 in db.InvoiceDetail
                                       join t2 in db.InvoiceMaster on t1.InvoiceMasterFk equals t2.Id
                                       where t1.SiteFk == data.SiteId
                                       select t2.InvoiceSubmissionDate).FirstOrDefault(),
                        PaymentStatus= (from t1 in db.InvoiceDetail
                                        join t2 in db.InvoiceMaster on t1.InvoiceMasterFk equals t2.Id
                                        where t1.SiteFk == data.SiteId && t2.IsPayment==true
                                        select t2.Id).FirstOrDefault(),
                        PaymentDate= (from t1 in db.InvoiceDetail
                                      join t2 in db.InvoiceMaster on t1.InvoiceMasterFk equals t2.Id
                                      where t1.SiteFk == data.SiteId && t2.IsPayment == true
                                      select t2.PaymentRecieveDate??new DateTime(1990,01,01)).FirstOrDefault()
                    };
                    list.Add(report);
                }
                ViewBag.allstatus = list;
                return View();
            }
        }
    }
}