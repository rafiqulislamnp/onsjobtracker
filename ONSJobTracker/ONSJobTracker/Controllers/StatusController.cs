﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.Status;

namespace ONSJobTracker.Controllers
{
    [SessionCheck]
    public class StatusController : Controller
    {
        #region Report Engineer Work Status

        public async Task<ActionResult> ReportEngineerWorkStatusIndex(int? planid,int? siteid)

        {
            using (var db = new xOssContext())
            {
                string employeeId = EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]);
                ViewBag.project = new SelectList(DropDownListHelper.GetProjectsOfAssignedReportEngineer(Convert.ToInt32(employeeId)), "Value", "Text");
                
                ViewBag.workstatus = new SelectList(DropDownListHelper.GetWorkStatus(), "Value", "Text");
                ViewBag.reportstatus = new SelectList(DropDownListHelper.GetReportStatus(), "Value", "Text");

                ReportEngineerWorkStatusViewModel viewmodel = new ReportEngineerWorkStatusViewModel();
                ViewBag.reporteng = viewmodel.ListReportEngineer(Convert.ToInt32(employeeId));


                if (planid!=null)
                {
                    var plandetailsinfo = (from t1 in db.PlanningMaster
                                               //join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                                           join t2 in db.Planning on t1.Id equals t2.PlanningMasterFk
                                           join t5 in db.PoDetails on t2.PoDetailsFk equals t5.Id
                                           join t3 in db.PoMaster on t5.PoMasterFk equals t3.Id
                                           join t4 in db.Project on t3.ProjectFk equals t4.Id
                                           where t1.Id == planid && t2.PoDetailsFk == siteid
                                           select new
                                           {
                                               ProjectId = t4.Id,
                                               PoMasterId = t3.Id,
                                               PoDetailsId = t2.PoDetailsFk
                                           }).FirstOrDefault();
                    ReportEngineerWorkStatus model = new ReportEngineerWorkStatus
                    {
                        ProjectFk= plandetailsinfo.ProjectId,
                        DeliveredDate = DateTime.Now
                    };
                    ViewBag.DropdownId = new JavaScriptSerializer().Serialize(plandetailsinfo);
                    return await Task.Run(() => View(model));
                }
                else
                {
                    ReportEngineerWorkStatus model = new ReportEngineerWorkStatus
                    {
                        //CompletedDate = DateTime.Now,
                        DeliveredDate = DateTime.Now
                    };
                    return await Task.Run(() => View(model));
                }
       
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateReportEngineerWorkStatus(ReportEngineerWorkStatus model, HttpPostedFileBase logFile)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    string filename = "";
                    if (model.Action == "Save")
                    {
                        if (logFile != null && logFile.ContentLength > 0)
                        {
                            string fileExtension = Path.GetExtension(logFile.FileName)?.ToLower();

                            //logfile.FileName;
                            var character = FileLocation.GetRandomNumber();
                            filename = "R-" + model.SiteName + character.ToString() + "(" + DateTime.Now.ToString("d") + ")" + fileExtension;
                            //string fileLocation = Server.MapPath(FileLocation.LogFile) + filename;
                            string fileLocation = Server.MapPath(FileLocation.LogFile) + filename;
                            logFile.SaveAs(fileLocation);
                        }
                        model.ReportFile = filename;
                        model.AddReady(true, DateTime.Now);
                        db.ReportEngineerWorkStatus.Add(model);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";

                    }
                    else if (model.Action == "Update")
                    {
                        {
                            ReportEngineerWorkStatus selectSingle =
                                 db.ReportEngineerWorkStatus.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                if (logFile != null && logFile.ContentLength > 0)
                                {
                                    string fullPath = Request.MapPath(FileLocation.LogFile + selectSingle.ReportFile);
                                    if (System.IO.File.Exists(fullPath))
                                    {
                                        System.IO.File.Delete(fullPath);
                                    }
                                    string fileExtension = Path.GetExtension(logFile.FileName)?.ToLower();

                                    //logfile.FileName;
                                    var character = FileLocation.GetRandomNumber();
                                    filename = "R-" + model.SiteName + character.ToString() + "(" + DateTime.Now.ToString("d") + ")" + fileExtension;
                                    //string fileLocation = Server.MapPath(FileLocation.LogFile) + filename;
                                    string fileLocation = Server.MapPath(FileLocation.LogFile) + filename;
                                    logFile.SaveAs(fileLocation);
                                    model.ReportFile = filename;
                                }
                                else
                                {
                                    model.ReportFile = selectSingle.ReportFile;
                                }
                                model.PoFk = selectSingle.PoFk;
                                model.SiteFk = selectSingle.SiteFk;
                                model.PlanningMasterFk = selectSingle.PlanningMasterFk;
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                    "ReportEngineerWorkStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("ReportEngineerWorkStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ReportEngineerWorkStatusIndex"));
        }

        public async Task<ActionResult> DeleteReportEngineerWorkStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    ReportEngineerWorkStatus selectSingle = db.ReportEngineerWorkStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.ReportEngineerWorkStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                            "ReportEngineerWorkStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("ReportEngineerWorkStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ReportEngineerWorkStatusIndex");
        }

        public async Task<ActionResult> SubmitReportEngineerWorkStatus(int id)
        {
            try
            {
                var report = new ReportEngineerWorkStatus { Id = id, Status = false };
                using (var db = new xOssContext()) // use your DbConext
                {
                    db.ReportEngineerWorkStatus.Attach(report);
                    db.Entry(report).Property(x => x.Status).IsModified = true;
                    await db.SaveChangesAsync();

                    // sweet alert
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Submitted Successfully.";
                    return RedirectToAction("ReportEngineerWorkStatusIndex");
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ReportEngineerWorkStatusIndex");
        }

        #endregion

        #region Work Delivery Status

        public async Task<ActionResult> WorkDeliveryStatusIndex()

        {
            using (var db = new xOssContext())
            {
                string employeeId = EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]);

                ViewBag.po = new SelectList(DropDownListHelper.GetPoMaster(), "Value", "Text");
                ViewBag.clients = new SelectList(DropDownListHelper.GetClientInfo(), "Value", "Text");
                ViewBag.rengs = new SelectList(DropDownListHelper.GetEmployees(), "Value", "Text");
                ViewBag.workstatus = new SelectList(DropDownListHelper.GetWorkStatus(), "Value", "Text");
                ViewBag.reportstatus = new SelectList(DropDownListHelper.GetReportStatus(), "Value", "Text");
                ViewBag.project = new SelectList(DropDownListHelper.GetProjectsForProjectManager(Convert.ToInt32(employeeId)), "Value", "Text");

                ReportEngineerWorkStatusViewModel data = new ReportEngineerWorkStatusViewModel();
                ViewBag.workdelivery = data.ListWorkDelivery(Convert.ToInt32(employeeId));
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateWorkDeliveryStatus(ReportEngineerWorkStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    var selectSingle =
                        db.ReportEngineerWorkStatus.FirstOrDefault(x => x.Id == model.Id);
                    if (selectSingle == null) return RedirectToAction("WorkDeliveryStatusIndex");
                    {
                        if (model.IsReportReassign)
                        {
                            //Create New Row
                            selectSingle.IsReportReassign = model.IsReportReassign;
                            selectSingle.IsWorkReassign = model.IsWorkReassign;
                            selectSingle.ReportWorkRef = model.Id;
                            selectSingle.CompletedDate = model.CompletedDate;
                            selectSingle.DeliveredDate = model.DeliveredDate;
                            selectSingle.Remarks = model.Remarks;
                            selectSingle.WorkStatusFk = model.WorkStatusFk;
                            selectSingle.ReportStatusFk = model.ReportStatusFk;
                            selectSingle.UpdateReady(true, DateTime.Now);
                            db.ReportEngineerWorkStatus.Add(selectSingle);

                            //Update Older One
                            var status = new ReportEngineerWorkStatus { Id = model.Id, IsDelivered = false };
                            db.ReportEngineerWorkStatus.Attach(status);
                            db.Entry(status).Property(x => x.IsDelivered).IsModified = true;


                            await db.SaveChangesAsync();


                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Reassign Successfully.";
                        }
                        else
                        {
                            selectSingle.IsReportReassign = model.IsReportReassign;
                            selectSingle.IsWorkReassign = model.IsWorkReassign;
                            selectSingle.ReportWorkRef = model.Id;
                            selectSingle.CompletedDate = model.CompletedDate;
                            selectSingle.DeliveredDate = model.DeliveredDate;
                            selectSingle.Remarks = model.Remarks;
                            selectSingle.WorkStatusFk = model.WorkStatusFk;
                            selectSingle.ReportStatusFk = model.ReportStatusFk;
                            db.Entry(selectSingle).State = EntityState.Modified;
                            db.SaveChanges();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }

                        if (model.IsWorkReassign)
                        {
                            var status = new PoDetails { Id = model.Id, Status = false };
                            db.PoDetails.Attach(status);
                            db.Entry(status).Property(x => x.Status).IsModified = true;
                        }

                        return RedirectToAction("WorkDeliveryStatusIndex");
                    }
                }

            }

            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("WorkDeliveryStatusIndex"));
        }

        public async Task<ActionResult> DeleteWorkDeliveryStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    WorkDeliveryStatus selectSingle = db.WorkDeliveryStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.WorkDeliveryStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                            "WorkDeliveryStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("WorkDeliveryStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkDeliveryStatusIndex");
        }
        public async Task<ActionResult> SubmitWorkForAcceptance(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    var status = new ReportEngineerWorkStatus { Id = id, IsReadyForAcceptance = true };
                    db.ReportEngineerWorkStatus.Attach(status);
                    db.Entry(status).Property(x => x.IsReadyForAcceptance).IsModified = true;
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkDeliveryStatusIndex");
        }

        #endregion

        #region Acceptance Status

        public async Task<ActionResult> AcceptanceStatusIndex()

        {
            using (var db = new xOssContext())
            {
                string employeeId = EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]);
                ViewBag.project = new SelectList(DropDownListHelper.GetProjectsForProjectManager(Convert.ToInt32(employeeId)), "Value", "Text");
                //ViewBag.po = new SelectList(DropDownListHelper.GetPoMaster(), "Value", "Text");
                //ViewBag.sites = new SelectList(DropDownListHelper.GetSitesForAcceptance(), "Value", "Text");
                //ViewBag.pacstatus = new SelectList(DropDownListHelper.GetPacStatus(), "Value", "Text");
                //ViewBag.facstatus = new SelectList(DropDownListHelper.GetFacStatus(), "Value", "Text");

                AcceptanceStatusViewModel model = new AcceptanceStatusViewModel();
                ViewBag.workdelivery = model.ListAcceptances();
                ViewBag.readyforacceptance = model.ReadyForAcceptanceList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateAcceptanceStatus(AcceptanceStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {

                    //var dictionary= db.PayementMilestone.Select(t => new { t.Id, t.Name })
                    //    .ToDictionary(t => t.Id, t => t.Name);
                    if (model.Action == "Save")
                    {

                        model.AddReady(true, DateTime.Now);
                        db.AcceptanceStatus.Add(model);
                        db.SaveChanges();
                        if (model.Statuses != null || model.Statuses.Any())
                        {
                            foreach (var data in model.Statuses)
                            {
                                AccetanceMilestone milestone = new AccetanceMilestone
                                {
                                    AcceptanceStatussFk = model.Id,
                                    PayementMilestoneFk = Convert.ToInt64(data)
                                };
                                milestone.AddReady(true, DateTime.Now);
                                db.AccetanceMilestone.Add(milestone);
                                await db.SaveChangesAsync();

                            }
                        }

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";

                    }
                    else if (model.Action == "Update")
                    {
                        db.AccetanceMilestone.RemoveRange(db.AccetanceMilestone.Where(c => c.AcceptanceStatussFk == model.Id));
                        db.SaveChanges();
                        AcceptanceStatus selectSingle =
                                    db.AcceptanceStatus.FirstOrDefault(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                "AcceptanceStatus", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            if (model.Statuses != null && model.Statuses.Any())
                            {

                                foreach (var data in model.Statuses)
                                {
                                    AccetanceMilestone milestone = new AccetanceMilestone
                                    {
                                        AcceptanceStatussFk = selectSingle.Id,
                                        PayementMilestoneFk = Convert.ToInt64(data)
                                    };

                                    milestone.AddReady(true, DateTime.Now);
                                    db.AccetanceMilestone.Add(milestone);
                                    db.SaveChanges();
                                }
                            }
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }
                    }
                }
                return RedirectToAction("AcceptanceStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("AcceptanceStatusIndex"));
        }

        public async Task<ActionResult> DeleteAcceptanceStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    db.AccetanceMilestone.RemoveRange(db.AccetanceMilestone.Where(c => c.AcceptanceStatussFk == id));
                    AcceptanceStatus selectSingle = db.AcceptanceStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.AcceptanceStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                            "AcceptanceStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("AcceptanceStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("AcceptanceStatusIndex");
        }

        #endregion

        #region Invoice Master Status

        public async Task<ActionResult> InvoiceMasterStatusIndex()
        {
            using (var db = new xOssContext())
            {
                ViewBag.po = new SelectList(DropDownListHelper.GetPoes(), "Value", "Text");
                ViewBag.invoicestatus = new SelectList(DropDownListHelper.GetInvoiceStatus(), "Value", "Text");

                InvoiceMasterViewModel model = new InvoiceMasterViewModel();

                ViewBag.invoicemaster = model.GetInvoiceMaster();
                InvoiceMaster master = new InvoiceMaster
                {
                    InvoiceSubmissionDate = DateTime.Today,
                    ProjectedPaymentDate = DateTime.Today
                };
                return await Task.Run(() => View(master));
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateInvoiceMasterStatus(InvoiceMaster model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.InvoiceMaster.Any(o => o.Cid == model.Cid))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Invoice Number exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            model.IsPayment = false;
                            db.InvoiceMaster.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Insert Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.InvoiceMaster.Any(x => x.Cid == model.Cid && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Invoice Number exists.";
                        }
                        else
                        {
                            InvoiceMaster selectSingle =
                                 db.InvoiceMaster.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;
                                model.InvoiceSubmissionDate = selectSingle.InvoiceSubmissionDate;
                                model.ProjectedPaymentDate = selectSingle.ProjectedPaymentDate;
                                model.Remarks = selectSingle.Remarks;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                    "InvoiceMaster", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("InvoiceMasterStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People."+e.InnerException.ToString()+"";
            }
            return await Task.Run(() => View("InvoiceMasterStatusIndex"));
        }

        public async Task<ActionResult> DeleteInvoiceMasterStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    InvoiceMaster selectSingle = db.InvoiceMaster.Find(id);
                    if (selectSingle != null)
                    {
                        db.InvoiceMaster.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "InvoiceMaster",
                            "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("InvoiceMasterStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("InvoiceMasterStatusIndex");
        }

        #endregion

        #region Invoice Details Status

        public async Task<ActionResult> InvoiceDetailsIndex(int id)
        {
            using (var db = new xOssContext())
            {
                ViewBag.po = new SelectList(DropDownListHelper.GetPoMasterForInvoice(), "Value", "Text");
                ViewBag.paymentmilesote = new SelectList(DropDownListHelper.GetPaymentMilstoneList(), "Value", "Text");
                //ViewBag.site = new SelectList(DropDownListHelper.GetSites(), "Value", "Text");

                //InvoiceMasterViewModel m1 = new InvoiceMasterViewModel();
                //ViewBag.invoicemaster = m1.GetInvoiceMasterById(poid);
                //ViewBag.poid = poid;

                InvoiceDetailViewModel m2 = new InvoiceDetailViewModel();
                ViewBag.invoicedetails = m2.ListInvoiceDetails(id);
                ViewBag.MasterId = id;
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateInvoiceDetails(InvoiceDetails model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        //if (db.WorkDeliveryStatus.Any(o => o.ProjectIdFk == model.ProjectIdFk))
                        //{
                        //    //sweet alert
                        //    Session["success_div"] = "true";
                        //    Session["success_msg"] = "Work Delivery exists.";
                        //}
                        //else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.InvoiceDetail.Add(model);
                            db.SaveChanges();
                            if (model.Milestones != null || model.Milestones.Any())
                            {
                                foreach (var data in model.Milestones)
                                {
                                    InvoiceMilestone milestone = new InvoiceMilestone
                                    {
                                        InvoiceDetailsFk = model.Id,
                                        PayementMilestoneFk = Convert.ToInt64(data)
                                    };
                                    milestone.AddReady(true, DateTime.Now);
                                    db.InvoiceMilestone.Add(milestone);
                                    await db.SaveChangesAsync();

                                }
                            }
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Insert Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        db.InvoiceMilestone.RemoveRange(db.InvoiceMilestone.Where(c => c.InvoiceDetailsFk == model.Id));
                        db.SaveChanges();
                        //if (db.WorkDeliveryStatus.Any(x => x.ProjectIdFk == model.ProjectIdFk && x.Id != model.Id))
                        //{
                        //    //sweet alert
                        //    Session["success_div"] = "true";
                        //    Session["success_msg"] = "Work Type exists.";
                        //}
                        //else

                        InvoiceDetails selectSingle =
                            db.InvoiceDetail.FirstOrDefault(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                "InvoiceDetails", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            if (model.Milestones != null && model.Milestones.Any())
                            {

                                foreach (var data in model.Milestones)
                                {
                                    InvoiceMilestone milestone = new InvoiceMilestone
                                    {
                                        InvoiceDetailsFk = selectSingle.Id,
                                        PayementMilestoneFk = Convert.ToInt64(data)
                                    };

                                    milestone.AddReady(true, DateTime.Now);
                                    db.InvoiceMilestone.Add(milestone);
                                    db.SaveChanges();
                                }
                            }
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }
                    }

                }

                return RedirectToAction("InvoiceDetailsIndex", new { id = model.InvoiceMasterFk });
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("InvoiceDetailsIndex"));
        }

        public async Task<ActionResult> DeleteInvoiceDetailsStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    InvoiceDetails selectSingle = db.InvoiceDetail.Find(id);
                    if (selectSingle != null)
                    {
                        db.InvoiceDetail.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "InvoiceDetail",
                            "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("InvoiceDetailsIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("InvoiceDetailsIndex");
        }

        #endregion

        #region Payment Status

        public async Task<ActionResult> PaymentStatusIndex()

        {
            using (var db = new xOssContext())
            {
                InvoiceMasterViewModel viewmodel = new InvoiceMasterViewModel();
                InvoiceMaster model = new InvoiceMaster
                {
                    PaymentRecieveDate = DateTime.Now
                };
                ViewBag.invoicemasterpayment = viewmodel.GetInvoiceMasterPayment();
                return await Task.Run(() => View(model));
            }
        }

        public async Task<ActionResult> PaidPaymentIndex()

        {
            using (var db = new xOssContext())
            {
                InvoiceMasterViewModel model = new InvoiceMasterViewModel();

                ViewBag.paidpayment = model.ListPaidPayment();
                return await Task.Run(() => View());
            }
        }
        [HttpPost]
        public async Task<ActionResult> CreatePaymentStatus(InvoiceMaster model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        ////if (db.WorkDeliveryStatus.Any(o => o.ProjectIdFk == model.ProjectIdFk))
                        ////{
                        ////    //sweet alert
                        ////    Session["success_div"] = "true";
                        ////    Session["success_msg"] = "Work Delivery exists.";
                        ////}
                        ////else
                        //{
                        //    model.AddReady(true, DateTime.Now);
                        //    //model.IsPayment = true;
                        //    db.Invoice.Add(model);
                        //    await db.SaveChangesAsync();

                        //    //sweet alert
                        //    Session["success_div"] = "true";
                        //    Session["success_msg"] = "Insert Successfully.";
                        //}
                    }
                    else if (model.Action == "Update")
                    {
                        //if (db.WorkDeliveryStatus.Any(x => x.ProjectIdFk == model.ProjectIdFk && x.Id != model.Id))
                        //{
                        //    //sweet alert
                        //    Session["success_div"] = "true";
                        //    Session["success_msg"] = "Work Type exists.";
                        //}
                        //else
                        {
                            InvoiceMaster selectSingle =
                                 db.InvoiceMaster.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;
                                model.IsPayment = true;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                    "PaymentStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("PaymentStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("PaymentStatusIndex"));
        }

        //public async Task<ActionResult> DeletePaymentStatus(int id)
        //{
        //    try
        //    {
        //        using (var db = new xOssContext()) // use your DbConext
        //        {
        //            Invoice selectSingle = await db.Invoice.FindAsync(id);
        //            if (selectSingle != null)
        //            {
        //                db.Invoice.Remove(selectSingle);
        //                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PaymentStatus", "Delete");
        //                await db.SaveChangesAsync();
        //                //sweet alert
        //                Session["success_div"] = "true";
        //                Session["success_msg"] = "Delete Successfully.";
        //                return RedirectToAction("InvoiceStatusIndex");
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        //sweet alert
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
        //    }
        //    return RedirectToAction("InvoiceStatusIndex");
        //}

        [HttpPost]
        public async Task<ActionResult> UpdatePayentStatus(InvoiceMaster model)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                InvoiceMaster selectSingle = db.InvoiceMaster.FirstOrDefault(x => x.Id == model.Id);
                // to remove  same key already exists in the ObjectStateManager
                ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);

                if (selectSingle != null)
                {
                    model.Id = selectSingle.Id;
                    model.InvoiceStatusFk = selectSingle.InvoiceStatusFk;
                    model.Cid = selectSingle.Cid;
                    model.InvoiceSubmissionDate = selectSingle.InvoiceSubmissionDate;
                    //model.PoFk = selectSingle.PoFk;
                    model.ProjectedPaymentDate = selectSingle.ProjectedPaymentDate;
                    model.Remarks = selectSingle.Remarks;
                    model.InsertBy = selectSingle.InsertBy;
                    model.InsertDate = selectSingle.InsertDate;
                    model.IsPayment = true;

                    model.UpdateReady(true, DateTime.Now);
                    SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PaymentStatus",
                        "Update");
                    db.Entry(model).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    //sweet alert
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Updated Successfully.";

                }
                return RedirectToAction("PaymentStatusIndex");
            }

        }
        #endregion

        public async Task<ActionResult> PaymentReconcile(int id)
        {
            try
            {
                var invoice = new InvoiceMaster { Id = id, Status = false };
                using (var db = new xOssContext()) // use your DbConext
                {
                    db.InvoiceMaster.Attach(invoice);
                    db.Entry(invoice).Property(x => x.Status).IsModified = true;
                    await db.SaveChangesAsync();

                    // sweet alert
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Submitted Successfully.";
                    return RedirectToAction("PaymentStatusIndex");
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("PaymentStatusIndex");
        }
    }
}