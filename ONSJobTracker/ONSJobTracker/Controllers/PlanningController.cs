﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.Purchase_Order;
using ONSJobTracker.ViewModels.Status;
using ONSJobTracker.ViewModels.Work_Plan;

namespace ONSJobTracker.Controllers
{
    [SessionCheck]
    public class PlanningController : Controller
    {
        private xOssContext _db = new xOssContext();
        // GET: Planning
        public async Task<ActionResult> WorkPlanIndex()
        {

            Planning model = new Planning
            {
                StartDate = DateTime.Today,
                FinishDate = DateTime.Today
            };
            ViewBag.allproject = new SelectList(DropDownListHelper.GetProjects(), "Value", "Text");
            ViewBag.allpomaster = new SelectList(DropDownListHelper.GetPoMaster(), "Value", "Text");
            ViewBag.allplanengineertype = new SelectList(DropDownListHelper.GetPlanEngineerType(), "Value", "Text");
            ViewBag.allresourcetype = new SelectList(DropDownListHelper.GetResourceType(), "Value", "Text");
            ViewBag.allfueltype = new SelectList(DropDownListHelper.GetfuelType(), "Value", "Text");
            ViewBag.allengineer = new SelectList(DropDownListHelper.GetEngineer(3), "Value", "Text"); //hard coded need to put engineer value manually
            return await Task.Run(() => View(model));
        }

        public async Task<ActionResult> CreateWorkPlan(FormCollection collection, List<string> SiteList)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    //var format = "dd-mm-yyyy"; // your datetime format
                    //var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                    string plan = Request.Form["PlanList"];
                    string transportplan = Request.Form["TransportPlanList"];
                    string othercostplan = Request.Form["OtherCostPlanList"];
                    //int siteid = Convert.ToInt32(Request.Form["Site"]);



                    var data = JsonConvert.DeserializeObject<List<WorkPlanViewModel>>(plan);
                    var transportdata = JsonConvert.DeserializeObject<List<TransportViewModel>>(transportplan);
                    var othercostdata = JsonConvert.DeserializeObject<List<OtherCostViewModel>>(othercostplan);
                    //var delsiteid = data.FirstOrDefault();
                    //db.Planning.RemoveRange(db.Planning.Where(x => x.PoDetailsFk == delsiteid.PoDetailsFk));


                    WorkPlanViewModel a = new WorkPlanViewModel();
                    string planRef = a.GetPlanReference();

                    PlanningMaster master = new PlanningMaster
                    {
                        PoDetailsFk = 0,
                        PlanReference = planRef
                    };
                    master.AddReady(true, DateTime.Now);
                    db.PlanningMaster.Add(master);
                    db.SaveChanges();
                    Int64 masterid = master.Id;


                    //var a = data[PoDetailsFk];
                    if (data != null && data.Any())
                    {
                        foreach (var site in SiteList)
                        {
                            Planning model = new Planning();
                            foreach (var viewmodel in data)
                            {
                                decimal taamount = 0;
                                decimal daamount = 0;
                                //DateTime startdate = DateTime.ParseExact(viewmodel.StartDate, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                //DateTime finishdate = DateTime.ParseExact(viewmodel.StartDate, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                model.PlanningMasterFk = masterid;
                                model.PoDetailsFk = Convert.ToInt64(site);
                                model.EmployeeInfoFk = viewmodel.EmployeeInfoFk;
                                model.StartDate = viewmodel.StartDate;
                                model.FinishDate = viewmodel.FinishDate;
                                model.Ta = viewmodel.Ta;
                                model.Da = viewmodel.Da;
                                model.Ta = viewmodel.OutsideTa;
                                model.Da = viewmodel.OutsideDa;
                                if (viewmodel.Ta)
                                {
                                    taamount += a.GetTaAmountByEmployeeDesignation(viewmodel.EmployeeInfoFk);
                                }
                                if (viewmodel.OutsideTa)//Half Da
                                {
                                    daamount += a.GetOutsideTaAmountByEmployeeDesignation(viewmodel.EmployeeInfoFk);
                                }
                                if (viewmodel.Da)
                                {
                                    daamount += a.GetDaAmountByEmployeeDesignation(viewmodel.EmployeeInfoFk);
                                }
                                if (viewmodel.OutsideDa)
                                {
                                    daamount += a.GetOutsideDaAmountByEmployeeDesignation(viewmodel.EmployeeInfoFk);
                                }

                                TimeSpan difference = viewmodel.FinishDate - viewmodel.StartDate; //create TimeSpan object
                                int days = (int)Math.Ceiling(difference.TotalDays);
                                model.Duration = days + 1;
                                model.TaAmount = (taamount * model.Duration) / SiteList.Count;
                                model.DaAmount = (daamount * model.Duration) / SiteList.Count;
                                model.Type = viewmodel.Type;
                                model.Remarks = viewmodel.Remarks;
                                model.AddReady(true, DateTime.Now);
                                db.Planning.Add(model);
                                db.SaveChanges();

                                // NO Report
                                if (model.Type==2 && model.EmployeeInfoFk== 10039)
                                {
                                    var report = new ReportEngineerWorkStatus
                                    {
                                        PoFk = db.PoDetails.Where(x=>x.Id==model.PoDetailsFk).Select(x=>x.PoMasterFk).FirstOrDefault(),
                                        SiteFk = model.Id,
                                        WorkStatusFk = 5,
                                        ReportStatusFk = 5,
                                        DeliveredDate = DateTime.Now,
                                        InsertBy = model.InsertBy,
                                        InsertDate = DateTime.Now,
                                        Status = true,
                                        IsWorkReassign = false,
                                        IsReportReassign = false,
                                        IsDelivered = false,
                                        IsReadyForAcceptance = false,
                                        ReportWorkRef = 0,
                                        PlanningMasterFk = model.PlanningMasterFk
                                    };
                                    db.ReportEngineerWorkStatus.Add(report);
                                    await db.SaveChangesAsync();
                                }
                                
                            }
                        }

                    }

                    if (transportdata != null && transportdata.Any())
                    {
                        Transport transport = new Transport();
                        foreach (var transporviewtmodel in transportdata)
                        {
                            //DateTime tstartdate = DateTime.ParseExact(transporviewtmodel.StartDate, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            //DateTime tfinishdate = DateTime.ParseExact(transporviewtmodel.StartDate, "M/d/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            transport.PlanningMasterFk = masterid;
                            transport.CarInfo = transporviewtmodel.CarInfo;
                            transport.DailyCost = transporviewtmodel.DailyCost;
                            transport.EstimatedDuration = transporviewtmodel.Duration;
                            transport.EstimatedKm = transporviewtmodel.EstimatedKm;
                            transport.FinishDate = transporviewtmodel.FinishDate;
                            transport.StartDate = transporviewtmodel.StartDate;
                            transport.FuelType = transporviewtmodel.FuelType;
                            transport.Remarks = transporviewtmodel.Remarks;
                            transport.TotalCarCost = transporviewtmodel.TotalCarCost;
                            transport.AddReady(true, DateTime.Now);
                            db.Transport.Add(transport);
                            await db.SaveChangesAsync();
                        }
                    }

                    if (othercostdata != null && othercostdata.Any())
                    {
                        OtherPlanCost cost = new OtherPlanCost();
                        foreach (var costmodel in othercostdata)
                        {
                            cost.PlanningMasterFk = masterid;
                            cost.CostName = costmodel.CostName;
                            cost.Amount = costmodel.Amount;
                            cost.Remarks = costmodel.Remarks;
                            cost.AddReady(true, DateTime.Now);
                            db.OtherPlanCost.Add(cost);
                            await db.SaveChangesAsync();
                        }
                    }
                    //sweet alert
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Insert Successfully.";
                    return RedirectToAction("WorkPlanIndex");
                }

            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkPlanIndex");
        }

        public async Task<ActionResult> WorkPlanList()
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                try
                {

                    var model = (from t1 in db.Planning
                                 join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                                 join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                                 select new PlanListViewModel
                                 {
                                     SiteId = t1.PoDetailsFk,
                                     Site = t2.Cid + " || " + t2.Name,
                                     Project = t3.ProjectCid + " || " + t3.PoNo
                                 }).ToList();
                    return await Task.Run(() => View(model));
                }
                catch (Exception e)
                {
                    //
                }
            }
            return await Task.Run(() => View());
        }


        public async Task<ActionResult> WorkPlanCreateEdit(PlanApproval model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Planning.Action == "Save")
                    {
                        var prevdata = db.Planning.FirstOrDefault(x => x.PlanningMasterFk == model.Planning.PlanningMasterFk);
                        model.Planning.PoDetailsFk = prevdata.PoDetailsFk;
                        decimal taamount = 0;
                        decimal daamount = 0;
                        WorkPlanViewModel a = new WorkPlanViewModel();

                        if (model.Planning.Ta)
                        {
                            taamount += a.GetTaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                        }
                        if (model.Planning.OutsideTa)
                        {
                            daamount += a.GetOutsideTaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                        }
                        if (model.Planning.Da)
                        {
                            daamount += a.GetDaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                        }
                        if (model.Planning.OutsideDa)
                        {
                            daamount += a.GetOutsideDaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                        }


                        TimeSpan difference = model.Planning.StartDate - model.Planning.FinishDate; //create TimeSpan object
                        int days = (int)Math.Ceiling(difference.TotalDays);
                        model.Planning.Duration = Math.Abs(days) + 1;
                        model.Planning.TaAmount = taamount * model.Planning.Duration;
                        model.Planning.DaAmount = daamount * model.Planning.Duration;
                        model.Planning.AddReady(true, DateTime.Now);
                        db.Planning.Add(model.Planning);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";
                    }

                    else if (model.Planning.Action == "Update")
                    {
                        Planning selectSingle = await db.Planning.FirstOrDefaultAsync(x => x.Id == model.Planning.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {
                            model.Planning.PlanningMasterFk = selectSingle.PlanningMasterFk;
                            model.Planning.PoDetailsFk = selectSingle.PoDetailsFk;
                            model.Planning.InsertBy = selectSingle.InsertBy;
                            model.Planning.InsertDate = selectSingle.InsertDate;
                            WorkPlanViewModel a = new WorkPlanViewModel();
                            if (model.Planning.Ta)
                            {
                                model.Planning.TaAmount = a.GetTaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                            }
                            else if (model.Planning.OutsideTa)
                            {
                                model.Planning.TaAmount = a.GetOutsideTaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                            }
                            else
                            {
                                model.Planning.TaAmount = 0;
                            }
                            if (model.Planning.Da)
                            {
                                model.Planning.DaAmount = a.GetDaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                            }
                            else if (model.Planning.OutsideDa)
                            {
                                model.Planning.DaAmount = a.GetOutsideDaAmountByEmployeeDesignation(model.Planning.EmployeeInfoFk);
                            }
                            else
                            {
                                model.Planning.DaAmount = 0;
                            }

                            TimeSpan difference = model.Planning.StartDate - model.Planning.FinishDate; //create TimeSpan object
                            int days = (int)Math.Ceiling(difference.TotalDays);
                            model.Planning.Duration = Math.Abs(days) + 1;
                            model.Planning.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Planning",
                                "Update");
                            db.Entry(model.Planning).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }
                    }

                }
                return RedirectToAction("WorkPlanViewForApproval", new { id = model.Planning.PlanningMasterFk });
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkPlanViewForApproval", new { id = model.Transport.PlanningMasterFk });
        }


        public async Task<ActionResult> DeletePlanningItem(int planid, int masterid)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    Planning selectSingle = await db.Planning.FindAsync(planid);
                    if (selectSingle != null)
                    {
                        db.Planning.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Planning",
                            "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Deleted Successfully";
                        return RedirectToAction("WorkPlanViewForApproval", new { id = masterid });
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkPlanViewForApproval", new { id = masterid });


        }

        //public async Task<ActionResult> TransportPlanIndex()
        //{
        //    ViewBag.allplan = new SelectList(DropDownListHelper.GetPlans(), "Value", "Text");
        //    ViewBag.allcartype = new SelectList(DropDownListHelper.GetCar(), "Value", "Text");
        //    ViewBag.allfueltype = new SelectList(DropDownListHelper.GetfuelType(), "Value", "Text");
        //    TransportPlanViewModel model = new TransportPlanViewModel();
        //    ViewBag.alltransportplan = model.AllTransportPlan();
        //    return await Task.Run(() => View());
        //}

        public async Task<ActionResult> CreateTransportPlan(PlanApproval model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Transport.CarInfo == 2)
                    {
                        model.Transport.FuelType = "0";
                        model.Transport.EstimatedKm = "0";
                    }
                    if (model.Transport.Action == "Save")
                    {
                        model.Transport.AddReady(true, DateTime.Now);
                        db.Transport.Add(model.Transport);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";
                    }

                    else if (model.Transport.Action == "Update")
                    {
                        Transport selectSingle = await db.Transport.FirstOrDefaultAsync(x => x.Id == model.Transport.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.Transport.InsertBy = selectSingle.InsertBy;
                            model.Transport.InsertDate = selectSingle.InsertDate;

                            model.Transport.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Transport",
                                "Update");
                            db.Entry(model.Transport).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }
                    }

                }
                return RedirectToAction("WorkPlanViewForApproval", new { id = model.Transport.PlanningMasterFk });
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkPlanViewForApproval", new { id = model.Transport.PlanningMasterFk });
        }

        public async Task<ActionResult> TransportPlanEdit(int id, int planid)
        {
            ViewBag.allplan = new SelectList(DropDownListHelper.GetPlans(), "Value", "Text");
            ViewBag.allcartype = new SelectList(DropDownListHelper.GetCar(), "Value", "Text");
            ViewBag.allfueltype = new SelectList(DropDownListHelper.GetfuelType(), "Value", "Text");
            var model = _db.Transport.Find(id);
            TempData["PlanId"] = planid;
            return await Task.Run(() => View(model));
        }
        //[HttpPost]
        //public async Task<ActionResult> TransportPlanEdit(Transport model)
        //{
        //    try
        //    {
        //        var panid = Convert.ToInt32(TempData["PlanId"].ToString());
        //        using (var db = new xOssContext()) // use your DbConext
        //        {
        //            Transport selectSingle = db.Transport.FirstOrDefault(x => x.Id == model.Id);
        //            // to remove  same key already exists in the ObjectStateManager
        //            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
        //            if (selectSingle != null)
        //            {

        //                //model.InsertBy = selectSingle.InsertBy;
        //                //model.InsertDate = selectSingle.InsertDate;

        //                model.UpdateReady(true, DateTime.Now);
        //                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Transport",
        //                    "Update");
        //                db.Entry(model).State = EntityState.Modified;
        //                await db.SaveChangesAsync();
        //                //sweet alert
        //                Session["success_div"] = "true";
        //                Session["success_msg"] = "Updated Successfully.";
        //            }
        //        }
        //        return RedirectToAction("WorkPlanViewForApproval", new { id = panid });
        //    }
        //    catch (Exception e)
        //    {

        //        //sweet alert
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
        //    }
        //    return await Task.Run(() => View("WorkPlanListForApproval"));
        //}

        //public async Task<ActionResult> DeleteTransportPlan(int id)
        //{
        //    try
        //    {
        //        using (var db = new xOssContext()) // use your DbConext
        //        {
        //            Transport selectSingle = await db.Transport.FindAsync(id);
        //            if (selectSingle != null)
        //            {
        //                db.Transport.Remove(selectSingle);
        //                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Transport",
        //                    "Delete");
        //                await db.SaveChangesAsync();
        //                //sweet alert
        //                Session["warning_div"] = "true";
        //                Session["warning_msg"] = "Deleted Successfully";
        //                return RedirectToAction("TransportPlanIndex");
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        //sweet alert
        //        Session["warning_div"] = "true";
        //        Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
        //    }
        //    return RedirectToAction("TransportPlanIndex");


        //}

        public async Task<ActionResult> DailyWorkUpdateIndex(int? planid, int? siteid)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    string employeeId = EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]);
                    ViewBag.project = new SelectList(DropDownListHelper.GetProjectsOfAssignedEmployee(Convert.ToInt32(employeeId)), "Value", "Text");
                    ViewBag.allworkstatus = new SelectList(DropDownListHelper.GetWorkStatus(), "Value", "Text");

                    if (planid != null)
                    {
                        var plandetailsinfo = (from t1 in db.PlanningMaster
                                                   //join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                                               join t2 in db.Planning on t1.Id equals t2.PlanningMasterFk
                                               join t5 in db.PoDetails on t2.PoDetailsFk equals t5.Id
                                               join t3 in db.PoMaster on t5.PoMasterFk equals t3.Id
                                               join t4 in db.Project on t3.ProjectFk equals t4.Id
                                               where t1.Id == planid && t2.PoDetailsFk == siteid
                                               select new
                                               {
                                                   ProjectId = t4.Id,
                                                   PoMasterId = t3.Id,
                                                   PoDetailsId = t2.PoDetailsFk
                                               }).FirstOrDefault();
                        WorkUpdate model = new WorkUpdate
                        {
                            Date = DateTime.Today,
                            ProjectFk = plandetailsinfo.ProjectId
                        };
                        ViewBag.DropdownId = new JavaScriptSerializer().Serialize(plandetailsinfo);
                        return await Task.Run(() => View(model));
                    }
                    else
                    {
                        WorkUpdate model = new WorkUpdate
                        {
                            Date = DateTime.Today
                        };
                        return await Task.Run(() => View(model));
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People. " + e.Message.ToString();
                return RedirectToAction("DailyWorkUpdateIndex");
            }
        }

        public async Task<ActionResult> CreateDailyWorkUpdate(WorkUpdate model, HttpPostedFileBase logFile)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
                    string filename = "";
                    if (model.Action == "Save")
                    {
                        if (logFile != null && logFile.ContentLength > 0)
                        {
                            string fileExtension = Path.GetExtension(logFile.FileName)?.ToLower();

                            var character = FileLocation.GetRandomNumber();
                            filename = model.SiteName + character.ToString() + GlobalFunction.DateToInt(DateTime.Now).ToString() + fileExtension;

                            string fileLocation = Server.MapPath(FileLocation.LogFile) + filename;
                            logFile.SaveAs(fileLocation);
                        }
                        model.FileLocation = filename;
                        model.Date = DateTime.Now;
                        model.AddReady(true, DateTime.Now);
                        db.WorkUpdate.Add(model);
                        db.SaveChanges();
                        WorkTeamMember member = new WorkTeamMember
                        {
                            EmployeeFk = employeeId,
                            WorkUpdateFk = model.Id
                        };
                        member.AddReady(true, DateTime.Now);
                        db.WorkTeamMember.Add(member);
                        await db.SaveChangesAsync();

                        //sweet alert
                        //Update report status for resource sharing PO

                        var report = db.ReportEngineerWorkStatus.FirstOrDefault(x=>x.SiteFk==model.SiteFk && x.PlanningMasterFk==model.PlanningMasterFk && x.Status==true && x.IsDelivered==false);
                        if (report!=null)
                        {
                            // to remove  same key already exists in the ObjectStateManager
                            //((IObjectContextAdapter)db).ObjectContext.Detach(report);

                            report.IsDelivered = true;
                            report.Status = false;
                           
                            db.Entry(report).State = EntityState.Modified;
                            
                            await db.SaveChangesAsync();
                            
                        }

                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";
                        return RedirectToAction("DailyWorkList");
                    }

                    if (model.Action == "Update")
                    {
                        WorkUpdate selectSingle = await db.WorkUpdate.FirstOrDefaultAsync(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {
                            if (logFile != null && logFile.ContentLength > 0)
                            {
                                string fullPath = Request.MapPath(FileLocation.LogFile + selectSingle.FileLocation);
                                if (System.IO.File.Exists(fullPath))
                                {
                                    System.IO.File.Delete(fullPath);
                                }
                                string fileExtension = Path.GetExtension(logFile.FileName)?.ToLower();

                                //logfile.FileName;
                                var character = FileLocation.GetRandomNumber();
                                filename = model.SiteName + character.ToString() + GlobalFunction.DateToInt(DateTime.Now).ToString() + fileExtension;
                                //string fileLocation = Server.MapPath(FileLocation.LogFile) + filename;
                                string fileLocation = Server.MapPath(FileLocation.LogFile) + filename;
                                logFile.SaveAs(fileLocation);
                                model.FileLocation = filename;
                            }
                            else
                            {
                                model.FileLocation = selectSingle.FileLocation;
                            }

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;
                            model.Date = selectSingle.Date;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkUpdate",
                                "Update");
                            db.Entry(model).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                            return RedirectToAction("DailyWorkList");
                        }
                    }
                }

                return RedirectToAction("DailyWorkUpdateIndex");
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People. " + e.Message.ToString();
                return RedirectToAction("DailyWorkUpdateIndex");
            }
        }

        public async Task<ActionResult> DailyWorkList()
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            try
            {
                var list = (from t1 in _db.WorkUpdate
                            join t2 in _db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                            join t3 in _db.WorkStatus on t1.WorkStatusFk equals t3.Id
                            join t4 in _db.WorkTeamMember on t1.Id equals t4.WorkUpdateFk
                            join t5 in _db.Planning on t2.Id equals t5.PlanningMasterFk
                            join t6 in _db.PoDetails on t1.SiteFk equals t6.Id
                            join t7 in _db.PoMaster on t6.PoMasterFk equals t7.Id
                            join t8 in _db.Project on t7.ProjectFk equals t8.Id
                            where t4.EmployeeFk == employeeId && t2.Status
                            select new WorkUpdateViewModel
                            {
                                Id = t1.Id,
                                Project = t8.ProjectName,
                                PoNo = t7.PoNo,
                                Site = t6.Name,
                                WorkStatus = t3.Name,
                                WorkStatusId = t3.Id,
                                StartTime = t1.StartTime,
                                EndTime = t1.EndTime,
                                Date = t1.Date
                            }).Distinct().OrderByDescending(x => x.Date).ToList();
                return await Task.Run(() => View(list));
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View());
        }

        public async Task<ActionResult> UpdateDailyWork(int workid)
        {

            using (var db = new xOssContext()) // use your DbConext
            {
                try
                {
                    //ViewBag.allplan = new SelectList(DropDownListHelper.GetPlans(), "Value", "Text");
                    string employeeId = EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]);
                    ViewBag.project = new SelectList(DropDownListHelper.GetProjectsOfAssignedEmployee(Convert.ToInt32(employeeId)), "Value", "Text");

                    ViewBag.allworkstatus = new SelectList(DropDownListHelper.GetWorkStatus(), "Value", "Text");
                    var model = db.WorkUpdate.FirstOrDefault(x => x.Id == workid);


                    var data = (from t1 in db.WorkUpdate
                                join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                join t3 in db.Planning on t2.Id equals t3.PlanningMasterFk
                                join t4 in db.PoMaster on t3.PoDetailsFk equals t4.Id
                                join t5 in db.Project on t4.ProjectFk equals t5.Id
                                where t1.Id == workid && t2.Status
                                select new
                                {
                                    ProjectId = t5.Id,
                                    PoId = t4.Id,
                                    SiteId = t3.PoDetailsFk
                                }).FirstOrDefault();
                    if (model != null)
                    {
                        Debug.Assert(data != null, nameof(data) + " != null");
                        model.ProjectFk = data.ProjectId;
                        model.PoFk = data.PoId;
                        model.SiteFk = data.SiteId;
                        ViewBag.po = new SelectList(DropDownListHelper.GetPoByProjectIdOfAssignedEmployee(model.ProjectFk), "Value", "Text");
                        ViewBag.site = new SelectList(DropDownListHelper.GetSitePoIdOfAssignedEmployee(model.PoFk), "Value", "Text");
                        return await Task.Run(() => View(model));
                    }
                }
                catch (Exception e)
                {

                    //sweet alert
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
                }
            }
            return await Task.Run(() => View());
        }

        public async Task<ActionResult> DeleteDailyWork(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    WorkUpdate selectSingle = await db.WorkUpdate.FindAsync(id);
                    if (selectSingle != null)
                    {
                        db.WorkUpdate.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkUpdate",
                            "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("DailyWorkList");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("DailyWorkList");


        }


        #region MyRegion


        public async Task<ActionResult> DailyWorkEngineerIndex(int id)
        {
            using (var db = new xOssContext())
            {
                var s = db.WorkTeamMember.ToList();
                var data = (from t1 in db.WorkTeamMember
                            join t2 in db.EmployeeInfo on t1.EmployeeFk equals t2.Id
                            where t1.WorkUpdateFk == id
                            select new WorkTeamMemberViewModel
                            {
                                EmployeeInfo = t2,
                                WorkTeamMember = t1,
                                DailyCostList = db.DailyCost.Where(x => x.WorkUpdateFk == id).ToList()
                            }).OrderBy(x => x.WorkTeamMember.Id).ToList();
                ViewBag.allengineers = data;
                WorkTeamMemberViewModel model = new WorkTeamMemberViewModel
                {
                    WorkTeamMember = new WorkTeamMember
                    {
                        WorkUpdateFk = id
                    },
                    DailyCost = new DailyCost
                    {
                        WorkUpdateFk = id
                    }
                };


                ViewBag.engineer = new SelectList(DropDownListHelper.GetEngineer(3), "Value", "Text");
                return await Task.Run(() => View(model));
            }

        }
        [HttpPost]
        public async Task<ActionResult> CreateDailyWorkEngineer(WorkTeamMemberViewModel model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.WorkTeamMember.Action == "Save")
                    {

                        model.WorkTeamMember.AddReady(true, DateTime.Now);
                        db.WorkTeamMember.Add(model.WorkTeamMember);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";

                    }
                    else if (model.WorkTeamMember.Action == "Update")
                    {

                        WorkTeamMember selectSingle = await db.WorkTeamMember.FirstOrDefaultAsync(x => x.Id == model.WorkTeamMember.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.WorkTeamMember.InsertBy = selectSingle.InsertBy;
                            model.WorkTeamMember.InsertDate = selectSingle.InsertDate;

                            model.WorkTeamMember.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkTeamMember", "Update");
                            db.Entry(model.WorkTeamMember).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }


                    }
                }
                return RedirectToAction("DailyWorkEngineerIndex", new { id = model.WorkTeamMember.WorkUpdateFk });
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("DailyWorkEngineerIndex"));
        }

        public async Task<ActionResult> DeleteDailyWorkEngineer(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    WorkTeamMember selectSingle = await db.WorkTeamMember.FindAsync(id);
                    if (selectSingle != null)
                    {
                        db.WorkTeamMember.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkTeamMember", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("DailyWorkEngineerIndex", new { id = selectSingle.WorkUpdateFk });
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("DailyWorkEngineerIndex");
        }

        [HttpPost]
        public async Task<ActionResult> CreateDailyWorkEngineerCost(WorkTeamMemberViewModel model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.DailyCost.Action == "Save")
                    {

                        model.DailyCost.AddReady(true, DateTime.Now);
                        db.DailyCost.Add(model.DailyCost);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";

                    }
                    else if (model.DailyCost.Action == "Update")
                    {

                        DailyCost selectSingle = await db.DailyCost.FirstOrDefaultAsync(x => x.Id == model.DailyCost.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.DailyCost.InsertBy = selectSingle.InsertBy;
                            model.DailyCost.InsertDate = selectSingle.InsertDate;

                            model.DailyCost.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "DailyCost", "Update");
                            db.Entry(model.DailyCost).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }


                    }
                }
                return RedirectToAction("DailyWorkEngineerIndex", new { id = model.DailyCost.WorkUpdateFk });
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("DailyWorkEngineerIndex"));
        }

        public async Task<ActionResult> DeleteDailyWorkEngineerCost(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    DailyCost selectSingle = await db.DailyCost.FindAsync(id);
                    if (selectSingle != null)
                    {
                        db.DailyCost.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "DailyCost", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("DailyWorkEngineerIndex", new { id = selectSingle.WorkUpdateFk });
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("DailyWorkEngineerIndex");
        }
        public async Task<ActionResult> WorkPlanListForApproval()
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                try
                {
                    var model = _db.PlanningMaster.OrderByDescending(x => x.Id).AsEnumerable();
                    return await Task.Run(() => View(model));
                }
                catch (Exception e)
                {
                    //
                }
            }
            return await Task.Run(() => View());
        }

        public async Task<ActionResult> WorkPlanViewForApproval(int id)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                try
                {
                    ViewBag.allCarInfo = new SelectList(DropDownListHelper.GetCarType(), "Value", "Text");
                    ViewBag.allfueltype = new SelectList(DropDownListHelper.GetfuelType(), "Value", "Text");
                    ViewBag.allengineer = new SelectList(DropDownListHelper.GetEngineer(3), "Value", "Text");
                    ViewBag.allplanengineertype = new SelectList(DropDownListHelper.GetPlanEngineerType(), "Value", "Text");
                    ViewBag.allvendor = new SelectList(DropDownListHelper.GetVendorList(), "Value", "Text");
                    //var master = (from t1 in db.PlanningMaster
                    //              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                    //              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                    //              join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                    //              join t5 in db.ClientInfo on t3.EndClientInfoFk equals t5.Id
                    //              where t1.Id == id
                    //              select new ApprovalMaster
                    //              {
                    //                  PlanningMaster = t1,
                    //                  PoMaster = t3,
                    //                  ClientName = t4.Name,
                    //                  EndClientName = t5.Name
                    //              }).FirstOrDefault();
                    var master = (from t1 in db.Planning
                                  join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                  join t3 in db.PoDetails on t1.PoDetailsFk equals t3.Id
                                  join t4 in db.PoMaster on t3.PoMasterFk equals t4.Id
                                  join t5 in db.ClientInfo on t4.ClientInfoFk equals t5.Id
                                  join t6 in db.ClientInfo on t4.EndClientInfoFk equals t6.Id
                                  join t7 in db.Project on t4.ProjectFk equals t7.Id
                                  where t2.Id == id
                                  group new { t2, t3, t7, t4, t5, t6 } by new { PlanningMasterId = t2.Id, Approval = t2.AdminApproval, SiteId = t3.Id, Projectname = t7.ProjectName, PoNo = t4.PoNo, ClientName = t5.Name, EndClientName = t6.Name }
                                  into grp
                                  select new ApprovalMaster
                                  {
                                      ClientName = grp.Key.ClientName,
                                      EndClientName = grp.Key.EndClientName,
                                      PoNo = grp.Key.PoNo,
                                      Projectname = grp.Key.Projectname,
                                      PoDetaisFk = grp.Key.SiteId,
                                      PlanApproval = grp.Key.Approval,
                                      PlanningMasterId = grp.Key.PlanningMasterId
                                  }).ToList();
                    PoDetailsViewModel viewmodel = new PoDetailsViewModel();
                    List<PoDetailsViewModel> poDetailsViews = new List<PoDetailsViewModel>();
                    foreach (var data in master)
                    {
                        poDetailsViews.Add(viewmodel.GetSiteDetails((int)data.PoDetaisFk));
                    }
                    var podetails = poDetailsViews;


                    var details = (from t1 in db.Planning
                                   join t2 in db.EmployeeInfo on t1.EmployeeInfoFk equals t2.Id
                                   join t3 in db.EmployeeDesignation on t2.EmployeeDesignationFk equals t3.Id
                                   where t1.PlanningMasterFk == id
                                   orderby t1.Type
                                   select new ApprovalDetails
                                   {
                                       Planning = t1,
                                       EmployeeInfo = t2,
                                       EmployeeDesignation = t3
                                   }).ToList();
                    var transport = (from t1 in db.Transport
                                     where t1.PlanningMasterFk == id
                                     select new TransportModelForPayment
                                     {
                                         Transport = t1
                                     }).ToList();
                    var otehrs = (from t1 in db.OtherPlanCost
                                  where t1.PlanningMasterFk == id
                                  select new OtherCostPayment
                                  {
                                      OtherPlanCost = t1
                                  }).ToList();
                    //var otehrs = db.OtherPlanCost.Where(x => x.PlanningMasterFk == id).ToList();
                    PlanApproval model = new PlanApproval
                    {
                        TransportList = transport,
                        ApprovalDetails = details,
                        ApprovalMaster = master,
                        PoDetailsView = podetails,
                        OtherPlanCostList = otehrs,
                        Transport = new Transport
                        {
                            PlanningMasterFk = Convert.ToInt64(id),
                            StartDate = DateTime.Now,
                            FinishDate = DateTime.Now
                        },
                        OtherPlanCost = new OtherPlanCost
                        {
                            PlanningMasterFk = Convert.ToInt64(id)
                        },
                        Planning = new Planning
                        {
                            PlanningMasterFk = Convert.ToInt64(id),
                            StartDate = DateTime.Now,
                            FinishDate = DateTime.Now
                        }
                    };

                    return await Task.Run(() => View(model));
                }
                catch (Exception e)
                {
                    //sweet alert
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
                }
                return await Task.Run(() => View());
            }
        }

        public async Task<ActionResult> ApprovePlan(bool status, int id)
        {
            using (var db = new xOssContext()) // use your DbConext
            {

                PlanningMaster selectSingle = await db.PlanningMaster.FirstOrDefaultAsync(x => x.Id == id);
                // to remove  same key already exists in the ObjectStateManager
                ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                PlanningMaster model = new PlanningMaster();
                if (selectSingle != null)
                {
                    model.PoDetailsFk = selectSingle.PoDetailsFk;
                    model.Id = selectSingle.Id;
                    model.PlanReference = selectSingle.PlanReference;

                    model.AccountsApproval = selectSingle.AccountsApproval;
                    model.AdminApproval = status;
                    model.InsertDate = selectSingle.InsertDate;
                    model.InsertBy = selectSingle.InsertBy;


                    model.UpdateReady(true, DateTime.Now);
                    SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PlanningMaster",
                        "Update");
                    db.Entry(model).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    //sweet alert
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Approved Successfully.";
                }
            }
            return RedirectToAction("WorkPlanListForApproval");
        }

        public async Task<ActionResult> AcceptOrDeniedTransportPlan(int transportid, bool status, int planid)
        {
            var transport = new Transport { Id = transportid, Status = status };
            using (var db = new xOssContext())
            {
                db.Transport.Attach(transport);
                db.Entry(transport).Property(x => x.Status).IsModified = true;
                await db.SaveChangesAsync();
            }
            return RedirectToAction("WorkPlanViewForApproval", new { id = planid });
        }

        [HttpPost]
        public async Task<ActionResult> CreateOthersCost(PlanApproval model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {

                    if (model.OtherPlanCost.Action == "Save")
                    {
                        model.OtherPlanCost.AddReady(true, DateTime.Now);
                        db.OtherPlanCost.Add(model.OtherPlanCost);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";
                    }

                    else if (model.OtherPlanCost.Action == "Update")
                    {
                        OtherPlanCost selectSingle = await db.OtherPlanCost.FirstOrDefaultAsync(x => x.Id == model.OtherPlanCost.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.OtherPlanCost.InsertBy = selectSingle.InsertBy;
                            model.OtherPlanCost.InsertDate = selectSingle.InsertDate;
                            model.OtherPlanCost.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "OtherPlanCost",
                                "Update");
                            db.Entry(model.OtherPlanCost).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }
                    }

                }
                return RedirectToAction("WorkPlanViewForApproval", new { id = model.OtherPlanCost.PlanningMasterFk });
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkPlanViewForApproval", new { id = model.OtherPlanCost.PlanningMasterFk });
        }
        public async Task<ActionResult> AcceptOrDeniedOtherPlan(int otherid, bool status, int planid)
        {
            var other = new OtherPlanCost { Id = otherid, Status = status };
            using (var db = new xOssContext())
            {
                db.OtherPlanCost.Attach(other);
                db.Entry(other).Property(x => x.Status).IsModified = true;
                await db.SaveChangesAsync();
            }
            return RedirectToAction("WorkPlanViewForApproval", new { id = planid });
        }

        [HttpPost]
        public async Task<ActionResult> AssignCars(PlanApproval model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    var transport = new Transport { Id = model.Transport.Id, CarFk = model.Transport.CarFk };
                    db.Transport.Attach(transport);
                    db.Entry(transport).Property(x => x.CarFk).IsModified = true;
                    await db.SaveChangesAsync();
                }
                //sweet alert
                Session["success_div"] = "true";
                Session["success_msg"] = "Assigned Successfully.";
                return RedirectToAction("WorkPlanViewForApproval", new { id = model.Transport.PlanningMasterFk });
            }
            catch
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkPlanListForApproval");
        }

        public ActionResult AcceptedPlanList()
        {

            using (var db = new xOssContext()) // use your DbConext
            {
                var model = db.PlanningMaster.Where(x => x.AdminApproval).ToList().OrderBy(x => x.Id);
                return View(model);
            }
        }

        public async Task<ActionResult> AssignAmountForPlan(int id)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                try
                {

                    //var master = (from t1 in db.PlanningMaster
                    //              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                    //              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                    //              join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                    //              join t5 in db.ClientInfo on t3.EndClientInfoFk equals t5.Id
                    //              where t1.Id == id
                    //              select new ApprovalMaster
                    //              {
                    //                  PlanningMaster = t1,
                    //                  PoMaster = t3,
                    //                  ClientName = t4.Name,
                    //                  EndClientName = t5.Name
                    //              }).FirstOrDefault();
                    var master = (from t1 in db.Planning
                                  join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                  join t3 in db.PoDetails on t1.PoDetailsFk equals t3.Id
                                  join t4 in db.PoMaster on t3.PoMasterFk equals t4.Id
                                  join t5 in db.ClientInfo on t4.ClientInfoFk equals t5.Id
                                  join t6 in db.ClientInfo on t4.EndClientInfoFk equals t6.Id
                                  join t7 in db.Project on t4.ProjectFk equals t7.Id
                                  where t2.Id == id
                                  group new { t2, t3, t7, t4, t5, t6 } by new { PlanningMasterId = t2.Id, Approval = t2.AdminApproval, SiteId = t3.Id, Projectname = t7.ProjectName, PoNo = t4.PoNo, ClientName = t5.Name, EndClientName = t6.Name }
                                 into grp
                                  select new ApprovalMaster
                                  {
                                      ClientName = grp.Key.ClientName,
                                      EndClientName = grp.Key.EndClientName,
                                      PoNo = grp.Key.PoNo,
                                      Projectname = grp.Key.Projectname,
                                      PoDetaisFk = grp.Key.SiteId,
                                      PlanApproval = grp.Key.Approval,
                                      PlanningMasterId = grp.Key.PlanningMasterId
                                  }).ToList();

                    //PoDetailsViewModel viewmodel = new PoDetailsViewModel();

                    //var podetails = viewmodel.GetSiteDetails((int)master.PlanningMaster.PoDetailsFk);
                    PoDetailsViewModel viewmodel = new PoDetailsViewModel();
                    List<PoDetailsViewModel> poDetailsViews = new List<PoDetailsViewModel>();
                    foreach (var data in master)
                    {
                        poDetailsViews.Add(viewmodel.GetSiteDetails((int)data.PoDetaisFk));
                    }
                    var podetails = poDetailsViews;

                    var details = (from t1 in db.Planning
                                   join t2 in db.EmployeeInfo on t1.EmployeeInfoFk equals t2.Id
                                   join t3 in db.EmployeeDesignation on t2.EmployeeDesignationFk equals t3.Id
                                   where t1.PlanningMasterFk == id && t1.Type == 1
                                   select new ApprovalDetails
                                   {
                                       Planning = t1,
                                       EmployeeInfo = t2,
                                       EmployeeDesignation = t3,
                                       TaDaAmounts = (from t4 in db.Payment
                                                      where t4.PlanningMasterFk == id && t4.EmployeeInfoFk == t2.Id && t4.CostType == 1
                                                      select new TaDaAmountViewModel
                                                      {
                                                          PaymentDate = t4.PaymentDate,
                                                          PaidAmount = t4.PaidAmount,
                                                          Remarks = t4.Remarks,
                                                          EmployeeName = ""
                                                      }).ToList()
                                   }).ToList();
                    //var transport = db.Transport.Where(x => x.PlanningMasterFk == id && x.Status).ToList();

                    var transport = (from t1 in db.Transport
                                     where t1.PlanningMasterFk == id && t1.Status
                                     select new TransportModelForPayment
                                     {
                                         Transport = t1,
                                         TransportPayment = (from t2 in db.Payment
                                                             join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id
                                                             where t2.PlanningMasterFk == id && t2.CostType == 2
                                                             select new TaDaAmountViewModel
                                                             {
                                                                 PaymentDate = t2.PaymentDate,
                                                                 PaidAmount = t2.PaidAmount,
                                                                 Remarks = t2.Remarks,
                                                                 EmployeeName = t3.Name
                                                             }).ToList()
                                     }).ToList();


                    var otehrs = (from t1 in db.OtherPlanCost
                                  where t1.PlanningMasterFk == id && t1.Status
                                  select new OtherCostPayment
                                  {
                                      OtherPlanCost = t1,
                                      OtherPlanCostPayment = (from t2 in db.Payment
                                                              join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id
                                                              where t2.PlanningMasterFk == id && t2.CostType == 3
                                                              select new TaDaAmountViewModel
                                                              {
                                                                  PaymentDate = t2.PaymentDate,
                                                                  PaidAmount = t2.PaidAmount,
                                                                  Remarks = t2.Remarks,
                                                                  EmployeeName = t3.Name
                                                              }).ToList()
                                  }).ToList();
                    //var otehrs = db.OtherPlanCost.Where(x => x.PlanningMasterFk == id && x.Status).ToList();
                    ViewBag.allemployee = new SelectList(DropDownListHelper.GetEngineerByPlan(id), "Value", "Text");
                    PlanApproval model = new PlanApproval
                    {
                        TransportList = transport,
                        ApprovalDetails = details,
                        ApprovalMaster = master,
                        PoDetailsView = podetails,
                        OtherPlanCostList = otehrs,
                        Transport = new Transport
                        {
                            PlanningMasterFk = Convert.ToInt64(id),

                        },
                        OtherPlanCost = new OtherPlanCost
                        {
                            PlanningMasterFk = Convert.ToInt64(id)
                        },
                        TaDaAmount = new Payment
                        {
                            PlanningMasterFk = Convert.ToInt64(id),
                            PaymentDate = DateTime.Now
                        }
                    };

                    return await Task.Run(() => View(model));
                }
                catch (Exception e)
                {
                    //sweet alert
                    Session["warning_div"] = "true";
                    Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
                }
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> PayTaDaAmounts(PlanApproval model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {

                    if (model.TaDaAmount.Action == "Save")
                    {
                        model.TaDaAmount.AddReady(true, DateTime.Now);
                        db.Payment.Add(model.TaDaAmount);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";
                    }

                    else if (model.TaDaAmount.Action == "Update")
                    {
                        Payment selectSingle = await db.Payment.FirstOrDefaultAsync(x => x.Id == model.TaDaAmount.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.TaDaAmount.InsertBy = selectSingle.InsertBy;
                            model.TaDaAmount.InsertDate = selectSingle.InsertDate;
                            model.TaDaAmount.CostType = selectSingle.CostType;
                            model.TaDaAmount.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "TaDaAmount",
                                "Update");
                            db.Entry(model.TaDaAmount).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }
                    }

                }
                return RedirectToAction("AssignAmountForPlan", new { id = model.TaDaAmount.PlanningMasterFk });
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("AssignAmountForPlan", new { id = model.TaDaAmount.PlanningMasterFk });
        }

        #endregion

        public async Task<ActionResult> WorkStatusView()
        {
            try
            {
                long employeeId = Convert.ToInt64(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
                string designationId = System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["UserDesignationId"];
                using (var db = new xOssContext()) // use your DbConext
                {
                    ViewBag.workstatuses = new SelectList(DropDownListHelper.GetWorkStatus(), "Value", "Text");
                    ViewBag.reportstatuses = new SelectList(DropDownListHelper.GetReportStatus(), "Value", "Text");
                    ViewBag.teamleaderstatus = new SelectList(DropDownListHelper.GetTeamleaderStatus(), "Value", "Text");


                    if (designationId == "5" || designationId == "8" || designationId == "9")
                    {
                        var data = (from t1 in db.Planning
                                    join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                    join t3 in db.PoDetails on t1.PoDetailsFk equals t3.Id
                                    join t6 in db.PoMaster on t3.PoMasterFk equals t6.Id
                                    join t7 in db.PoMaster on t3.PoMasterFk equals t7.Id
                                    join t8 in db.Project on t7.ProjectFk equals t8.Id
                                    where t2.AdminApproval
                                    select new WorkStatusViewModel
                                    {
                                        PlanningMasterId = t2.Id,
                                        PlanmasterName = t2.PlanReference,
                                        ProjectName = t8.ProjectName,
                                        PO = t7.PoNo,
                                        SiteName = t3.Name,
                                        SiteId = t3.Id,
                                        TeamLeaderStatus = t2.TeamLeaderStatus
                                    }).Distinct().OrderByDescending(x => x.PlanningMasterId).ToList();

                        var currentworkstatus = db.WorkUpdate.GroupBy(a => new { a.PlanningMasterFk, a.SiteFk }).
                        Select(g => g.OrderByDescending(x => x.Id).FirstOrDefault()).
                        Select(c => new { c.PlanningMasterFk, c.WorkStatusFk, c.SiteFk });

                        var currentreportstatus = db.ReportEngineerWorkStatus.GroupBy(a => new { a.PlanningMasterFk, a.SiteFk }).
                        Select(g => g.OrderByDescending(x => x.Id).FirstOrDefault()).
                        Select(c => new { c.PlanningMasterFk, c.ReportStatusFk, c.SiteFk });

                        ViewBag.workstatus = new JavaScriptSerializer().Serialize(currentworkstatus);
                        ViewBag.reportstatus = new JavaScriptSerializer().Serialize(currentreportstatus);

                        return await Task.Run(() => View(data));
                    }
                    else
                    {
                        var data = (from t1 in db.Planning
                                    join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                    join t3 in db.PoDetails on t1.PoDetailsFk equals t3.Id
                                    join t4 in db.EmployeeInfo on t1.EmployeeInfoFk equals t4.Id
                                    join t5 in db.EmployeeDesignation on t4.EmployeeDesignationFk equals t5.Id
                                    join t6 in db.PoMaster on t3.PoMasterFk equals t6.Id
                                    join t7 in db.PoMaster on t3.PoMasterFk equals t7.Id
                                    join t8 in db.Project on t7.ProjectFk equals t8.Id
                                    where t2.AdminApproval && t1.EmployeeInfoFk == employeeId
                                    select new WorkStatusViewModel
                                    {
                                        PlanningMasterId = t2.Id,
                                        PlanmasterName = t2.PlanReference,
                                        ProjectName = t8.ProjectName,
                                        PO = t7.PoNo,
                                        SiteName = t3.Name,
                                        SiteId = t3.Id,
                                        TeamLeaderStatus = t2.TeamLeaderStatus,
                                        Type = t1.Type
                                    }).Distinct().OrderByDescending(x => x.PlanningMasterId).ToList();

                        List<WorkUpdateStausForAssignedEmp> currentworkstatus = new List<WorkUpdateStausForAssignedEmp>();
                        List<ReportUpdateStausForAssignedEmp> currentreportstatus = new List<ReportUpdateStausForAssignedEmp>();
                        foreach (var assignedwork in data)
                        {
                            var status = db.WorkUpdate.Where(p => p.PlanningMasterFk == assignedwork.PlanningMasterId && p.SiteFk == assignedwork.SiteId).
                                        GroupBy(a => new { a.PlanningMasterFk, a.SiteFk }).
                                        Select(g => g.OrderByDescending(x => x.Id).FirstOrDefault()).
                                        Select(c => new WorkUpdateStausForAssignedEmp { PlanningMasterFk = c.PlanningMasterFk, WorkStatusFk = c.WorkStatusFk, SiteFk = c.SiteFk }).FirstOrDefault();

                            var reports = db.ReportEngineerWorkStatus.Where(p => p.PlanningMasterFk == assignedwork.PlanningMasterId && p.SiteFk == assignedwork.SiteId).
                                        GroupBy(a => new { a.PlanningMasterFk, a.SiteFk }).
                                        Select(g => g.OrderByDescending(x => x.Id).FirstOrDefault()).
                                        Select(c => new ReportUpdateStausForAssignedEmp { PlanningMasterFk = c.PlanningMasterFk, ReportStatusFk = c.ReportStatusFk, SiteFk = c.SiteFk }).FirstOrDefault();

                            currentworkstatus.Add(status);
                            currentreportstatus.Add(reports);
                        }

                        ViewBag.workstatus = new JavaScriptSerializer().Serialize(currentworkstatus);
                        ViewBag.reportstatus = new JavaScriptSerializer().Serialize(currentreportstatus);
                        return await Task.Run(() => View(data));
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View());
        }

        public async Task<ActionResult> ViewWorkStatusesInDetails(Int64 planid, Int64 siteid)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    //var master = (from t1 in db.PlanningMaster
                    //              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                    //              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                    //              join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                    //              join t5 in db.ClientInfo on t3.EndClientInfoFk equals t5.Id
                    //              where t1.Id == id
                    //              select new ApprovalMaster
                    //              {
                    //                  PlanningMaster = t1,
                    //                  PoMaster = t3,
                    //                  ClientName = t4.Name,
                    //                  EndClientName = t5.Name
                    //              }).FirstOrDefault();
                    //PoDetailsViewModel viewmodel = new PoDetailsViewModel();
                    //var podetails = viewmodel.GetSiteDetails((int)master.PlanningMaster.PoDetailsFk);
                    var master = (from t1 in db.Planning
                                  join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                  join t3 in db.PoDetails on t1.PoDetailsFk equals t3.Id
                                  join t4 in db.PoMaster on t3.PoMasterFk equals t4.Id
                                  join t5 in db.ClientInfo on t4.ClientInfoFk equals t5.Id
                                  join t6 in db.ClientInfo on t4.EndClientInfoFk equals t6.Id
                                  join t7 in db.Project on t4.ProjectFk equals t7.Id
                                  where t2.Id == planid && t3.Id == siteid
                                  group new { t2, t3, t7, t4, t5, t6 } by new { PlanningMasterId = t2.Id, Approval = t2.AdminApproval, SiteId = t3.Id, Projectname = t7.ProjectName, PoNo = t4.PoNo, ClientName = t5.Name, EndClientName = t6.Name }
                                 into grp
                                  select new ApprovalMaster
                                  {
                                      ClientName = grp.Key.ClientName,
                                      EndClientName = grp.Key.EndClientName,
                                      PoNo = grp.Key.PoNo,
                                      Projectname = grp.Key.Projectname,
                                      PoDetaisFk = grp.Key.SiteId,
                                      PlanApproval = grp.Key.Approval,
                                      PlanningMasterId = grp.Key.PlanningMasterId
                                  }).ToList();
                    PoDetailsViewModel viewmodel = new PoDetailsViewModel();
                    List<PoDetailsViewModel> poDetailsViews = new List<PoDetailsViewModel>();
                    foreach (var data in master)
                    {
                        poDetailsViews.Add(viewmodel.GetSiteDetails((int)data.PoDetaisFk));
                    }
                    var podetails = poDetailsViews;

                    var workupdateslist = (from t1 in db.WorkUpdate
                                           join t2 in db.WorkStatus on t1.WorkStatusFk equals t2.Id
                                           where t1.PlanningMasterFk == planid && t1.SiteFk == siteid
                                           orderby t1.Date
                                           select new WorkUpdateViewModel
                                           {
                                               Date = t1.Date,
                                               Location = t1.Location,
                                               FileLocation = t1.FileLocation,
                                               Id = t1.Id,
                                               StartTime = t1.StartTime,
                                               EndTime = t1.EndTime,
                                               WorkStatus = t2.Name,
                                               Problem = t1.Problem ?? "N/A",
                                               Resolve = t1.Resolve ?? "N/A",
                                               Remarks = t1.Remarks ?? "N/A",
                                               DriverName = t1.Drivername ?? "N/A",
                                               DriverContact = t1.DriverContact ?? "N/A",
                                               EndMilage = t1.EndMileage ?? 0,
                                               StartMilage = t1.StartMileage ?? 0,
                                               CarRegNo = t1.CarRegNo ?? "N/A",
                                               DailyExpense = (from t3 in db.WorkTeamMember
                                                               join t4 in db.EmployeeInfo on t3.EmployeeFk equals t4.Id
                                                               where t3.WorkUpdateFk == t1.Id
                                                               select new WorkTeamMemberViewModel
                                                               {
                                                                   EmployeeInfo = t4,
                                                                   WorkTeamMember = t3,
                                                                   DailyCostList = db.DailyCost.Where(x => x.WorkUpdateFk == t1.Id && x.EmployeeFk == t4.Id).ToList()
                                                               }).OrderBy(x => x.WorkTeamMember.Id).ToList()
                                           }).ToList();

                    var report = (from t1 in db.ReportEngineerWorkStatus
                                  join t2 in db.WorkStatus on t1.WorkStatusFk equals t2.Id
                                  join t3 in db.ReportStatus on t1.ReportStatusFk equals t3.Id
                                  where t1.PlanningMasterFk == planid && t1.SiteFk == siteid
                                  orderby t1.Id
                                  select new ReportEngineerWorkStatusViewModel
                                  {
                                      Id = t1.Id,
                                      WorkStatus = t2.Name,
                                      ReportStatus = t3.Name,
                                      CompletedDate = t1.CompletedDate ?? new DateTime(1900, 01, 01),
                                      DeliveredDate = t1.DeliveredDate,
                                      Remarks = t1.Remarks,
                                      ReportFile = t1.ReportFile,
                                      IsReportReassign = t1.IsReportReassign,
                                      Status = t1.Status
                                  }).ToList();

                    WorkStatusDetails model = new WorkStatusDetails
                    {
                        ApprovalMaster = master,
                        PoDetailsView = podetails,
                        WorkUpdates = workupdateslist,
                        ReportStatus = report
                    };
                    // return File(Path.Combine(@"c:\path", fileFromDB.FileNameOnDisk), MimeMapping.GetMimeMapping(fileFromDB.FileName), fileFromDB.FileName);


                    return await Task.Run(() => View(model));
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View());
        }

        public async Task<ActionResult> HoldWorkPlan(int planid, bool status)
        {
            try
            {
                var plan = new PlanningMaster { Id = planid, Status = status };
                using (var db = new xOssContext()) // use your DbConext
                {
                    db.PlanningMaster.Attach(plan);
                    db.Entry(plan).Property(x => x.Status).IsModified = true;
                    await db.SaveChangesAsync();

                    // sweet alert
                    Session["success_div"] = "true";
                    Session["success_msg"] = "Hold Successfully.";
                    return RedirectToAction("WorkPlanListForApproval");
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkPlanListForApproval");
        }
    }
}