﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ONSJobTracker.CustomizedFunction;

namespace ONSJobTracker.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            Session.Abandon();
            return View();
        }
        [HttpPost]
        public ActionResult Login(string loginId, string password, bool rememberme = false)
        {
            var user = new UserManager().IsValid(loginId, password);
            if (user != null)
            {
                HttpCookie cookie = new HttpCookie("UserInfo");
                string userid = EncryptDecrypt.Encrypt(user.UserId.ToString());
                string employeeid = EncryptDecrypt.Encrypt(user.EmployeeId.ToString());
                cookie.Values.Add("UserId", userid);
                cookie.Values.Add("EmployeeId", employeeid);
                cookie.Values.Add("UserName",user.UserName);
                cookie.Values.Add("UserEmail",user.UserEmail);
                cookie.Values.Add("UserImage",user.UserImage);
                cookie.Values.Add("UserDesignation", user.UserDesignation);
                cookie.Values.Add("UserDesignationId", user.DesignationId.ToString());
                HttpContext.Response.Cookies.Add(cookie);
                Response.Cookies["UserInfo"].Expires = DateTime.Now.AddDays(rememberme ? 30 : 1);
                UserManager userManager = new UserManager();
                userManager.GetUserMenu();
                return RedirectToAction("Index", "Home");
            }
            ViewBag.message = "Login Failed!! Invalid Login ID or Password.";
            return View("Login");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            var c = new HttpCookie("UserInfo");
            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);
            return RedirectToAction("Login");
        }

        public ActionResult AccessDenied()
        {
            return View();
        }
    }
}