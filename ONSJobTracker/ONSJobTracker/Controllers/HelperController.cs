﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.UserEmployee;
using ONSJobTracker.ViewModels.Work_Plan;

namespace ONSJobTracker.Controllers
{
    public class HelperController : Controller
    {
        public JsonResult GetEmployeeDetails(int employeeid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var employee = (from t1 in db.EmployeeInfo
                                join t2 in db.EmployeeDepartment on t1.EmployeeDepartmentFk equals t2.Id
                                join t3 in db.EmployeeDesignation on t1.EmployeeDesignationFk equals t3.Id
                                where t1.Id == employeeid
                                select new EmployeeViewModel
                                {
                                    Id = t1.Id,
                                    Name = t1.Name,
                                    CId = t1.CId,
                                    Mobile = t1.Mobile,
                                    Email = t1.Email,
                                    Address = t1.Address,
                                    ImageLink = t1.ImageLink,
                                    EmployeeDepartment = t2.Name,
                                    EmployeeDesignation = t3.Name
                                }).FirstOrDefault();

                return Json(employee, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUserMenuRole(int userid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return Json(db.MenuRole.Where(x => x.UserFk == userid).ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SetUserMenuRole(int moduleid, int detailsmenuid, bool status, int userid)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                MenuRole model = new MenuRole();
                bool updatedstatus = true;
                string menuid = "";
                MenuRole selectSingle =
                    db.MenuRole.FirstOrDefault(x =>
                        x.UserFk == userid && x.MenuMasterFk == moduleid && x.MenuDetailsFk == detailsmenuid);
                // to remove  same key already exists in the ObjectStateManager
                ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                if (selectSingle != null)
                {
                    model.Id = selectSingle.Id;
                    model.UserFk = selectSingle.UserFk;
                    model.MenuMasterFk = selectSingle.MenuMasterFk;
                    model.MenuDetailsFk = selectSingle.MenuDetailsFk;
                    model.InsertBy = selectSingle.InsertBy;
                    model.InsertDate = selectSingle.InsertDate;
                    model.UpdateReady(status, DateTime.Now);
                    SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                        "EmployeeDesignation", "Update");
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    updatedstatus = model.Status;
                    menuid = model.MenuDetailsFk.ToString();

                }
                return Json(new { updatedstatus = updatedstatus, menuid = menuid }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetNoProjectByClient(int clientid)
        {
            using (var db = new xOssContext())
            {
                string str;
                db.Configuration.ProxyCreationEnabled = false;
                var count = db.PoMaster.Count(p => p.ClientInfoFk == clientid) + 1;
                if (count < 10)
                {
                    str = "00" + count;
                }
                else if (count < 100)
                {
                    str = "0" + count;
                }
                else
                {
                    str = count.ToString();
                }

                return Json(new { counter = str }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetMasterPo(int poid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var po = (from t1 in db.PoMaster
                          join t2 in db.ClientInfo on t1.ClientInfoFk equals t2.Id
                          join t3 in db.ClientInfo on t1.EndClientInfoFk equals t3.Id
                          where t1.Id == poid
                          select new
                          {
                              PoNumber = t1.PoNo,
                              ProjectCode = t1.ProjectCid,
                              ClientName = t2.Name,
                              EndClientName = t3.Name
                          }).ToList();
                return Json(po, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetDetailsPoByPoId(int poid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return Json(db.PoDetails.Where(x => x.PoMasterFk == poid).ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSiteByPoId(int poid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var cluster = (from t1 in db.PoDetails
                               where t1.PoMasterFk == poid && t1.Status
                               select new
                               {
                                   Id = t1.Id,
                                   Text = t1.Cid + " || " + t1.Name
                               }).ToList();
                return Json(cluster, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSiteDetails(int siteid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var site = (from t1 in db.PoDetails
                            join t2 in db.Cluster on t1.ClusterFk equals t2.Id
                            join t3 in db.WorkType on t1.WorkTypeFk equals t3.Id
                            join t4 in db.Region on t1.RegionFk equals t4.Id
                            join t5 in db.District on t4.DistrictFk equals t5.Id
                            where t1.Id == siteid
                            select new
                            {
                                SiteId = t1.Cid,
                                SiteName = t1.Name,
                                ClusterId = t2.ClusterCid,
                                ClusterName = t2.Name,
                                Option = t1.Option,
                                WorkType = t3.Name,
                                Region = t4.Name,
                                Address = t1.Address,
                                District = t5.Name,
                            }).FirstOrDefault();
                return Json(site, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetWorkPlanbyId(int siteid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var plan = (from t1 in db.Planning
                            join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                            join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                            join t4 in db.EmployeeInfo on t1.EmployeeInfoFk equals t4.Id
                            where t1.PoDetailsFk == siteid
                            select new WorkPlanViewModel
                            {
                                PoNo = t3.PoNo,
                                PoDetailsFk = t1.PoDetailsFk,
                                Sitename = t2.Cid + " || " + t2.Name,
                                EmployeeInfoFk = t1.EmployeeInfoFk,
                                Engineername = t4.Name + " || " + t4.CId,
                                StartDate = t1.StartDate /*SqlFunctions.DateName("day", t1.StartDate) + "/" + SqlFunctions.DateName("month", t1.StartDate) + "/" + SqlFunctions.DateName("year", t1.StartDate)*/,
                                FinishDate = t1.FinishDate /*SqlFunctions.DateName("day", t1.FinishDate) + "/" + SqlFunctions.DateName("month", t1.FinishDate) + "/" + SqlFunctions.DateName("year", t1.FinishDate)*/,
                                Remarks = t1.Remarks,
                                Ta = t1.Ta,
                                Da = t1.Da,
                                Duration = t1.Duration
                            }).ToList();
                return Json(plan, JsonRequestBehavior.AllowGet);
            }
        }

        // Displaying Client Name 
        public JsonResult GetClientInfo(int projectId)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var workdelivery = (from t1 in db.PoMaster
                                    join t2 in db.ClientInfo on t1.ClientInfoFk equals t2.Id
                                    where t1.Id == projectId
                                    select new
                                    {
                                        ClientName = t2.Name
                                    }).FirstOrDefault();
                return Json(workdelivery, JsonRequestBehavior.AllowGet);
            }
        }

        // Displaying Client Name 
        public JsonResult GetSiteInfo(int cid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var workdelivery = (from t1 in db.PoDetails
                                    join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                    join t3 in db.ClientInfo on t2.ClientInfoFk equals t3.Id
                                    where t1.Id == cid
                                    select new
                                    {
                                        PO = t2.PoNo,
                                        SiteName = t1.Name,
                                        ClientName = t3.Name,
                                        PoValue = t2.PoValue,
                                        Price = t1.UnitPrice
                                    }).FirstOrDefault();
                return Json(workdelivery, JsonRequestBehavior.AllowGet);
            }
        }

        //Generate Invoice Number
        public JsonResult GenerateInvNumByPo()
        {
            using (var db = new xOssContext())
            {
                string str;
                db.Configuration.ProxyCreationEnabled = false;
                var count = db.InvoiceMaster.Count() + 1;
                //var po = db.PoMaster.Where(p => p.Id == poid).Select(x => x.PoNo).SingleOrDefault();
                if (count < 10)
                {
                    str = "ONS-B" + "-00000" + count;
                }
                else if (count < 100)
                {
                    str = "ONS-B" + "-0000" + count;
                }
                else if (count < 1000)
                {
                    str = "ONS-B" + "-000" + count;
                }
                else if (count < 10000)
                {
                    str = "ONS-B" + "-00" + count;
                }
                else if (count < 100000)
                {
                    str = "ONS-B" + "-0" + count;
                }
                else
                {
                    str = count.ToString();
                }

                return Json(new { counter = str }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPlanReferenceBySite(int siteid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var count = db.PlanningMaster.Count(x => x.PoDetailsFk == siteid) + 1;
                var workdelivery = (from t1 in db.PoDetails
                                    join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                                    where t1.Id == siteid
                                    select new
                                    {
                                        Reference = "P/" + t2.ProjectCid + "/" + t2.PoNo + "/" + t1.Cid + "/" + t1.Name + "-" + count
                                    }).FirstOrDefault();
                return Json(workdelivery, JsonRequestBehavior.AllowGet);
            }
        }

        //Get Site List according to PO Id

        public JsonResult GetSitesByPoId(int poid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                return Json(db.PoDetails.Where(x => x.PoMasterFk == poid).ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        //Load Plan Reference No according to PoMaster Select
        public JsonResult GetPlanRefList(int poId)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var typelist = (from t1 in db.PlanningMaster
                                join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                                join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                                where t3.Id == poId
                                select new
                                {
                                    Id = t1.Id,
                                    PlanReference = t1.PlanReference
                                }).ToList();
                return Json(typelist, JsonRequestBehavior.AllowGet);
            }
        }

        //Bind data according to Plan Reference ------
        public JsonResult GetPlanInfo(int planid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var workdelivery = (from t1 in db.Planning
                                    join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                    join t3 in db.PoDetails on t2.PoDetailsFk equals t3.Id
                                    join t4 in db.PoMaster on t3.PoMasterFk equals t4.Id
                                    join t5 in db.ClientInfo on t4.ClientInfoFk equals t5.Id
                                    where t2.Id == planid
                                    select new
                                    {
                                        //Id = t3.Id,
                                        SiteName = t3.Name,
                                        ClientName = t5.Name
                                    }).Distinct();
                return Json(workdelivery, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetFieldEng(int sid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                //var fe = (from t1 in db.PlanningMaster
                //          join t2 in db.Planning on t1.Id equals t2.PlanningMasterFk
                //          join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id
                //          where t2.PoDetailsFk == sitefk && t2.Type!=2
                //          select new
                //          {
                //              EmployeeName = t3.Name
                //          }).ToList().Distinct();

                Int64 siteid = db.ReportEngineerWorkStatus.Where(x => x.Id == sid).Select(x => x.SiteFk).Single();
                int lastplanid = int.Parse(db.Planning.Where(x => x.Id == siteid).OrderByDescending(p => p.Id)
                    .Select(r => r.PlanningMasterFk).First().ToString());
                var fe = (from t1 in db.WorkUpdate
                          join t2 in db.WorkTeamMember on t1.Id equals t2.WorkUpdateFk
                          join t3 in db.EmployeeInfo on t2.EmployeeFk equals t3.Id
                          where t1.PlanningMasterFk == lastplanid
                          select new
                          {
                              EmployeeName = t3.Name
                          }).ToList().Distinct();
                return Json(fe, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetFieldEngName(int sitefk)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;

                int lastplanid = int.Parse(db.Planning.Where(x => x.PoDetailsFk == sitefk).OrderByDescending(x => x.Id)
                    .Select(x => x.PlanningMasterFk).FirstOrDefault().ToString());
                var fe = (from t1 in db.WorkUpdate
                          join t2 in db.WorkTeamMember on t1.Id equals t2.WorkUpdateFk
                          join t3 in db.EmployeeInfo on t2.EmployeeFk equals t3.Id
                          where t1.PlanningMasterFk == lastplanid
                          select new
                          {
                              EmployeeName = t3.Name
                          }).ToList().Distinct();
                return Json(fe, JsonRequestBehavior.AllowGet);
            }
        }
        // retrieve Report Engineer from Plan.....
        public JsonResult GetReportEng(int sitefk)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var fe = (from t1 in db.PlanningMaster
                          join t2 in db.Planning on t1.Id equals t2.PlanningMasterFk
                          join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id
                          join t4 in db.EmployeeDesignation on t3.EmployeeDesignationFk equals t4.Id
                          where t2.Type == 2
                          select new
                          {
                              EmployeeName = t3.Name,
                              Designation = t4.Name
                          }).ToList().Distinct();
                return Json(fe, JsonRequestBehavior.AllowGet);
            }
        }

        // retrieve Plan Id from Plan.....
        public JsonResult GetPlanId(int sitefk)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var plan = (from t1 in db.Planning
                            join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                            join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                            join t4 in db.Project on t3.ProjectFk equals t4.Id
                            join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                            join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                            where t1.PoDetailsFk == sitefk && t5.Id == employeeId
                            orderby t1.Id descending
                            select new
                            {
                                Id = t1.Id
                            }).FirstOrDefault();

                return Json(plan, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult UpdateTaDaAmount(string type, int id, decimal amount)
        {
            switch (type)
            {
                case "Ta":
                    {
                        var planning = new Planning { Id = id, TaAmount = amount };
                        using (var db = new xOssContext())
                        {
                            db.Planning.Attach(planning);
                            db.Entry(planning).Property(x => x.TaAmount).IsModified = true;
                            db.SaveChanges();
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                case "Da":
                    {
                        var planning = new Planning { Id = id, DaAmount = amount };
                        using (var db = new xOssContext())
                        {
                            db.Planning.Attach(planning);
                            db.Entry(planning).Property(x => x.DaAmount).IsModified = true;
                            db.SaveChanges();
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSiteMilestones(int siteid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var milestone = (from t1 in db.SiteMilestone
                                 join t2 in db.PayementMilestone on t1.PayementMilestoneFk equals t2.Id
                                 where t1.PoDetailsFk == siteid
                                 select new
                                 {
                                     Id = t1.Id,
                                     Name = t2.Name,
                                     Percent = t1.MilestonePercentage,
                                     MilestoneId = t1.PayementMilestoneFk
                                 }).OrderByDescending(o => o.Id).ToList();
                return Json(milestone, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAcceptanceMilestone(int siteid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var dataset = (from t1 in db.SiteMilestone
                               join t2 in db.PayementMilestone on t1.PayementMilestoneFk equals t2.Id
                               where t1.PoDetailsFk == siteid
                               select new
                               {
                                   Id = t2.Id,
                                   Name = t2.Name
                               }).ToList();
                var exclude = (from t1 in db.AccetanceMilestone
                               join t2 in db.PayementMilestone on t1.PayementMilestoneFk equals t2.Id
                               join t3 in db.AcceptanceStatus on t1.AcceptanceStatussFk equals t3.Id
                               join t4 in db.PoDetails on t3.SiteFk equals t4.Id
                               where t3.SiteFk == siteid
                               select new
                               {
                                   Id = t2.Id,
                                   Name = t2.Name
                               }).ToList();
                var results = dataset.Where(i => exclude.All(e => i.Id != e.Id));

                return Json(results, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSitesByInvoice(int poid)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = (from t1 in db.AcceptanceStatus
                                join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                                where t1.PoFk == poid
                                select new
                                {
                                    value = t2.Id,
                                    text = t2.Name + " || " + t2.Cid
                                }).Distinct();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.value.ToString(), Text = type.text });
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMilestoneForInvoiceBySite(int siteid)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var list = from t1 in db.AccetanceMilestone
                           join t2 in db.AcceptanceStatus on t1.AcceptanceStatussFk equals t2.Id
                           join t3 in db.PayementMilestone on t1.PayementMilestoneFk equals t3.Id
                           where t2.SiteFk == siteid
                           select new
                           {
                               value = t3.Id,
                               text = t3.Name
                           };
                foreach (var data in list)
                {
                    result.Add(new SelectListItem { Value = data.value.ToString(), Text = data.text });
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInvoiceAmountByMilestone(int siteid, int milestoneid)
        {
            using (var db = new xOssContext())
            {
                decimal result = 0;
                var data = (from t1 in db.SiteMilestone
                            join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                            where t1.PoDetailsFk == siteid && t1.PayementMilestoneFk == milestoneid
                            select new
                            {
                                SiteValue = t2.UnitPrice,
                                Percentage = t1.MilestonePercentage
                            }).FirstOrDefault();

                if (data != null)
                {
                    result = (data.SiteValue * data.Percentage) / 100;
                }
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetPoByProjectId(int pid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var po = (from t1 in db.PoMaster
                          join t2 in db.ClientInfo on t1.ClientInfoFk equals t2.Id
                          where t1.ProjectFk == pid
                          select new
                          {
                              Id = t1.Id,
                              Text = t1.PoNo + " (" + t2.Name + ")"
                          }).ToList();
                return Json(po, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPoByProjectIdOfAssignedEmployee(int pid)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var polist = (from t1 in db.Planning
                              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                              join t4 in db.Project on t3.ProjectFk equals t4.Id
                              join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                              join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                              where t4.Id == pid && t5.Id == employeeId
                              select new
                              {
                                  value = t3.Id,
                                  text = t3.PoNo
                              }).Distinct().ToList();
                return Json(polist, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSitePoIdOfAssignedEmployee(int poid)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var polist = (from t1 in db.Planning
                              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                              join t4 in db.Project on t3.ProjectFk equals t4.Id
                              join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                              join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                              where t3.Id == poid && t5.Id == employeeId
                              select new
                              {
                                  value = t2.Id,
                                  text = t2.Name
                              }).Distinct().ToList();
                return Json(polist, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPoByProjectIdOfAssignedReportEngineer(int pid)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var polist = (from t1 in db.Planning
                              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                              join t4 in db.Project on t3.ProjectFk equals t4.Id
                              join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                              join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                              where t4.Id == pid && t5.Id == employeeId && t1.Type == 2
                              select new
                              {
                                  value = t3.Id,
                                  text = t3.PoNo
                              }).Distinct().ToList();
                return Json(polist, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSiteByPoIdOfAssignedReportEngineer(int poid)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var planningsitelist = (from t1 in db.Planning
                                        join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                                        join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                                        join t4 in db.Project on t3.ProjectFk equals t4.Id
                                        join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                                        join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                                        where t3.Id == poid && t5.Id == employeeId && t1.Type == 2
                                        select new
                                        {
                                            value = t2.Id,
                                            text = t2.Name
                                        }).Distinct().ToList();

                var reportedsitelist = (from t1 in db.ReportEngineerWorkStatus
                                        join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                                        where t1.Status == false && t1.IsReportReassign == false && t1.IsDelivered
                                        select new
                                        {
                                            value = t2.Id,
                                            text = t2.Name
                                        }).ToList();

                var result = planningsitelist.Except(reportedsitelist).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLastPlanId(int sitefk)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var plan = (from t1 in db.Planning
                            join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                            where t1.PoDetailsFk == sitefk
                            orderby t1.Id descending
                            select new
                            {
                                Id = t1.PlanningMasterFk
                            }).FirstOrDefault();

                return Json(plan, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetPOForDelivery(int pid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var polist = (from t1 in db.ReportEngineerWorkStatus
                              join t2 in db.PoMaster on t1.PoFk equals t2.Id
                              join t3 in db.Project on t2.ProjectFk equals t3.Id
                              where t3.Id == pid && t1.Status == false
                              select new
                              {
                                  value = t2.Id,
                                  text = t2.PoNo
                              }).Distinct().ToList();
                return Json(polist, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSiteForDelivery(int poid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var sitelist = (from t1 in db.ReportEngineerWorkStatus
                                join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                                where t1.PoFk == poid && t1.Status == false
                                group t1.Id by t2.Name into g
                                select new
                                {
                                    value = g.Max(),
                                    text = g.Key
                                }).ToList();

                //var site= sitelist.GroupBy(x=>x.text).Select(g=>g.)
                return Json(sitelist, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetReportEngineerBySite(int id)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var data = (from t1 in db.ReportEngineerWorkStatus
                            join t2 in db.User on t1.InsertBy equals t2.Id
                            join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id
                            where t1.Id == id
                            select new
                            {
                                Engineer = t3.Name + "( " + t3.CId + " )",
                                EmployeeInfoFk = t3.Id
                            }).FirstOrDefault();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetPOForAcceptance(int poid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var polist = (from t1 in db.ReportEngineerWorkStatus
                              join t2 in db.PoMaster on t1.PoFk equals t2.Id
                              join t3 in db.Project on t2.ProjectFk equals t3.Id
                              where t3.Id == poid && t1.IsReadyForAcceptance
                              select new
                              {
                                  value = t2.Id,
                                  text = t2.PoNo
                              }).Distinct().ToList();
                return Json(polist, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSitesByPoIdforAcceptance(int pid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var sitelist = (from t1 in db.ReportEngineerWorkStatus
                                join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                                where t1.PoFk == pid && t1.IsReadyForAcceptance
                                select new
                                {
                                    value = t2.Id,
                                    text = t2.Name
                                }).Distinct().ToList();
                return Json(sitelist, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ChangeWorkStatus(int planid, int status,int sitefk)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var latestworkstatus = db.WorkUpdate.Where(x => x.PlanningMasterFk == planid && x.SiteFk==sitefk).OrderByDescending(x => x.Id).FirstOrDefault();
                ((IObjectContextAdapter)db).ObjectContext.Detach(latestworkstatus);
                var workupdate = new WorkUpdate() { Id = latestworkstatus.Id, WorkStatusFk = status };
                db.WorkUpdate.Attach(workupdate);
                db.Entry(workupdate).Property(x => x.WorkStatusFk).IsModified = true;
                int success=db.SaveChanges();
                if (success>0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ChangeReportStatus(int planid, int status,int sitefk)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var latestreportstatus = db.ReportEngineerWorkStatus.Where(x => x.PlanningMasterFk == planid && x.SiteFk==sitefk).OrderByDescending(x => x.Id).FirstOrDefault();
                ((IObjectContextAdapter)db).ObjectContext.Detach(latestreportstatus);
                var reportupdate = new ReportEngineerWorkStatus() { Id = latestreportstatus.Id, ReportStatusFk = status };
                db.ReportEngineerWorkStatus.Attach(reportupdate);
                db.Entry(reportupdate).Property(x => x.ReportStatusFk).IsModified = true;
                int success = db.SaveChanges();
                if (success > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult ChangeTeamLeaderStatus(int planid, int status)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var masterplan = new PlanningMaster() { Id = planid, TeamLeaderStatus = status };
                db.PlanningMaster.Attach(masterplan);
                db.Entry(masterplan).Property(x => x.TeamLeaderStatus).IsModified = true;
                int success = db.SaveChanges();
                if (success > 0)
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetCarByVendorsstId(int vid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var po = (from t1 in db.CarInfo
                          join t2 in db.VendorInfo on t1.VendorInfoFk equals t2.Id
                          where t1.VendorInfoFk == vid
                          select new
                          {
                              Id = t1.Id,
                              Text = t1.CarNo + " (" + t1.DriverName + ")"
                          }).ToList();
                return Json(po, JsonRequestBehavior.AllowGet);
            }
        }

    }
}