﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.Purchase_Order;
using ONSJobTracker.ViewModels.Work_Plan;

namespace ONSJobTracker.Controllers
{
    [SessionCheck]
    public class PurchaseOrderController : Controller
    {
        #region Project

        public async Task<ActionResult> ProjectIndex()
        {
            using (var db = new xOssContext())
            {
                ViewBag.allproject = (from t1 in db.Project
                                      join t2 in db.ClientInfo on t1.ClientInfoFk equals t2.Id
                                      join t3 in db.EmployeeInfo on t1.EmployeeInfoFk equals t3.Id
                                      select new ProjectViewModel
                                      {
                                          Project = t1,
                                          ClientInfo = t2,
                                          EmployeeInfo = t3
                                      }).OrderByDescending(x => x.Project.Id).ToList();
                ViewBag.allregularclient = new SelectList(DropDownListHelper.GetClientInfo(1), "Value", "Text");
                ViewBag.allemployee = new SelectList(DropDownListHelper.GetEngineer(3), "Value", "Text");
                Project model = new Project
                {
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now
                };

                return await Task.Run(() => View(model));
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateProject(Project model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.Project.Any(o => o.ProjectName.ToLower() == model.ProjectName.ToLower()))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Project Name exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.Project.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Insert Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.Project.Any(x => x.ProjectName == model.ProjectName && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Project Name exists.";
                        }
                        else
                        {
                            Project selectSingle = db.Project.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Project", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("ProjectIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ProjectIndex"));
        }

        public async Task<ActionResult> DeleteProject(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    Project selectSingle = db.Project.Find(id);
                    if (selectSingle != null)
                    {
                        db.Project.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Project", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("ProjectIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("ProjectIndex");
        }
        #endregion

        #region Purchase Order Master

        public async Task<ActionResult> PoMasterIndex(int id)
        {

            ViewBag.allregularclient = new SelectList(DropDownListHelper.GetClientInfo(1), "Value", "Text");
            ViewBag.allendclient = new SelectList(DropDownListHelper.GetClientInfo(), "Value", "Text");
            ViewBag.allprojecttype = new SelectList(DropDownListHelper.GetProjectType(), "Value", "Text");
            PoMasterViewModel model = new PoMasterViewModel();
            ViewBag.allmasterpo = model.GetallPoMaster(id);
            ViewBag.projectcountedid = model.GetProjectCountedId();
            PoMaster mastermodel = new PoMaster { ProjectFk = id, StartDate = DateTime.Today, EndDate = DateTime.Today };
            return await Task.Run(() => View(mastermodel));
        }

        [HttpPost]
        public async Task<ActionResult> CreatePoMaster(PoMaster model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {

                        var data = db.Project.FirstOrDefault(x => x.Id == model.ProjectFk);
                        if (data != null) model.ClientInfoFk = data.ClientInfoFk;
                        model.AddReady(true, DateTime.Now);

                        db.PoMaster.Add(model);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Inserst Successfully.";

                    }
                    else if (model.Action == "Update")
                    {
                        PoMaster selectSingle =
                             db.PoMaster.FirstOrDefault(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;
                            model.ClientInfoFk = selectSingle.ClientInfoFk;
                            model.EndClientInfoFk = selectSingle.EndClientInfoFk;
                            model.ProjectCid = selectSingle.ProjectCid;
                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                "PoMaster", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";

                        }

                    }
                }
                return RedirectToAction("PoMasterIndex", new { id = model.ProjectFk });
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("PoMasterIndex", new { id = model.ProjectFk });
        }

        public async Task<ActionResult> DeletePoMaster(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    PoMaster selectSingle = db.PoMaster.Find(id);
                    if (selectSingle != null)
                    {
                        db.PoMaster.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PoMaster", "Delete");
                        db.PoDetails.RemoveRange(db.PoDetails.Where(x => x.PoMasterFk == id));
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("PoMasterIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("PoMasterIndex");
        }

        //public async Task<ActionResult> DetailsPoMaster(int id)
        //{
        //    PoMasterViewModel model = new PoMasterViewModel();
        //    PoDetailsViewModel model1 = new PoDetailsViewModel();
        //    ViewBag.masterpo = model.GetPoMasterbyId(id);
        //    ViewBag.alldetailsPobyId = model1.GetallPoDetailsById(id);
        //    return await Task.Run(() => View());
        //}

        #endregion

        #region Purchase Order Details

        public async Task<ActionResult> PoDetailsIndex(int id)
        {
            ViewBag.allworktype = new SelectList(DropDownListHelper.GetWorkType(), "Value", "Text");
            ViewBag.allcluster = new SelectList(DropDownListHelper.GetCluster(), "Value", "Text");
            ViewBag.allregion = new SelectList(DropDownListHelper.GetRegion(), "Value", "Text");
            ViewBag.allworkstation = new SelectList(DropDownListHelper.GetWorkStation(), "Value", "Text");
            ViewBag.paymentmilesote = new SelectList(DropDownListHelper.GetPaymentMilstoneList(), "Value", "Text");
            PoDetailsViewModel model = new PoDetailsViewModel();
            PoMasterViewModel model1 = new PoMasterViewModel();
            ViewBag.masterpo = model1.GetPoMasterbyId(id);
            ViewBag.alldetailpo = model.GetallPoDetailsById(id);
            return await Task.Run(() => View());
        }

        [HttpPost]
        public async Task<ActionResult> CreatePoDetails(PoDetails model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    var data = JsonConvert.DeserializeObject<List<SiteMilestoneViewModel>>(model.SiteMilestone);
                    if (model.Action == "Save")
                    {

                        model.AddReady(true, DateTime.Now);
                        db.PoDetails.Add(model);
                        db.SaveChanges();
                        if (data != null && data.Any())
                        {
                            SiteMilestone siteMilestone = new SiteMilestone();
                            foreach (var viewModel in data)
                            {
                                siteMilestone.PoDetailsFk = model.Id;
                                siteMilestone.MilestonePercentage = viewModel.Percent;
                                siteMilestone.PayementMilestoneFk = viewModel.PayementMilestoneFk;
                                siteMilestone.AddReady(true, DateTime.Now);
                                db.SiteMilestone.Add(siteMilestone);
                                await db.SaveChangesAsync();
                            }
                        }
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Inserst Successfully.";
                    }
                    else if (model.Action == "Update")
                    {
                        db.SiteMilestone.RemoveRange(db.SiteMilestone.Where(c => c.PoDetailsFk == model.Id));
                        db.SaveChanges();
                        PoDetails selectSingle =
                             db.PoDetails.FirstOrDefault(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data),
                                "PoDetails", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            if (data != null && data.Any())
                            {
                                SiteMilestone siteMilestone = new SiteMilestone();
                                foreach (var viewModel in data)
                                {
                                    siteMilestone.PoDetailsFk = selectSingle.Id;
                                    siteMilestone.MilestonePercentage = viewModel.Percent;
                                    siteMilestone.PayementMilestoneFk = viewModel.PayementMilestoneFk;
                                    siteMilestone.AddReady(true, DateTime.Now);
                                    db.SiteMilestone.Add(siteMilestone);
                                    db.SaveChanges();
                                }
                            }
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }
                    }
                }
                return RedirectToAction("PoDetailsIndex", new { id = model.PoMasterFk });
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("PoMasterIndex"));
        }

        public async Task<ActionResult> DeletePoDetails(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    db.SiteMilestone.RemoveRange(db.SiteMilestone.Where(c => c.PoDetailsFk == id));
                    PoDetails selectSingle = db.PoDetails.Find(id);
                    if (selectSingle != null)
                    {
                        db.PoDetails.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PoDetails", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("PoDetailsIndex", new { id = selectSingle.PoMasterFk });
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("PoMasterIndex");
        }

        #endregion

    }
}