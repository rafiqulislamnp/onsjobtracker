﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using ONSJobTracker.CustomizedFunction;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels.Settings;

namespace ONSJobTracker.Controllers
{
    [SessionCheck]
    public class SettingController : Controller
    {

        #region Work Type

        public async Task<ActionResult> WorkTypeIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allworktypes = db.WorkType.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateWorkType(WorkType model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.WorkType.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Work Type exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.WorkType.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Insert Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.WorkType.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Work Type exists.";
                        }
                        else
                        {
                            WorkType selectSingle = db.WorkType.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkType", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }
                return RedirectToAction("WorkTypeIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("WorkTypeIndex"));
        }

        public async Task<ActionResult> DeleteWorkType(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    WorkType selectSingle = db.WorkType.Find(id);
                    if (selectSingle != null)
                    {
                        db.WorkType.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkType", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("WorkTypeIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("WorkTypeIndex");
        }

        #endregion

        #region Project Type

        public async Task<ActionResult> ProjectTypeIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allProjecttypes = db.ProjectType.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateProjectType(ProjectType model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.ProjectType.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Project Type exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.ProjectType.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.ProjectType.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Project Type exists.";
                        }
                        else
                        {
                            ProjectType selectSingle = db.ProjectType.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ProjectType", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }

                return RedirectToAction("ProjectTypeIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ProjectTypeIndex"));
        }

        public async Task<ActionResult> DeleteProjectType(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    ProjectType selectSingle = db.ProjectType.Find(id);
                    if (selectSingle != null)
                    {
                        db.ProjectType.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ProjectType", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "deleted Successfully";
                        return RedirectToAction("ProjectTypeIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("ProjectTypeIndex");
        }

        #endregion

        #region Project Status


        public async Task<ActionResult> ProjectStatusIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allProjectstatus = db.ProjectStatus.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateProjectStatus(ProjectStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.ProjectStatus.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Project Status exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.ProjectStatus.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.ProjectStatus.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Project Status exists.";
                        }
                        else
                        {
                            ProjectStatus selectSingle = db.ProjectStatus.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {

                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ProjectStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                db.SaveChanges();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }

                    }
                }

                return RedirectToAction("ProjectStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ProjectStatusIndex"));
        }

        public async Task<ActionResult> DeleteProjectStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    ProjectStatus selectSingle = db.ProjectStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.ProjectStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ProjectStatus", "Delete");
                        db.SaveChanges();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("ProjectStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ProjectStatusIndex");
        }


        #endregion

        #region Cluster Setings

        public async Task<ActionResult> ProjectClusterIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allCluster = db.Cluster.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateCluster(Cluster model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.Cluster.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Cluster exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.Cluster.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.Cluster.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Cluster exists.";
                        }
                        else
                        {
                            Cluster selectSingle = db.Cluster.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Cluster", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                db.SaveChanges();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("ProjectClusterIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ProjectClusterIndex"));
        }

        public async Task<ActionResult> DeleteCluster(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    Cluster selectSingle = db.Cluster.Find(id);
                    if (selectSingle != null)
                    {
                        db.Cluster.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Cluster", "Delete");
                        db.SaveChanges();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("ProjectClusterIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ProjectClusterIndex");
        }


        #endregion

        #region Region


        public async Task<ActionResult> ProjectRegionIndex()

        {
            using (var db = new xOssContext())
            {
                VmRegion vm = new VmRegion();
                ViewBag.GetDistrict = new SelectList(DropDownListHelper.GetDistrictList(), "Value", "Text");
                ViewBag.allRegions = vm.GetRegion(); //db.Region.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateRegion(Region model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {

                        model.AddReady(true, DateTime.Now);
                        db.Region.Add(model);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Inserst Successfully.";

                    }
                    else if (model.Action == "Update")
                    {

                        Region selectSingle = db.Region.FirstOrDefault(x => x.Id == model.Id);

                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {
                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Region", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            db.SaveChanges();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }

                    }
                }
                return RedirectToAction("ProjectRegionIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ProjectRegionIndex"));
        }

        public async Task<ActionResult> DeleteRegion(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    Region selectSingle = db.Region.Find(id);
                    if (selectSingle != null)
                    {
                        db.Region.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "Region", "Delete");
                        db.SaveChanges();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("ProjectRegionIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ProjectRegionIndex");
        }



        #endregion

        #region Work Status


        public async Task<ActionResult> ProjectWorkStatusIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allWorkStatus = db.WorkStatus.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateWorkStatus(WorkStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.WorkStatus.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Work Status exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.WorkStatus.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.WorkStatus.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Work Status exists.";
                        }
                        else
                        {
                            WorkStatus selectSingle = db.WorkStatus.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                db.SaveChanges();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("ProjectWorkStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ProjectWorkStatusIndex"));
        }

        public async Task<ActionResult> DeleteWorkStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    WorkStatus selectSingle = db.WorkStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.WorkStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("ProjectWorkStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ProjectWorkStatusIndex");
        }


        #endregion

        #region Report Status

        public async Task<ActionResult> ReportStatusIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allReportStatus = db.ReportStatus.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateReportStatus(ReportStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.ReportStatus.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Report Status exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.ReportStatus.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.ReportStatus.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Report Status exists.";
                        }
                        else
                        {
                            ReportStatus selectSingle = db.ReportStatus.FirstOrDefault(x => x.Id == model.Id);

                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ReportStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("ReportStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ReportStatusIndex"));
        }

        public async Task<ActionResult> DeleteReportStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    ReportStatus selectSingle = db.ReportStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.ReportStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ReportStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("ReportStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ReportStatusIndex");
        }


        #endregion

        #region PAC Status

        public async Task<ActionResult> PacStatusIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allPac = db.PacStatus.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreatePacStatus(PacStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.PacStatus.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "PAC Status exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.PacStatus.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.PacStatus.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "PAC Status exists.";
                        }
                        else
                        {
                            PacStatus selectSingle = db.PacStatus.FirstOrDefault(x => x.Id == model.Id);

                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PacStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("PacStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("PacStatusIndex"));
        }

        public async Task<ActionResult> DeletePacStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    PacStatus selectSingle = db.PacStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.PacStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PacStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("PacStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("PacStatusIndex");
        }



        #endregion

        #region FAC Status


        public async Task<ActionResult> FacStatusIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allFac = db.FacStatus.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateFacStatus(FacStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.FacStatus.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "FAC Status exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.FacStatus.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.FacStatus.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "FAC Status exists.";
                        }
                        else
                        {
                            FacStatus selectSingle = db.FacStatus.FirstOrDefault(x => x.Id == model.Id);

                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "FacStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("FacStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("FacStatusIndex"));
        }

        public async Task<ActionResult> DeleteFacStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    FacStatus selectSingle = db.FacStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.FacStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "FacStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("FacStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("FacStatusIndex");
        }



        #endregion

        #region Invoice Status


        public async Task<ActionResult> InvoiceStatusIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allInvoice = db.InvoiceStatus.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateInvoiceStatus(InvoiceStatus model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.InvoiceStatus.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Invoice Status exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.InvoiceStatus.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.InvoiceStatus.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Invoice Status exists.";
                        }
                        else
                        {
                            InvoiceStatus selectSingle = db.InvoiceStatus.FirstOrDefault(x => x.Id == model.Id);

                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "InvoiceStatus", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("InvoiceStatusIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("InvoiceStatusIndex"));
        }

        public async Task<ActionResult> DeleteInvoiceStatus(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    InvoiceStatus selectSingle = db.InvoiceStatus.Find(id);
                    if (selectSingle != null)
                    {
                        db.InvoiceStatus.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "InvoiceStatus", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("InvoiceStatusIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("InvoiceStatusIndex");
        }


        #endregion

        #region PaymentMilestone


        public async Task<ActionResult> PaymentMilestoneIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.paymentmilestone = db.PayementMilestone.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreatePaymentMilestone(PayementMilestone model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.PayementMilestone.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Invoice Status exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.PayementMilestone.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.PayementMilestone.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Invoice Status exists.";
                        }
                        else
                        {
                            PayementMilestone selectSingle = db.PayementMilestone.FirstOrDefault(x => x.Id == model.Id);

                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PayementMilestone", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("PaymentMilestoneIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("PaymentMilestoneIndex"));
        }

        public async Task<ActionResult> DeletePaymentMilestone(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    PayementMilestone selectSingle = db.PayementMilestone.Find(id);
                    if (selectSingle != null)
                    {
                        db.PayementMilestone.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "PayementMilestone", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("PaymentMilestoneIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("InvoiceStatusIndex");
        }


        #endregion

        #region Client Type

        public async Task<ActionResult> ClientInfoIndex()

        {

            VmClientInfo vm = new VmClientInfo();
            ViewBag.ClientType = new SelectList(DropDownListHelper.GetClientTypeList(), "Value", "Text");
            ViewBag.allClientInfo = vm.ClientInfos();//db.ClientInfo.OrderBy(x => x.Id).ToList();
            return await Task.Run(() => View());

        }

        [HttpPost]
        public async Task<ActionResult> CreateClientInfo(ClientInfo model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.ClientInfo.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Client Name exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.ClientInfo.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.ClientInfo.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Client Name exists.";
                        }
                        else
                        {
                            ClientInfo selectSingle = db.ClientInfo.FirstOrDefault(x => x.Id == model.Id);

                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ClientInfo", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("ClientInfoIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("ClientInfoIndex"));
        }

        public async Task<ActionResult> DeleteClientInfo(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    ClientInfo selectSingle = db.ClientInfo.Find(id);
                    if (selectSingle != null)
                    {
                        db.ClientInfo.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "ClientInfo", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("ClientInfoIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("ClientInfoIndex");
        }


        #endregion

        #region Work Station

        public async Task<ActionResult> WorkStationIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allWorkStatus = db.WorkStation.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateWorkStation(WorkStation model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {
                        if (db.WorkStation.Any(o => o.Name.ToLower() == model.Name.ToLower()))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Work Station exists.";
                        }
                        else
                        {
                            model.AddReady(true, DateTime.Now);
                            db.WorkStation.Add(model);
                            await db.SaveChangesAsync();

                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Inserst Successfully.";
                        }
                    }
                    else if (model.Action == "Update")
                    {
                        if (db.WorkStation.Any(x => x.Name == model.Name && x.Id != model.Id))
                        {
                            //sweet alert
                            Session["warning_div"] = "true";
                            Session["warning_msg"] = "Work Station exists.";
                        }
                        else
                        {
                            WorkStation selectSingle = db.WorkStation.FirstOrDefault(x => x.Id == model.Id);
                            // to remove  same key already exists in the ObjectStateManager
                            ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                            if (selectSingle != null)
                            {
                                model.InsertBy = selectSingle.InsertBy;
                                model.InsertDate = selectSingle.InsertDate;

                                model.UpdateReady(true, DateTime.Now);
                                SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkStation", "Update");
                                db.Entry(model).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                                //sweet alert
                                Session["success_div"] = "true";
                                Session["success_msg"] = "Updated Successfully.";
                            }
                        }
                    }
                }
                return RedirectToAction("WorkStationIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return await Task.Run(() => View("WorkStationIndex"));
        }

        public async Task<ActionResult> DeleteWorkStation(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    WorkStation selectSingle = db.WorkStation.Find(id);
                    if (selectSingle != null)
                    {
                        db.WorkStation.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "WorkStation", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["warning_div"] = "true";
                        Session["warning_msg"] = "Deleted Successfully";
                        return RedirectToAction("WorkStationIndex");
                    }
                }
            }
            catch (Exception e)
            {
                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further problem, Contact with Concern People.";
            }
            return RedirectToAction("WorkStationIndex");
        }


        #endregion

        #region Vehicle Info
        public async Task<ActionResult> VendorInfoIndex()

        {
            using (var db = new xOssContext())
            {
                ViewBag.allvendors = db.VendorInfo.OrderBy(x => x.Id).ToList();
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateVendorInfo(VendorInfo model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {

                        model.AddReady(true, DateTime.Now);
                        db.VendorInfo.Add(model);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";

                    }
                    else if (model.Action == "Update")
                    {

                        VendorInfo selectSingle = db.VendorInfo.FirstOrDefault(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "VendorInfo", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }


                    }
                }
                return RedirectToAction("VendorInfoIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("VendorInfoIndex"));
        }

        public async Task<ActionResult> DeleteVendorInfo(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    VendorInfo selectSingle = db.VendorInfo.Find(id);
                    if (selectSingle != null)
                    {
                        db.VendorInfo.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "VendorInfo", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("VendorInfoIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("VendorInfoIndex");
        }


        public async Task<ActionResult> CarInfoIndex()
        {
            using (var db = new xOssContext())
            {
                var data = (from t1 in db.CarInfo
                            join t2 in db.VendorInfo on t1.VendorInfoFk equals t2.Id
                            select new CarInfoVm
                            {
                               CarInfo=t1,
                               VendorName=t2.Name
                            }).ToList();

                ViewBag.allcars = data;
                ViewBag.GetVendors = new SelectList(DropDownListHelper.GetVendorList(), "Value", "Text");
                return await Task.Run(() => View());
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateCarInfo(CarInfo model)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    if (model.Action == "Save")
                    {

                        model.AddReady(true, DateTime.Now);
                        db.CarInfo.Add(model);
                        await db.SaveChangesAsync();

                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Insert Successfully.";

                    }
                    else if (model.Action == "Update")
                    {

                        CarInfo selectSingle = db.CarInfo.FirstOrDefault(x => x.Id == model.Id);
                        // to remove  same key already exists in the ObjectStateManager
                        ((IObjectContextAdapter)db).ObjectContext.Detach(selectSingle);
                        if (selectSingle != null)
                        {

                            model.InsertBy = selectSingle.InsertBy;
                            model.InsertDate = selectSingle.InsertDate;

                            model.UpdateReady(true, DateTime.Now);
                            SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "CarInfo", "Update");
                            db.Entry(model).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            //sweet alert
                            Session["success_div"] = "true";
                            Session["success_msg"] = "Updated Successfully.";
                        }


                    }
                }
                return RedirectToAction("CarInfoIndex");
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return await Task.Run(() => View("CarInfoIndex"));
        }

        public async Task<ActionResult> DeleteCarInfo(int id)
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    CarInfo selectSingle = db.CarInfo.Find(id);
                    if (selectSingle != null)
                    {
                        db.CarInfo.Remove(selectSingle);
                        SaveLog.Save(db, new JavaScriptSerializer().Serialize(Json(selectSingle).Data), "CarInfo", "Delete");
                        await db.SaveChangesAsync();
                        //sweet alert
                        Session["success_div"] = "true";
                        Session["success_msg"] = "Delete Successfully.";
                        return RedirectToAction("CarInfoIndex");
                    }
                }
            }
            catch (Exception e)
            {

                //sweet alert
                Session["warning_div"] = "true";
                Session["warning_msg"] = "Oops! Please, try again. For further probolem, Contact with Concern People.";
            }
            return RedirectToAction("CarInfoIndex");
        }

        #endregion
    }
}