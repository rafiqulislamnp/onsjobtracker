﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.MenuDetails
{
    public class MenuDetailsViewModel
    {
        public Int64 Id { get; set; }
        public string ModuleName { get; set; }
        public Int64 ModuleId { get; set; }
        public string MenuType { get; set; }
        public string MenuName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool IsMenu { get; set; }
        public int MenuSl { get; set; }
        private xOssContext db = new xOssContext();

        public List<MenuDetailsViewModel> GetAllUserMenuDetails()
        {
            var menu = (from t1 in db.MenuDetails
                join t2 in db.MenuMaster on t1.MenuMasterFk equals t2.Id
                select new MenuDetailsViewModel
                {
                    Id = t1.Id,
                    ModuleName = t2.ModuleName,
                    ModuleId = t2.Id,
                    MenuType = t1.MenuType,
                    MenuName = t1.MenuName,
                    ControllerName = t1.ControllerName,
                    ActionName = t1.ActionName,
                    IsMenu = t1.IsMenu,
                    MenuSl = t1.MenuSl,
                }).OrderByDescending(x => x.Id).ToList();
            return menu;
        }
    }
}