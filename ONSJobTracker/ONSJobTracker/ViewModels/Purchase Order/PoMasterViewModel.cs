﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Purchase_Order
{
    public class PoMasterViewModel
    {
        public Int64 Id { get; set; }
        public string ProjectId { get; set; }
        public string PoNo { get; set; }
        public Int64 ProjectTypeId { get; set; }
        public string ProjectType { get; set; }
        public Int64 RegularClientId { get; set; }
        public string ClientName { get; set; }
        public Int64 EndClientId { get; set; }
        public string EndClientName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal PoValue { get; set; }
        public string Remarks { get; set; }

        private xOssContext _db = new xOssContext();

        public IEnumerable<PoMasterViewModel> GetallPoMaster(int id)
        {
            var data = (from t1 in _db.PoMaster
                join t2 in _db.ClientInfo on t1.ClientInfoFk equals t2.Id
                join t3 in _db.ClientInfo on t1.EndClientInfoFk equals t3.Id
                join t4 in _db.ProjectType on t1.ProjectTypeFk equals t4.Id
                where t1.ProjectFk==id orderby t1.Id
                select new PoMasterViewModel
                {
                    Id = t1.Id,
                    PoNo = t1.PoNo,
                    PoValue = t1.PoValue,
                    ProjectId = t1.ProjectCid,
                    Remarks = t1.Remarks,
                    RegularClientId = t2.Id,
                    ClientName = t2.Name,
                    EndClientId = t3.Id,
                    EndClientName = t3.Name,
                    ProjectTypeId = t4.Id,
                    ProjectType = t4.Name,
                    StartDate = t1.StartDate,
                    EndDate = t1.EndDate
                }).AsEnumerable();
            return data;
        }

        public string GetProjectCountedId()
        {
            try
            {
                using (var db = new xOssContext()) // use your DbConext
                {
                    var count = db.PoMaster.Count(p=>p.InsertDate.Value.Year.ToString()==DateTime.Now.Year.ToString()) + 1;
                    if (count < 10)
                    {
                        return "00" + count;
                    }
                    if (count < 100)
                    {
                        return "0" + count;
                    }
                    return count.ToString();
                }

            }
            catch (Exception e)
            {
                return "N/A";
            }

        }
        public PoMasterViewModel GetPoMasterbyId(int id)
        {
            var data = (from t1 in _db.PoMaster
                join t2 in _db.ClientInfo on t1.ClientInfoFk equals t2.Id
                join t3 in _db.ClientInfo on t1.EndClientInfoFk equals t3.Id
                join t4 in _db.ProjectType on t1.ProjectTypeFk equals t4.Id
                where t1.Id == id
                orderby t1.Id
                select new PoMasterViewModel
                {
                    Id = t1.Id,
                    PoNo = t1.PoNo,
                    PoValue = t1.PoValue,
                    ClientName = t2.Name,
                    ProjectType = t4.Name
                }).FirstOrDefault();
            return data;
        }
    }
}