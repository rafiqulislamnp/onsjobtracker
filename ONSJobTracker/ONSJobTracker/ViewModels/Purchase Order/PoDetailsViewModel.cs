﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Purchase_Order
{
    public class SiteMilestoneViewModel
    {
        public int CounterId { get; set; }
        public Int64 PayementMilestoneFk { get; set; }
        public string PayementMilestone { get; set; }
        public decimal Percent { get; set; }
    }
    public class PoDetailsViewModel
    {
        public Int64 Id { get; set; }
        public string Cid { get; set; }
        public string Name { get; set; }
        public Int64 PoMasterFk { get; set; }
        public string PoNo { get; set; }
        public Int64 WorkTypeFk { get; set; }
        public string WorkType { get; set; }
        public decimal UnitPrice { get; set; }
        public Int64 ClusterFk { get; set; }
        public string ClusterName { get; set; }
        public Int64 RegionFk { get; set; }
        public string Region { get; set; }
        public Int64 WorkStationFk { get; set; }
        public string WorkStation { get; set; }
        public string Address { get; set; }
        public string Option { get; set; }
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string ClusterId { get; set; }
        public string District { get; set; }
        public string Remarks { get; set; }

        public IEnumerable<SiteMilestoneViewModel> SiteMilestones { get; set; }

        xOssContext db = new xOssContext();

        public IEnumerable<PoDetailsViewModel> PoDetailsList()
        {
            var poDetails = (from t1 in db.PoDetails
                             join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                             join t3 in db.WorkType on t1.WorkTypeFk equals t3.Id
                             join t4 in db.Cluster on t1.ClusterFk equals t4.Id
                             join t5 in db.Region on t1.RegionFk equals t5.Id
                             join t6 in db.WorkStation on t1.WorkStationFk equals t6.Id

                             orderby t1.Id
                             select new PoDetailsViewModel
                             {
                                 Id = t1.Id,
                                 Cid = t1.Cid,
                                 Name = t1.Name,
                                 PoMasterFk = t1.PoMasterFk,
                                 PoNo = t2.PoNo,
                                 WorkTypeFk = t1.WorkTypeFk,
                                 WorkType = t3.Name,
                                 UnitPrice = t1.UnitPrice,
                                 ClusterFk = t1.ClusterFk,
                                 ClusterName = t4.Name,
                                 RegionFk = t1.RegionFk,
                                 Region = t5.Name,
                                 WorkStationFk = t1.WorkStationFk,
                                 WorkStation = t6.Name,
                                 Address = t1.Address,
                                 Option = t1.Option,
                                 Remarks = t1.Remarks,


                             }).AsEnumerable();
            return poDetails;
        }

        //----get Po Details According to Id
        public IEnumerable<PoDetailsViewModel> GetallPoDetailsById(int id)
        {
            var poDetails = (from t1 in db.PoDetails
                             join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                             join t3 in db.WorkType on t1.WorkTypeFk equals t3.Id
                             join t4 in db.Cluster on t1.ClusterFk equals t4.Id
                             join t5 in db.Region on t1.RegionFk equals t5.Id
                             join t6 in db.WorkStation on t1.WorkStationFk equals t6.Id
                             where t1.PoMasterFk == id
                             orderby t1.Id
                             select new PoDetailsViewModel
                             {
                                 Id = t1.Id,
                                 Cid = t1.Cid,
                                 Name = t1.Name,
                                 PoMasterFk = t1.PoMasterFk,
                                 PoNo = t2.PoNo,
                                 WorkTypeFk = t1.WorkTypeFk,
                                 WorkType = t3.Name,
                                 UnitPrice = t1.UnitPrice,
                                 ClusterFk = t1.ClusterFk,
                                 ClusterName = t4.Name,
                                 RegionFk = t1.RegionFk,
                                 Region = t5.Name,
                                 WorkStationFk = t1.WorkStationFk,
                                 WorkStation = t6.Name,
                                 Address = t1.Address,
                                 Option = t1.Option,
                                 Remarks = t1.Remarks,
                                 SiteMilestones = (from t7 in db.SiteMilestone
                                                   join t8 in db.PayementMilestone on t7.PayementMilestoneFk equals t8.Id
                                                   where t7.PoDetailsFk == t1.Id
                                                   select new SiteMilestoneViewModel
                                                   {
                                                       PayementMilestoneFk = t7.Id,
                                                       PayementMilestone = t8.Name,
                                                       Percent = t7.MilestonePercentage,
                                                       CounterId = 0
                                                   }).ToList()
                             }).ToList();
            return poDetails;
        }

        public PoDetailsViewModel GetSiteDetails(int siteid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var site = (from t1 in db.PoDetails
                            join t2 in db.Cluster on t1.ClusterFk equals t2.Id
                            join t3 in db.WorkType on t1.WorkTypeFk equals t3.Id
                            join t4 in db.Region on t1.RegionFk equals t4.Id
                            join t5 in db.District on t4.DistrictFk equals t5.Id
                            where t1.Id == siteid
                            select new PoDetailsViewModel
                            {
                                SiteId = t1.Cid,
                                SiteName = t1.Name,
                                ClusterId = t2.ClusterCid,
                                ClusterName = t2.Name,
                                Option = t1.Option,
                                WorkType = t3.Name,
                                Region = t4.Name,
                                Address = t1.Address,
                                District = t5.Name,
                            }).FirstOrDefault();
                return site;
            }

        }
    }
}