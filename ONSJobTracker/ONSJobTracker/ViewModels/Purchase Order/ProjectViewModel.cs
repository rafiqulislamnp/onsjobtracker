﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Purchase_Order
{
    public class ProjectViewModel
    {
        public Project Project { get; set; }
        public ClientInfo ClientInfo { get; set; }
        public EmployeeInfo EmployeeInfo { get; set; }
    }
}