﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ONSJobTracker.ViewModels
{
    public class MenuModels
    {
        public Int64 ModuleId { get; set; }
        public string ModuleName { get; set; }
        public Int64 MenuId { get; set; }
        public string MenuName { get; set; }
        public int MenuSl { get; set; }
        public string Controller { get; set; }
        public string ActionResult { get; set; }
        public bool IsMenu { get; set; }

    }

    public class LoginUser
    {
        public Int64 UserId { get; set; }
        public Int64 EmployeeId { get; set; }
        public Int64 DesignationId { get; set; }
        public string UserName { get; set; }
        public string UserDesignation { get; set; }
        public string UserImage { get; set; }
        public string UserEmail { get; set; }
    }
}