﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ONSJobTracker.ViewModels.Report
{
    public class SummaryReportVm
    {
        public string Customer { get; set; }
        public string Project { get; set; }
        public string WorkType { get; set; }
        public decimal UnitValue { get; set; }
        public string PaymentMilestone { get; set; }
        public decimal MilestonePercent { get; set; }
        public int WithoutPoQty { get; set; }
        public int WaitingAcceptanceQty { get; set; }
        public int AcceptedQty { get; set; }
        public int InvoicedQty { get; set; }
        public decimal InvoicedAmount { get; set; }
        public int PaymetRecievedQty { get; set; }
        public decimal PaidAmount { get; set; }
        public int NotCompletedQty { get; set; }
        public Int64 WorkTypeId { get; set; }
    }
    public class ExpensePaymentVm
    {
        public string Employee { get; set; }
        public Int64 EmployeeId { get; set; }
        public decimal Expense { get; set; }
        public decimal PaidAmount { get; set; }
    }

    public class ExpenseDetails
    {
        public decimal Expense { get; set; }
        public string ProjectName { get; set; }
        public string PoNo { get; set; }
        public string SiteName { get; set; }
        public string CostTitle { get; set; }
    }

    public class EmployeeExpense
    {
        public List<ExpenseDetails> ExpenseDetails { get; set; }
        public DateTime? ExpenseDate { get; set; }
    }

    public class CarStatusReport
    {
        public string VendorName { get; set; }
        public string CarNo { get; set; }
        public Int64 CarId { get; set; }
        public int? Milage { get; set; }
    }

    public class SpecificCarInfo
    {
        public string Project { get; set; }
        public int? Milage { get; set; }
        public string Location { get; set; }
    }
    public class SpecificCarMilageInfo
    {
        public string Project { get; set; }
        public int? Milage { get; set; }
        public string Location { get; set; }
        public DateTime Date { get; set; }
        public List<string> Engineer { get; set; }
    }

    public class OverallPMWorkStatus
    {
        public string PoNo { get; set; }
        public string WorkType { get; set; }
        public Int64 SiteId { get; set; }
        public string SiteCId { get; set; }
        public string SiteName { get; set; }
        public string ClusterId { get; set; }
        public string ClientName { get; set; }
        public string EndClientName { get; set; }
        public List<string> FieldEngineer { get; set; }
        public string FieldWorkStatus { get; set; }
        public string ReportEngineer { get; set; }
        public string ReportStatus { get; set; }
        public bool DeliveryStatus { get; set; }
        public int AcceptanceMilestones { get; set; }
        public int TotalMilestones { get; set; }
        public DateTime AcceptanceDate { get; set; }
    }

    public class AccountStatusOfProject
    {
        public Int64 SIteId { get; set; }
        public string SiteName { get; set; }
        public string SiteCId { get; set; }
        public string PoNo { get; set; }
        public string Project { get; set; }
        public Int64 InvoiceStatus { get; set; }
        public DateTime InvoiceDate { get; set; }
        public Int64 PaymentStatus { get; set; }
        public DateTime PaymentDate { get; set; }
    }
}