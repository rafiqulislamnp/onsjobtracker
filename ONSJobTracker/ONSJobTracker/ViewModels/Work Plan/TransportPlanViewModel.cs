﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Work_Plan
{
    public class TransportPlanViewModel
    {
        public Int64 Id { get; set; }
        public Int64 PlanMasterFk { get; set; }
        public string PlanMaster { get; set; }
        public string TransportCid { get; set; }
        public int EstimatedDuration { get; set; }
        public int CarInfo { get; set; }
        public string FuelType { get; set; }
        public string EstimatedKm { get; set; }
        public string TotalCarCost { get; set; }

        public List<TransportPlanViewModel> AllTransportPlan()
        {
            using (var db = new xOssContext())
            {
                var data = (from t1 in db.Transport
                    join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                    orderby t1.Id  
                    select new TransportPlanViewModel
                    {
                        Id = t1.Id,
                        PlanMasterFk=t2.Id,
                        PlanMaster=t2.PlanReference,
                        TransportCid=t1.TransportCid,
                        CarInfo= t1.CarInfo,
                        EstimatedKm= t1.EstimatedKm,
                        FuelType= t1.FuelType,
                        TotalCarCost= t1.TotalCarCost,
                        EstimatedDuration= t1.EstimatedDuration,
                    }).ToList();
                return data;
            }
        }
    }

    public class TransportModelForPayment
    {
        public Transport Transport { get; set; }
        public List<TaDaAmountViewModel> TransportPayment { get; set; }
    }
}