﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Work_Plan
{

    public class WorkPlanViewModel
    {
        public int CounterId { get; set; }
        public string PoNo { get; set; }
        public Int64 PoDetailsFk { get; set; }
        public string Sitename { get; set; }
        public Int64 EmployeeInfoFk { get; set; }
        public string Engineername { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime StartDate { get; set; }
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime FinishDate { get; set; }
        public string Remarks { get; set; }
        public bool Ta { get; set; }
        public bool Da { get; set; }
        public bool OutsideTa { get; set; }
        public bool OutsideDa { get; set; }
        public int Duration { get; set; }
        public int Type { get; set; }
        public string ResourceType { get; set; }
        public string GetPlanReference()
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var count = db.PlanningMaster.Count() + 1;
                //var workdelivery = (from t1 in db.PoDetails
                //    join t2 in db.PoMaster on t1.PoMasterFk equals t2.Id
                //    where t1.Id == siteid
                //    select new
                //    {
                //        Reference = "P/" + t2.ProjectCid + "/" + t2.PoNo + "/" + t1.Cid + "/" + t1.Name + "-" + count
                //    }).FirstOrDefault();
                string reference = "P/" + count+" ("+DateTime.Now.ToString("dd MMM yyyy")+")";
                return reference;
            }
        }

        public decimal GetTaAmountByEmployeeDesignation(Int64 employeeid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var amount = (from t1 in db.EmployeeInfo
                              join t2 in db.EmployeeDesignation on t1.EmployeeDesignationFk equals t2.Id
                              where t1.Id == employeeid
                              select new
                              {
                                  TaAmount = t2.Ta
                                  
                              }).FirstOrDefault();
                if (amount!=null)
                {
                    return amount.TaAmount;
                }
                return 0;
            }
        }
        public decimal GetDaAmountByEmployeeDesignation(Int64 employeeid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var amount = (from t1 in db.EmployeeInfo
                    join t2 in db.EmployeeDesignation on t1.EmployeeDesignationFk equals t2.Id
                    where t1.Id == employeeid
                    select new
                    {
                        DaAmount = t2.Da

                    }).FirstOrDefault();
                if (amount != null)
                {
                    return amount.DaAmount;
                }
                return 0;
            }
        }
        public decimal GetOutsideTaAmountByEmployeeDesignation(Int64 employeeid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var amount = (from t1 in db.EmployeeInfo
                    join t2 in db.EmployeeDesignation on t1.EmployeeDesignationFk equals t2.Id
                    where t1.Id == employeeid
                    select new
                    {
                        TaAmount = t2.OutsideTa

                    }).FirstOrDefault();
                if (amount != null)
                {
                    return amount.TaAmount;
                }
                return 0;
            }
        }
        public decimal GetOutsideDaAmountByEmployeeDesignation(Int64 employeeid)
        {
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var amount = (from t1 in db.EmployeeInfo
                    join t2 in db.EmployeeDesignation on t1.EmployeeDesignationFk equals t2.Id
                    where t1.Id == employeeid
                    select new
                    {
                        DaAmount = t2.OutsideDa

                    }).FirstOrDefault();
                if (amount != null)
                {
                    return amount.DaAmount;
                }
                return 0;
            }
        }
    }

    public class PlanListViewModel
    {
        public Int64 SiteId { get; set; }
        public string Site{ get; set; }
        public string Project { get; set; }
    }

    public class WorkPlanApprovalViewModel
    {
        public Planning Planning { get; set; }
        public Transport Transport { get; set; }
    }

    public class TransportViewModel
    {
        public int CounterId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string Remarks { get; set; }
        public int Duration { get; set; }
        public int CarInfo { get; set; }
        public string EstimatedKm { get; set; }
        public string FuelType { get; set; }
        public string TotalCarCost { get; set; }
        public decimal DailyCost { get; set; }
        public string ResourceType { get; set; }
    }

    public class OtherCostViewModel
    {
        public int CounterId { get; set; }
        public string CostName { get; set; }
        public decimal Amount { get; set; }
        public string Remarks { get; set; }
        public string ResourceType { get; set; }
    }

    public class TaDaAmountViewModel
    {
        public DateTime PaymentDate { get; set; }
        public decimal PaidAmount { get; set; }
        public string Remarks { get; set; }
        public string EmployeeName { get; set; }
    }
}