﻿using ONSJobTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.ViewModels.Work_Plan;
using ONSJobTracker.ViewModels.Purchase_Order;
using ONSJobTracker.ViewModels.Status;

namespace ONSJobTracker.ViewModels.Work_Plan
{
    public class WorkStatusViewModel
    {
        public Int64 SiteId { get; set; }
        public string PlanmasterName { get; set; }
        public Int64 PlanningMasterId { get; set; }
        public string ProjectName { get; set; }
        public string PO { get; set; }
        public string SiteName { get; set; }
        public int WorkStatus { get; set; }
        public int ReportStatus { get; set; }
        public int TeamLeaderStatus { get; set; }
        public int Type { get; set; }
    }

    public class WorkStatusDetails {

        public List<ApprovalMaster> ApprovalMaster { get; set; }
        public List<PoDetailsViewModel> PoDetailsView { get; set; }
        public List<WorkUpdateViewModel> WorkUpdates { get; set; }
        public List<ReportEngineerWorkStatusViewModel> ReportStatus { get; set; }
    }

}