﻿using ONSJobTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ONSJobTracker.ViewModels.Work_Plan
{
    public class WorkUpdateViewModel
    {
        public Int64 Id { get; set; }
        public string Project { get; set; }
        public string PoNo { get; set; }
        public string Site { get; set; }
        public string WorkStatus { get; set; }
        public Int64 WorkStatusId { get; set; }
        public DateTime Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Problem { get; set; }
        public string Resolve { get; set; }
        public string Remarks { get; set; }
        public string Location { get; set; }

        public string DriverName { get; set; }
        public string DriverContact { get; set; }
        public string CarRegNo { get; set; }
        public int StartMilage { get; set; }
        public int EndMilage { get; set; }
        public string FileLocation { get; set; }

        public List<WorkTeamMemberViewModel> DailyExpense { get; set; }
    }
    public class WorkUpdateStausForAssignedEmp
    {
        public Int64 PlanningMasterFk { get; set; }
        public Int64 WorkStatusFk { get; set; }
        public Int64 SiteFk { get; set; }
    }
    public class ReportUpdateStausForAssignedEmp
    {
        public Int64 PlanningMasterFk { get; set; }
        public Int64 ReportStatusFk { get; set; }
        public Int64 SiteFk { get; set; }
    }
}