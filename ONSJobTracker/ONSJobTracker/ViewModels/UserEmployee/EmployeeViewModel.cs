﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.UserEmployee
{
    public class EmployeeViewModel
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
        public string CId { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string ImageLink { get; set; }
        public Int64 EmployeeDesignationId { get; set; }
        public string EmployeeDesignation { get; set; }
        public Int64 EmployeeDepartmentId { get; set; }
        public string EmployeeDepartment { get; set; }
        public bool Status { get; set; }


        public Int64 userId { get; set; }
        public string LoginId { get; set; }

        //public Int64 UserId { get; set; }
        //public string Password { get; set; }

        //[NotMapped] // Does not effect with your database
        //[Compare("Password")]
        //public string ConfirmPassword { get; set; }

        //xOssContext db = new xOssContext();

        //public EmployeeViewModel GetEmployee(int id)
        //{
        //    var emp = (from em in db.EmployeeInfo
        //               join u in db.User on em.Id equals u.EmployeeInfoFk
        //        join des in db.EmployeeDesignation on em.EmployeeDesignationFk equals des.Id
        //        join dept in db.EmployeeDepartment on em.EmployeeDepartmentFk equals dept.Id
        //        where em.Id == id
        //               select new EmployeeViewModel()
        //        {
        //                Id = em.Id, Name = em.Name, CId = em.CId, Mobile = em.Mobile, Address = em.Address, 
        //                Email = em.Email,ImageLink = em.ImageLink, UserId = u.Id,
        //                EmployeeDesignation = des.Name, EmployeeDesignationId = des.Id,
        //                  EmployeeDepartmentId = dept.Id, EmployeeDepartment = dept.Name
        //        }).FirstOrDefault();
        //    return emp;
        //}
    }

    public class UserViewModel
    {
        public Int64 Id { get; set; }
        public string Name { get; set; }
        public string CId { get; set; }
        public string LoginId { get; set; }
        public string ImageLink { get; set; }
        public string UserType { get; set; }
        public bool Status { get; set; }

    }
}