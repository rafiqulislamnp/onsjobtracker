﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Status
{
    public class ReportEngineerWorkStatusViewModel
    {
        public Int64 Id { get; set; }
         public Int64 PoFk { get; set; }  // Po Master Fk
         public string Po { get; set; }  // Po Master Fk
         public Int64 ProjectId { get; set; }  // Project Id---
         public string ProjectName { get; set; }  // Project Name---
      
        public Int64 SiteFk { get; set; }  // Planning Master Fk
        public string SiteName { get; set; }

        public Int64 ClientFk { get; set; }
        public string ClientName { get; set; }

        public Int64 WorkStatusFk { get; set; }
        public string WorkStatus { get; set; }
        
        public Int64 ReportStatusFk { get; set; }
        public string ReportStatus { get; set; }
        [DisplayName("Completed Date")]
        public DateTime CompletedDate { get; set; }
     
        public DateTime DeliveredDate { get; set; }
        public string Remarks { get; set; }
        public string ReportFile { get; set; }

        public bool Status { get; set; }
        public bool IsReportReassign { get; set; }
        public bool IsReadyForAcceptance { get; set; }

        xOssContext db = new xOssContext();

        public List<ReportEngineerWorkStatusViewModel> ListReportEngineer(int employeeId)
        {
            var list = (from t1 in db.ReportEngineerWorkStatus
                        join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                        join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                        join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                        join t5 in db.WorkStatus on t1.WorkStatusFk equals t5.Id
                        join t6 in db.ReportStatus on t1.ReportStatusFk equals t6.Id
                        join t7 in db.User on t1.InsertBy equals t7.Id
                        join t8 in db.EmployeeInfo on t7.EmployeeInfoFk equals t8.Id
                        join t9 in db.Project on t3.ProjectFk equals t9.Id
                        where t8.Id==employeeId 
                select new ReportEngineerWorkStatusViewModel
                {
                    Id = t1.Id, 
                    CompletedDate = t1.CompletedDate ?? new DateTime(1900,01,01),
                    DeliveredDate = t1.DeliveredDate,
                    Remarks = t1.Remarks,
                    PoFk = t3.Id,Po = t3.PoNo,
                    ProjectId = t9.Id, ProjectName = t9.ProjectName,
                    SiteFk = t2.Id, SiteName = t2.Name,
                    ClientFk = t4.Id, ClientName = t4.Name,
                    WorkStatusFk = t5.Id, WorkStatus = t5.Name,
                    ReportStatusFk = t6.Id, ReportStatus = t6.Name,
                    Status=t1.Status,
                    IsReportReassign=t1.IsReportReassign
                }).OrderByDescending(x=>x.Id).ToList();
            return list;
        }

        public List<ReportEngineerWorkStatusViewModel> ListWorkDelivery(int employeeId)
        {
            var list = (from t1 in db.ReportEngineerWorkStatus
                join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                join t5 in db.WorkStatus on t1.WorkStatusFk equals t5.Id
                join t6 in db.ReportStatus on t1.ReportStatusFk equals t6.Id
                join t7 in db.Project on t3.ProjectFk equals t7.Id
                where t7.EmployeeInfoFk == employeeId && t1.Status==false && t1.IsDelivered && t1.IsReadyForAcceptance==false
                select new ReportEngineerWorkStatusViewModel
                {
                    Id = t1.Id,
                    CompletedDate = t1.CompletedDate ?? new DateTime(1900, 01, 01),
                    DeliveredDate = t1.DeliveredDate,
                    Remarks = t1.Remarks,
                    PoFk = t3.Id,
                    Po = t3.PoNo,
                    ProjectId = t7.Id,
                    ProjectName = t7.ProjectName,
                    SiteFk = t2.Id,
                    SiteName = t2.Name,
                    ClientFk = t4.Id,
                    ClientName = t4.Name,
                    WorkStatusFk = t5.Id,
                    WorkStatus = t5.Name,
                    ReportStatusFk = t6.Id,
                    ReportStatus = t6.Name,
                    Status = t1.Status,
                    IsReportReassign = t1.IsReportReassign,
                    IsReadyForAcceptance = t1.IsReadyForAcceptance
                }).OrderByDescending(x => x.Id).ToList();

            var query = list
                .GroupBy(x => x.SiteFk,
                    (k, g) => g.Aggregate((a, x) => (x.Id > a.Id) ? x : a)).ToList();
            return query;
        }
        
    }
}