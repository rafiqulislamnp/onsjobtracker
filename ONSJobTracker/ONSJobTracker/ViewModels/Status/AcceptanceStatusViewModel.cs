﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Status
{
    public class AcceptanceStatusViewModel
    {
        public Int64 Id { get; set; }
        public Int64 SiteFk { get; set; }  // Po Details Id....
        public string Cid { get; set; }
        public string SiteName { get; set; }
        public string ClientName { get; set; }
        public Int64 PoID { get; set; }
        public string PoName { get; set; }       
        public string ProjectName { get; set; } 
        public Int64 ProjectId { get; set; } 
        public string Remarks { get; set; }
        public List<AcceptanceMilestoneVm> AcceptanceMilestoneVms { get; set; }
        xOssContext db = new xOssContext();

        public List<AcceptanceStatusViewModel> ListAcceptances()
        {
            var acceptances = (from t1 in db.AcceptanceStatus
                join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                join t5 in db.PoMaster on t2.PoMasterFk equals t5.Id
                join t6 in db.ClientInfo on t5.ClientInfoFk equals t6.Id
                join t7 in db.Project on t5.ProjectFk equals t7.Id
                orderby t1.Id
                select new AcceptanceStatusViewModel
                {
                    Id = t1.Id,
                    SiteFk = t2.Id,Cid = t2.Cid,SiteName = t2.Name,
                    PoID = t5.Id,
                    PoName = t5.PoNo,
                    ProjectName = t7.ProjectName,
                    ProjectId = t7.Id,
                    ClientName = t6.Name,
                    Remarks = t1.Remarks,
                    AcceptanceMilestoneVms= (from t3 in db.AccetanceMilestone
                                           join t4 in db.PayementMilestone on t3.PayementMilestoneFk equals t4.Id
                                           where t3.AcceptanceStatussFk==t1.Id
                                           select new AcceptanceMilestoneVm
                                           {
                                               AcceptancemMilestones= t4.Name
                                              
                                           }).ToList()
                }).ToList();
            return acceptances;
        }

        public List<ReportEngineerWorkStatusViewModel> ReadyForAcceptanceList()
        {
            var list = (from t1 in db.ReportEngineerWorkStatus
                join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                join t4 in db.ClientInfo on t3.ClientInfoFk equals t4.Id
                join t5 in db.WorkStatus on t1.WorkStatusFk equals t5.Id
                join t6 in db.ReportStatus on t1.ReportStatusFk equals t6.Id
                join t7 in db.Project on t3.ProjectFk equals t7.Id
                where t1.Status == false && t1.IsReadyForAcceptance
                select new ReportEngineerWorkStatusViewModel
                {
                    Id = t1.Id,
                    CompletedDate = t1.CompletedDate ?? new DateTime(1900, 01, 01),
                    DeliveredDate = t1.DeliveredDate,
                    Remarks = t1.Remarks,
                    PoFk = t3.Id,
                    Po = t3.PoNo,
                    ProjectId = t7.Id,
                    ProjectName = t7.ProjectName,
                    SiteFk = t2.Id,
                    SiteName = t2.Name,
                    ClientFk = t4.Id,
                    ClientName = t4.Name,
                    WorkStatusFk = t5.Id,
                    WorkStatus = t5.Name,
                    ReportStatusFk = t6.Id,
                    ReportStatus = t6.Name,
                    Status = t1.Status,
                    IsReportReassign = t1.IsReportReassign,
                    IsReadyForAcceptance = t1.IsReadyForAcceptance
                }).OrderByDescending(x => x.Id).ToList();
            return list;
        }
    }
    public class AcceptanceMilestoneVm
    {
        public string AcceptancemMilestones { get; set; }

    }
}