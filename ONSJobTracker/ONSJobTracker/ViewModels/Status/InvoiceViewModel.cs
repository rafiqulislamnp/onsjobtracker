﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Status
{
    public class InvoiceViewModel
    {
        public Int64 Id { get; set; }
        public Int64 SiteFk { get; set; }  // Po Details Id....
        public string SiteName { get; set; }
        public string ClientName { get; set; }
        public string Cid { get; set; }
        public string Po { get; set; }
        public decimal ProjectedInvoiceAmount { get; set; }
        public decimal OtherClaimAmount { get; set; }
        public decimal InvoicedAmount { get; set; }
        public Int64 InvoiceStatusFk { get; set; }
        public string InvoiceStatus { get; set; }
        public DateTime ProjectedPaymentDate { get; set; }
        public DateTime InvoiceSubmissionDate { get; set; }
        public string Remarks { get; set; }

        private xOssContext db = new xOssContext();

        //public List<InvoiceViewModel> ListInvoice()
        //{
        //    var list = (from t1 in db.Invoice
        //        join t2 in db.PoDetails on t1.SiteFk equals t2.Id
        //        join t3 in db.InvoiceStatus on t1.InvoiceStatusFk equals t3.Id
        //        join t5 in db.PoMaster on t2.PoMasterFk equals t5.Id
        //        join t6 in db.ClientInfo on t5.ClientInfoFk equals t6.Id
        //        orderby t1.Id
        //        select new InvoiceViewModel()
        //        {
        //            Id = t1.Id,
        //            SiteFk = t2.Id,
        //            SiteName = t2.Name,
        //            Po = t5.PoNo,
        //            ClientName = t6.Name,
        //            Remarks = t1.Remarks,
        //            ProjectedInvoiceAmount = t5.PoValue,
        //            OtherClaimAmount = t1.OtherClaimAmount,
        //            InvoicedAmount = t5.PoValue + t1.OtherClaimAmount,
        //            InvoiceSubmissionDate = t1.InvoiceSubmissionDate,
        //            ProjectedPaymentDate = t1.ProjectedPaymentDate,

        //            InvoiceStatusFk = t3.Id,InvoiceStatus = t3.Name,
        //        }).ToList();
        //    return list;
        //}
    }
}