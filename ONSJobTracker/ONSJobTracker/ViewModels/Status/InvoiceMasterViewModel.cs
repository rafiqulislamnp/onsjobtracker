﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Status
{
    public class InvoiceMasterViewModel
    {
        public Int64 Id { get; set; }
        public Int64 PoFk { get; set; }
        public string Po { get; set; }
        public Int64 InvoiceStatusFk { get; set; }
        public string InvoiceStatus { get; set; }
        public DateTime ProjectedPaymentDate { get; set; }
        public DateTime InvoiceSubmissionDate { get; set; }
        public DateTime InvoiceRecievedDate { get; set; }
        public string Remarks { get; set; }
        public string Cid { get; set; }
        public string BankDetails { get; set; }
        public bool IsPayment { get; set; }
        public bool Status { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal ReievedAmount { get; set; }
        xOssContext db = new xOssContext();

        public List<InvoiceMasterViewModel> GetInvoiceMaster()
        {
            var list = (from t1 in db.InvoiceMaster
                //join t2 in db.PoMaster on t1.PoFk equals t2.Id
                join t3 in db.InvoiceStatus on t1.InvoiceStatusFk equals t3.Id
                orderby t1.Id where t1.IsPayment == false
                select new InvoiceMasterViewModel
                {
                    Id= t1.Id, 
                    InvoiceSubmissionDate = t1.InvoiceSubmissionDate,
                    ProjectedPaymentDate = t1.ProjectedPaymentDate,
                    Cid = t1.Cid,Remarks = t1.Remarks,
                    TotalAmount=db.InvoiceDetail.Where(x=>x.InvoiceMasterFk==t1.Id).Select(x=>x.FinalAmount).DefaultIfEmpty(0).Sum(),
                    InvoiceStatusFk = t3.Id, InvoiceStatus = t3.Name,
                }).ToList();
            return list;
        }

        //public InvoiceMasterViewModel GetInvoiceMasterById(int id)
        //{
        //    var data = (from t1 in db.InvoiceMaster
        //       // join t2 in db.PoMaster on t1.PoFk equals t2.Id
        //        join t3 in db.InvoiceStatus on t1.InvoiceStatusFk equals t3.Id
        //        orderby t1.Id
        //                where t1.PoFk == id
        //        select new InvoiceMasterViewModel()
        //        {
        //            Id = t1.Id,
        //            InvoiceSubmissionDate = t1.InvoiceSubmissionDate,
        //            ProjectedPaymentDate = t1.ProjectedPaymentDate,
        //            Cid = t1.Cid,
        //            Remarks = t1.Remarks,
        //            PoFk = t2.Id,
        //            Po = t2.PoNo,
        //            InvoiceStatusFk = t3.Id,
        //            InvoiceStatus = t3.Name,
        //        }).FirstOrDefault();
        //    return data;
        //}


        public List<InvoiceMasterViewModel> GetInvoiceMasterPayment()
        {
            var list = (from t1 in db.InvoiceMaster
                //join t2 in db.PoMaster on t1.PoFk equals t2.Id
                join t3 in db.InvoiceStatus on t1.InvoiceStatusFk equals t3.Id
                join t4 in db.InvoiceDetail on t1.Id equals t4.InvoiceMasterFk
                orderby t1.Id
                        where t1.IsPayment == false
                select new InvoiceMasterViewModel
                {
                    Id = t1.Id,
                    InvoiceSubmissionDate = t1.InvoiceSubmissionDate,
                    ProjectedPaymentDate = t1.ProjectedPaymentDate,
                    Cid = t1.Cid, Remarks = t1.Remarks, IsPayment = t1.IsPayment,
                    InvoiceStatusFk = t3.Id, InvoiceStatus = t3.Name,
                    Status = t1.Status,
                    TotalAmount = db.InvoiceDetail.Where(x => x.InvoiceMasterFk == t1.Id).Select(x => x.FinalAmount).DefaultIfEmpty(0).Sum()
                }).ToList();
            return list;
        }

        public List<InvoiceMasterViewModel> ListPaidPayment()
        {
            var list = (from t1 in db.InvoiceMaster
                //join t2 in db.PoMaster on t1.PoFk equals t2.Id
                join t3 in db.InvoiceStatus on t1.InvoiceStatusFk equals t3.Id
                join t4 in db.InvoiceDetail on t1.Id equals t4.InvoiceMasterFk
                orderby t1.Id
                where t1.IsPayment
                select new InvoiceMasterViewModel
                {
                    Id = t1.Id,
                    InvoiceSubmissionDate = t1.InvoiceSubmissionDate,
                    ProjectedPaymentDate = t1.ProjectedPaymentDate,
                    Cid = t1.Cid,
                    Remarks = t1.Remarks,
                    IsPayment = t1.IsPayment,
                    ReievedAmount = t1.RecieveAmount,
                    BankDetails = t1.BankDetails,
                    InvoiceRecievedDate = t1.PaymentRecieveDate??DateTime.Now,
                    TotalAmount = db.InvoiceDetail.Where(x => x.InvoiceMasterFk == t1.Id).Select(x => x.FinalAmount).DefaultIfEmpty(0).Sum()
                }).ToList();
            return list;
        }
    }
}