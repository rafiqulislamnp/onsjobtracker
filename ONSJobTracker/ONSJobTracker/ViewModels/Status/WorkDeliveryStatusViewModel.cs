﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Status
{
    public class WorkDeliveryStatusViewModel
    {
        public Int64 Id { get; set; }
        public Int64 ProjectIdFk { get; set; } //Po Master Id
        public string ProjectId { get; set; }
        public Int64 SiteFk { get; set; }  // Po Details Id....
        public string SiteName { get; set; }
        public Int64 ClientFk { get; set; }
        public string ClientName { get; set; }
        public Int64 FieldEngineerFk { get; set; }
        public string FEng { get; set; }
        public Int64 ReportEngineerFk { get; set; }
        public string REng { get; set; }
        public Int64 WorkStatusFk { get; set; }
        public string WorkStatus { get; set; }
        public Int64 ReportStatusFk { get; set; }
        public string ReportStatus { get; set; }
        public DateTime CompletedDate { get; set; }
       
        public DateTime DeliveredDate { get; set; }
       
        public Int64 PacStatusFk { get; set; }
        public string PacStatus { get; set; }
        public Int64 FacStatusFk { get; set; }
        public string FacStatus { get; set; }
        public string Remarks { get; set; }

        xOssContext db = new xOssContext();

        public IEnumerable<WorkDeliveryStatusViewModel> ListWorkDelivery()
        {
            var list =
            (
                from t1 in db.WorkDeliveryStatus
                join t3 in db.PoDetails on t1.SiteFk equals t3.Id
                join t2 in db.PoMaster on t3.PoMasterFk equals t2.Id
                join t4 in db.ClientInfo on t2.ClientInfoFk equals t4.Id
                join t6 in db.EmployeeInfo on t1.ReportEngineerFk equals t6.Id
                join t7 in db.WorkStatus on t1.WorkStatusFk equals t7.Id
                join t8 in db.ReportStatus on t1.ReportStatusFk equals t8.Id
                orderby t1.Id
                select new WorkDeliveryStatusViewModel
                {
                    Id = t1.Id,
                    ProjectIdFk = t2.Id, ProjectId = t2.ProjectCid,
                    SiteFk = t3.Id, SiteName = t3.Name,
                    /*ClientFk = t4.Id,*/ ClientName = t4.Name,
                    //FieldEngineerFk = t5.Id, FEng = t5.Name,
                    ReportEngineerFk = t6.Id, REng = t6.Name,
                    WorkStatusFk = t7.Id, WorkStatus = t7.Name,
                    ReportStatusFk = t8.Id, ReportStatus = t8.Name,
                    CompletedDate = t1.CompletedDate, DeliveredDate = t1.DeliveredDate ?? DateTime.Now,
                    //PacStatusFk = t9.Id, PacStatus = t9.Name,
                    //FacStatusFk = t10.Id, FacStatus = t10.Name,
                    Remarks = t1.Remarks
                }).AsEnumerable();
            return list;
        }
    }
}