﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Status
{
    public class InvoiceDetailViewModel
    {
        public Int64 Id { get; set; }
        public Int64 InvoiceMasterFk { get; set; }
        public Int64 PoFk { get; set; }  // Po Details Id....
        public string Po { get; set; }

        public Int64 SiteFk { get; set; }  // Po Details Id....
        public string SiteName { get; set; }  
        public decimal SiteAmount { get; set; }
        public decimal OtherClaimAmount { get; set; }
        public decimal Tax { get; set; }
        public decimal Vat { get; set; }
        public decimal TotalAmount { get; set; }
        public List<AcceptanceMilestoneVm> AcceptanceMilestoneVms { get; set; }
        public string Remarks { get; set; }

        xOssContext db = new xOssContext();

        public List<InvoiceDetailViewModel> ListInvoiceDetails(int id)
        {
            var list = (from t1 in db.InvoiceDetail
                join t2 in db.InvoiceMaster on t1.InvoiceMasterFk equals t2.Id
                join t3 in db.PoDetails on t1.SiteFk equals t3.Id
                join t4 in db.PoMaster on t3.PoMasterFk equals t4.Id                
                where t1.InvoiceMasterFk == id
                orderby t1.Id
                select new InvoiceDetailViewModel
                {
                    Id= t1.Id, SiteAmount = t1.SiteAmount, 
                    OtherClaimAmount = t1.OtherClaimAmount, Remarks = t1.Remarks,
                    TotalAmount = t1.FinalAmount, Tax = t1.Tax, Vat = t1.Vat,
                    PoFk = t4.Id, Po = t4.PoNo,
                    SiteFk = t3.Id, SiteName = t3.Name,
                    AcceptanceMilestoneVms = (from t5 in db.InvoiceMilestone
                        join t6 in db.PayementMilestone on t5.PayementMilestoneFk equals t6.Id
                        where t5.InvoiceDetailsFk == t1.Id
                        select new AcceptanceMilestoneVm
                        {
                            AcceptancemMilestones = t6.Name

                        }).ToList()
                }).ToList();
            return list;
        }
    }
}