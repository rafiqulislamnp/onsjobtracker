﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Settings
{
    public class VmRegion
    {
        public Int64 RegionId { get; set; }
        public string Name { get; set; }
        public Int64 DistrictFk { get; set; }
        public string DistrictName{ get; set; }
        public string Remarks { get; set; }

        //public ICollection<VmRegion> VmRegions { get; set; }

        private xOssContext db = new xOssContext();

        public List<VmRegion> GetRegion()
        {
            var r = (from rg in db.Region
                join
                    d in db.District on rg.DistrictFk equals d.Id
                select new VmRegion()
                {
                    RegionId = rg.Id,
                    Name = rg.Name,
                    DistrictFk = d.Id,
                    DistrictName = d.Name,
                    Remarks = rg.Remarks
                }).ToList();
            return r;
        }
    }
}