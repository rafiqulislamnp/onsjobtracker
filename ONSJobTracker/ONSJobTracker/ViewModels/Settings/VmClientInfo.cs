﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.ViewModels.Settings
{
    public class VmClientInfo
    {
        public Int64 ClientInfoId { get; set; }
       public Int64 ClientTypeFk { get; set; } // END, REGULAR (in drop down)
        public string ClientType { get; set; }
        public string Name { get; set; }
        public string Cid { get; set; }
        public string ContactPerson { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Remarks { get; set; }

        xOssContext db = new xOssContext();
        public List<VmClientInfo> ClientInfos()
        {
            var clients = (from ci in db.ClientInfo
                join ct in db.ClientType on ci.ClientTypeFk equals ct.Id
                select new VmClientInfo()
                {
                    ClientInfoId = ci.Id,
                    Name = ci.Name,
                    Cid = ci.Cid,
                    ContactPerson = ci.ContactPerson,
                    Mobile = ci.Mobile,
                    Address = ci.Address,
                    Email = ci.Email,
                    Remarks = ci.Remarks,
                    ClientTypeFk = ct.Id,
                    ClientType = ct.Type
                }).ToList();
            return clients;
        }
    }
}