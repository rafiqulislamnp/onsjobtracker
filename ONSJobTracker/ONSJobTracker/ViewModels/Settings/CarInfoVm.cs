﻿using ONSJobTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ONSJobTracker.ViewModels.Settings
{
    public class CarInfoVm
    {
        public CarInfo CarInfo { get; set; }
        public string VendorName { get; set; }
    }
}