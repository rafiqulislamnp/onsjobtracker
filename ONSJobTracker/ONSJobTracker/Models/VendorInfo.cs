﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ONSJobTracker.Models
{
    public class VendorInfo:BaseModel
    {
        [DisplayName("Name")]
        [Required]
        public string Name { get; set; }

        [DisplayName("Contact No")]
        [Required]
        [MinLength(11)]
        [MaxLength(11)]
        public string ContactNo { get; set; }

        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }

    public class CarInfo : BaseModel
    {
        [DisplayName("Car No")]
        [Required]
        public string CarNo { get; set; }

        [DisplayName("Driver Name")]
        [Required]
        public string DriverName { get; set; }

        [DisplayName("Contact No")]
        [Required]
        [MinLength(11)]
        [MaxLength(11)]
        public string ContactNo { get; set; }

        [DisplayName("Enroll Date")]
        public DateTime EnrollDate { get; set; }

        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }


        [DisplayName("Vendor")]
        public Int64 VendorInfoFk { get; set; }
       
    }
}