﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ONSJobTracker.Models
{
    public class WorkType : BaseModel
    {
        [DisplayName("Work Type")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class ProjectType : BaseModel
    {
        [DisplayName("Project Type")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Project Id")]
        public string ProjectCid { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class ProjectStatus : BaseModel
    {
        [DisplayName("Project Status")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class Cluster : BaseModel
    {
        [DisplayName("Cluster Name")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Cluster Id")]
        public string ClusterCid { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class Region : BaseModel
    {
        [DisplayName("Region Name")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [DisplayName("District")]
        public int DistrictFk { get; set; }

        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class District : BaseModel
    {
        [DisplayName("District Name")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class WorkStatus : BaseModel
    {
        [DisplayName("Work Status")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class ReportStatus : BaseModel
    {
        [DisplayName("Report Status")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class PacStatus : BaseModel
    {
        [DisplayName("PAC Status")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class FacStatus : BaseModel
    {
        [DisplayName("FAC Status")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class InvoiceStatus : BaseModel
    {
        [DisplayName("Invoice Status")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class PayementMilestone : BaseModel
    {
        [DisplayName("Milestone")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }

    public class ClientType : BaseModel
    {
        [DisplayName("Type")]
        [MaxLength(10)]
        [Required]
        public string Type { get; set; } // END, REGULAR (in drop down)

        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class ClientInfo : BaseModel
    {
        [DisplayName("Type")]
        public Int64 ClientTypeFk { get; set; } // END, REGULAR (in drop down)

        [DisplayName("Name")]
        [MaxLength(150)]
        [Required]
        public string Name { get; set; }

        [DisplayName("Client ID")]
        public string Cid { get; set; }

        [DisplayName("Contact Person")]
        [MaxLength(150)]
        public string ContactPerson { get; set; }

        [DisplayName("Mobile")]
        [MaxLength(100)]
        public string Mobile { get; set; }

        [DisplayName("Address")]
        [MaxLength(200)]
        public string Address { get; set; }

        [DisplayName("Email")]
        [MaxLength(100)]
        public string Email { get; set; }

        [DisplayName("Others Information")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class WorkStation : BaseModel
    {
        [DisplayName("Work Station")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class PoMaster : BaseModel
    {
        [DisplayName("Project Name")]
        public Int64 ProjectFk { get; set; }
        [DisplayName("PO Number")]
        [MaxLength(100)]
        [Required]
        public string PoNo { get; set; }
        [DisplayName("PO Value")]
        public decimal PoValue { get; set; }
        public Int64 ClientInfoFk { get; set; }
        [DisplayName("End Client Name")]
        public Int64 EndClientInfoFk { get; set; }
        [DisplayName("Project Type")]
        public Int64 ProjectTypeFk { get; set; }
        [DisplayName("Project ID")]
        public string ProjectCid { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("End Date")]
        public DateTime EndDate { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }
    }
    public class PoDetails : BaseModel
    {
        [DisplayName("Site Id")]
        public string Cid { get; set; }
        [MaxLength(100)]
        [Required]
        [DisplayName("Site/Task")]
        public string Name { get; set; }
        [DisplayName("PO")]
        public Int64 PoMasterFk { get; set; }
        [DisplayName("Work Type")]
        public Int64 WorkTypeFk { get; set; }
        [DisplayName("Unit Price")]
        public decimal UnitPrice { get; set; }
        [DisplayName("Cluster")]
        public Int64 ClusterFk { get; set; }
        [DisplayName("Region")]
        public Int64 RegionFk { get; set; }
        [DisplayName("Work Station")]
        public Int64 WorkStationFk { get; set; }
        [DisplayName("Payement Milestone")]
        public Int64? PayementMilestoneFk { get; set; }
        [DisplayName("Milestone Percentage")]
        public decimal? MilestonePercentage { get; set; }
        [MaxLength(100)]
        public string Address { get; set; }
        public string Option { get; set; }
        [MaxLength(200)]
        public string Remarks { get; set; }
        [NotMapped]
        public string SiteMilestone { get; set; }
    }
    public class Project : BaseModel
    {
        [DisplayName("Project Name")]
        public string ProjectName { get; set; }
        [DisplayName("Client Name")]
        public Int64 ClientInfoFk { get; set; }

        [DisplayName("Project Manager")]
        public Int64 EmployeeInfoFk{ get; set; }

        [DisplayName("Project ID")]
        public string ProjectCid { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("End Date")]
        public DateTime EndDate { get; set; }
        public int Volume { get; set; }
    }
    public class SiteMilestone:BaseModel
    {
        public Int64 PoDetailsFk { get; set; }
        public Int64 PayementMilestoneFk { get; set; }
        public decimal MilestonePercentage{ get; set; }
    }
    public class AccetanceMilestone : BaseModel
    {
        public Int64 AcceptanceStatussFk { get; set; }
        public Int64 PayementMilestoneFk { get; set; }       
    }
}