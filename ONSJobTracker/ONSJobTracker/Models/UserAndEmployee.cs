﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ONSJobTracker.Models
{
    public class User : BaseModel
    {
        [DisplayName("Employee Id")]
        public Int64 EmployeeInfoFk { get; set; }

        [DisplayName("Login Id")]
        [Index("User_LoginId_Unique", IsUnique = true)]
        [MaxLength(100)]
        public string LoginId { get; set; }

        [DisplayName("Password")]
        [MaxLength(500)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [NotMapped]
        [MaxLength(500)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }

        [DisplayName("User Type")]
        [MaxLength(15)]
        public string UserType { get; set; } //     Admin/UserAdmin/User


    }
    public class EmployeeInfo : BaseModel
    {
        [DisplayName("Name")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [DisplayName("Employee ID")]
        [Required]
        public string CId { get; set; }


        [DisplayName("Mobile")]
        [MaxLength(11)]
        [MinLength(11)]
        [Required]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Mobile Number must be numeric")]
        public string Mobile { get; set; }

        [DisplayName("Address")]
        [MaxLength(200)]
        public string Address { get; set; }

        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(100)]
        [Index("EmployeeInfo_Email_Unique", IsUnique = true)]
        [Required]
        public string Email { get; set; }

        [DisplayName("Image")]
        [MaxLength(200)]
        public string ImageLink { get; set; }

        [DisplayName("Designation")]
        public Int64 EmployeeDesignationFk { get; set; }

        [DisplayName("Department")]
        public Int64 EmployeeDepartmentFk { get; set; }
    }
    public class EmployeeDesignation : BaseModel
    {
        
        [DisplayName("Designation")]
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
        public decimal Ta { get; set; }
        [DisplayName("Outside Ta")]
        public decimal OutsideTa { get; set; }
        public decimal Da { get; set; }
        [DisplayName("Outside Da")]
        public decimal OutsideDa { get; set; }

        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }

    }
    public class EmployeeDepartment : BaseModel
    {
        [DisplayName("Department")]
        [MaxLength(100)]
        public string Name { get; set; }
        [DisplayName("Remarks")]
        [MaxLength(200)]
        public string Remarks { get; set; }

    }
    public class MenuMaster : BaseModel
    {
        [MaxLength(100, ErrorMessage = "Please write the Module Name"), MinLength(4)]
        [DisplayName("Module Name")]
        public string ModuleName { get; set; }
        [DisplayName("Module Serial")]
        public int ModuleSl { get; set; }
    }
    public class MenuDetails : BaseModel
    {
        [DisplayName("Module Name")]
        public Int64 MenuMasterFk { get; set; }
        
       
        [DisplayName("Menu Type")]
        public string MenuType { get; set; }

        [MaxLength(100, ErrorMessage = "Please write the Fair Name"), MinLength(3)]
        [DisplayName("Menu Name")]
        public string MenuName { get; set; }
        [MaxLength(200, ErrorMessage = "Please write the Fair Name"), MinLength(3)]
        [DisplayName("Controller Name")]
        public string ControllerName { get; set; }
        [MaxLength(200, ErrorMessage = "Please write the Fair Name"), MinLength(3)]
        [DisplayName("Action Result")]
        public string ActionName { get; set; }
        public bool IsMenu { get; set; }
        [DisplayName("Menu Serial")]
        public int MenuSl { get; set; }
    }

    public class MenuRole : BaseModel
    {
        [Index("MenuRole_Unique", 1, IsUnique = true)]
        public Int64 UserFk { get; set; }


        [Index("MenuRole_Unique", 2, IsUnique = true)]
        public Int64 MenuMasterFk { get; set; }


        [Index("MenuRole_Unique", 3, IsUnique = true)]
        public Int64 MenuDetailsFk { get; set; }

    }
}