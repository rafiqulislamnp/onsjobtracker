﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ONSJobTracker.Models
{
    public class WorkDeliveryStatus: BaseModel
    {
        [NotMapped]
        [DisplayName("Project")]
        public Int64 ProjectFk { get; set; } //Po Master Id

        [DisplayName("PO.")]
        public Int64 PoFk { get; set; } //Po Master Id

        [DisplayName("Site")]
        public Int64 SiteFk { get; set; }  // Po Details Id....
        [DisplayName("Site Name")]
        [NotMapped]
        public string SiteName { get; set; }
        
        [DisplayName("Field Engineer")]
        public Int64 FieldEngineerFk { get; set; }
        [DisplayName("Report Engineer")]
        public Int64 ReportEngineerFk { get; set; }
        [DisplayName("Work Status")]
        public Int64 WorkStatusFk { get; set; }
        [DisplayName("Report Status")]
        public Int64 ReportStatusFk { get; set; }
        [DisplayName("Completed Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime CompletedDate { get; set; }
        [DisplayName("Delivered Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DeliveredDate { get; set; }
        //[DisplayName("PAC Status")]
        //public Int64 PacStatusFk { get; set; }
        //[DisplayName("FAC Status")]
        //public Int64 FacStatusFk { get; set; }
        [DisplayName("Remarks")]
        [StringLength(200)]
        public string Remarks { get; set; }

        [NotMapped]
        public bool IsReportReassign { get; set; }
        [NotMapped]
        public bool IsWorkReassign { get; set; }
        [NotMapped]
        public Int64 ReportWorkRef { get; set; }
    }

    public class AcceptanceStatus : BaseModel
    {
        [DisplayName("Project")]
        [NotMapped]
        public Int64 ProjectFk { get; set; }
        [DisplayName("PO.")]
        public Int64 PoFk { get; set; }
        [DisplayName("Site Name")]
        public Int64 SiteFk { get; set; }  // Po Details Id....
        [NotMapped]
        [DisplayName("Client Name")]
        public string ClientName { get; set; }
        [NotMapped]
        public string Po { get; set; }
        //[DisplayName("PAC")]
        //public Int64 PacStatusFk { get; set; }
        //[DisplayName("FAC")]
        //public Int64 FacStatusFk { get; set; }
        [NotMapped]
        [DisplayName("Acceptance Status")]
        public List<string> Statuses { get; set; }
        [StringLength(200)]
        public string Remarks { get; set; }
    }
    public class ReportEngineerWorkStatus : BaseModel
    {
        [DisplayName("Project")]
        [NotMapped]
        public Int64 ProjectFk { get; set; }
        [DisplayName("PO")]
        public Int64 PoFk { get; set; }  // Po Master Id
        [DisplayName("Site")]
        public Int64 SiteFk { get; set; }  // Po Details Id
        public Int64 PlanningMasterFk { get; set; }
        public string ReportFile { get; set; }
        [DisplayName("Work Status")]
        public Int64 WorkStatusFk { get; set; }
        [DisplayName("Report Status")]
        public Int64 ReportStatusFk { get; set; }
        [DisplayName("Completed Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? CompletedDate { get; set; }
        [DisplayName("Delivered Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime DeliveredDate { get; set; }
        [NotMapped]
        public string SiteName { get; set; }
        public bool IsReportReassign { get; set; }
        public bool IsWorkReassign { get; set; }
        public bool IsDelivered { get; set; }
        public bool IsReadyForAcceptance { get; set; }
        
        public Int64 ReportWorkRef { get; set; }
        public string Remarks { get; set; }
        public ReportEngineerWorkStatus()
        {
            IsReportReassign = false;
            IsWorkReassign = false;
            IsDelivered = true;
            ReportWorkRef = 0;
            IsReadyForAcceptance = false;
        }
    }
    public class InvoiceMaster : BaseModel
    {
        [DisplayName("Inv. Status")]
        public Int64 InvoiceStatusFk { get; set; }
        [DisplayName("Proj. Payment Date")]
        public DateTime ProjectedPaymentDate { get; set; }
        [DisplayName("Inv. Sub. Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        [DataType(DataType.Date)]
        public DateTime InvoiceSubmissionDate { get; set; }
        public string Remarks { get; set; }

        [DisplayName("Inv. Number")]
        public string Cid { get; set; }
        [NotMapped]
        public decimal TotalAmount { get; set; }  // sum of unitprice and claim amount
        public bool IsPayment { get; set; }
        [DisplayName("Payment Recieve Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? PaymentRecieveDate { get; set; }
        public string BankDetails { get; set; }
        public decimal RecieveAmount { get; set; }
    }
    public class InvoiceDetails : BaseModel
    {
        [DisplayName("PO.")]
        public Int64 PoFk { get; set; }
        public Int64 InvoiceMasterFk { get; set; }
        [NotMapped]
        [DisplayName("Payment Milestone")]
        public List<string> Milestones { get; set; }
        //public Int64 PaymentMilestoneFk { get; set; }
        [DisplayName("Site Name")]
        public Int64 SiteFk { get; set; }  // Po Details Id....
        [DisplayName("Projected Invoice Amount")]
        public decimal SiteAmount { get; set; }
        public decimal Tax { get; set; }
        public decimal Vat { get; set; }
        [DisplayName("Other Claim Amount")]
        public decimal OtherClaimAmount { get; set; }
        [DisplayName("Total Amount")]
        [NotMapped]
        public decimal TotalAmount { get; set; }
        [DisplayName("Final Amount")]
        public decimal FinalAmount { get; set; }
        public string Remarks { get; set; }
    }
    public class InvoiceMilestone:BaseModel
    {
        public Int64 InvoiceDetailsFk { get; set; }
        public Int64 PayementMilestoneFk { get; set; }
    }
}