﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ONSJobTracker.ViewModels.Purchase_Order;
using ONSJobTracker.ViewModels.Work_Plan;

namespace ONSJobTracker.Models
{
    public class Planning:BaseModel
    {
        [NotMapped]
        [DisplayName("Project Name")]
        public Int64 ProjectFk { get; set; }
        [NotMapped]
        [DisplayName("PO No")]
        public Int64 PoMasterFk { get; set; }
        [DisplayName("Site/Task")]
        public Int64 PoDetailsFk { get; set; }
        public Int64 PlanningMasterFk { get; set; }

        [DisplayName("Engineer Name")]
        public Int64 EmployeeInfoFk { get; set; }
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        [DisplayName("Finish Date")]
        public DateTime FinishDate { get; set; }
        [DisplayName("Outside T/A")]
        public bool OutsideTa { get; set; }
        [DisplayName("Outside D/A")]
        public bool OutsideDa { get; set; }

        [DisplayName("T/A")]
        public bool Ta { get; set; }
        [DisplayName("D/A")]
        public bool Da { get; set; }
        public decimal TaAmount { get; set; }
        public decimal DaAmount { get; set; }
        public int Duration { get; set; }
        [MaxLength(200)]
        public string Remarks { get; set; }
        [DisplayName("Work Type")]
        public int Type { get; set; }
        [NotMapped]
        [DisplayName("Resource type")]
        public string ResourceType  { get; set; }
        [NotMapped]
        public Transport Transport { get; set; }
        [NotMapped]
        public OtherPlanCost OtherPlanCost { get; set; }
        [NotMapped]
        public List<string> SiteList { get; set; }
    }

    public class PlanningMaster:BaseModel
    {
        public Int64 PoDetailsFk { get; set; }
        public string PlanReference { get; set; }
        public bool AdminApproval { get; set; }
        public bool AccountsApproval { get; set; }
        public int TeamLeaderStatus { get; set; }
        public PlanningMaster()
        {
            TeamLeaderStatus = 1;
        }
    }

    public class Transport:BaseModel
    {
        [DisplayName("Plan Reference")]
        public Int64 PlanningMasterFk { get; set; }
        [DisplayName("Transport Reference")]
        public string TransportCid { get; set; }
        [DisplayName("Estimated Duration")]
        public int EstimatedDuration { get; set; }

        [DisplayName("Per Day Cost")]
        public decimal DailyCost { get; set; }
        [DisplayName("Car Info")]
        public int CarInfo { get; set; }
        [DisplayName("Fuel Type")]
        public string FuelType { get; set; }
        [DisplayName("Estimated KM")]
        public string EstimatedKm { get; set; }
        [DisplayName("Total Cost")]
        public string TotalCarCost { get; set; }
        [DisplayName("DA Amount")]
        public string Daamount { get; set; }
        public string Remarks { get; set; }
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }
        [DisplayName("Finish Date")]
        public DateTime FinishDate { get; set; }

        [NotMapped]
        [DisplayName("Vendor")]
        public Int64 VendorFk { get; set; }
        [DisplayName("Car No")]
        public Int64 CarFk { get; set; }
    }

    public class WorkUpdate:BaseModel
    {
        [DisplayName("Plan Reference")]
        public Int64 PlanningMasterFk { get; set; }
        [DisplayName("Project")]
        [NotMapped]
        public Int64 ProjectFk { get; set; }
        [NotMapped]
        [DisplayName("PO")]
        public Int64 PoFk { get; set; }
        [DisplayName("Site")]
        public Int64 SiteFk { get; set; } // Po Details Id.....

        [NotMapped]
        public string SiteName { get; set; } 
        [DisplayName("Driver Name")]
        public string Drivername { get; set; }
        [DisplayName("Driver Contact")]
        public string DriverContact { get; set; }
        [DisplayName("Car Reg. No.")]
        public string CarRegNo { get; set; }
        [DisplayName("Start Mileage")]
        public int? StartMileage { get; set; }
        [DisplayName("End Mileage")]
        public int? EndMileage { get; set; }
        [DisplayName("Work Status")]
        public Int64 WorkStatusFk { get; set; }
        public string Problem { get; set; }
        public string Resolve { get; set; }
        [DisplayName("Start Time")]
        public string StartTime { get; set; }
        [DisplayName("End Time")]
        public string EndTime { get; set; }
        public string Location { get; set; }
        public string FileLocation { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime Date { get; set; }
        public string Remarks { get; set; }
    }

    public class WorkTeamMember:BaseModel
    {
        public Int64 WorkUpdateFk { get; set; }
        [DisplayName("Engineer Name")]
        public Int64 EmployeeFk { get; set; }

        public string Remarks { get; set; }
        [NotMapped]
        public string Name { get; set; }
    }

    public class WorkTeamMemberViewModel
    {
        public WorkTeamMember WorkTeamMember { get; set; }
        public EmployeeInfo EmployeeInfo { get; set; }
        public DailyCost DailyCost { get; set; }
        public List<DailyCost> DailyCostList { get; set; }
    }

    public class PlanApproval
    {
        public List<ApprovalDetails> ApprovalDetails { get; set; }
        public List<TransportModelForPayment> TransportList { get; set; }
        public List<OtherCostPayment> OtherPlanCostList { get; set; }
        public List<ApprovalMaster> ApprovalMaster { get; set; }
        public List<PoDetailsViewModel> PoDetailsView { get; set; }
        public Transport Transport { get; set; }
        public OtherPlanCost OtherPlanCost { get; set; }
        public Payment TaDaAmount { get; set; }
        public Planning Planning { get; set; }
        
    }

    public class ApprovalDetails
    {
        public EmployeeInfo EmployeeInfo { get; set; }
        public EmployeeDesignation EmployeeDesignation { get; set; }
        public Planning Planning { get; set; }
        public List<TaDaAmountViewModel> TaDaAmounts { get; set; }
      
    }

    public class ApprovalMaster
    {
        public PoMaster PoMaster { get; set; }
        public string ClientName { get; set; }
        public string EndClientName { get; set; }      
        public PlanningMaster PlanningMaster { get; set; }
        public string PoNo { get; set; }
        public string Projectname { get; set; }
        public Int64 PoDetaisFk { get; set; }
        public Int64 PlanningMasterId { get; set; }
        public bool PlanApproval { get; set; }
    }

    public class OtherPlanCost:BaseModel
    {
        public Int64 PlanningMasterFk { get; set; }
        [DisplayName("Resource Title")]
        public string CostName { get; set; }
        public decimal Amount { get; set; }
        public string Remarks { get; set; }
    }

    public class OtherCostPayment
    {
        public OtherPlanCost OtherPlanCost { get; set; }
        public List<TaDaAmountViewModel> OtherPlanCostPayment { get; set; }
    }
    //Accounts
    public class Payment:BaseModel
    {
        public Int64 PlanningMasterFk { get; set; }
        [DisplayName("Receive By ")]
        public Int64 EmployeeInfoFk { get; set; }
        [DisplayName("Payment Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime  PaymentDate { get; set; }
        [DisplayName("Paid Amount")]
        public decimal PaidAmount { get; set; }
        [DisplayName("Note")]
        public string Remarks { get; set; }
        public int CostType { get; set; }
    }

    public class DailyCost : BaseModel
    {
        public Int64 WorkUpdateFk { get; set; }
        public Int64 EmployeeFk { get; set; }

        [DisplayName("Cost Title")]
        public string CostTitle { get; set; }
        [DisplayName("Cost Details")]
        public string CostDetails { get; set; }
        [DisplayName("Amount")]
        public decimal Amount { get; set; }
    }
}