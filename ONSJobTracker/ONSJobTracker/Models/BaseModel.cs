﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web;
using ONSJobTracker.CustomizedFunction;

namespace ONSJobTracker.Models
{
    public class BaseModel
    {
        [Key]
        public Int64 Id { get; set; }
        public Int64? InsertBy { get; set; }
        public DateTime? InsertDate { get; set; }
        public Int64? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        [DisplayName("Status")]
        public bool Status { get; set; }
        [NotMapped]
        public String Action { get; set; }
        public void AddReady(bool status, DateTime date)
        {
            InsertBy = UserId();
            Status = status;
            InsertDate = GlobalFunction.Timezone(date);
        }
        public void UpdateReady(bool status, DateTime date)
        {
            Status = status;
            UpdateBy = UserId();
            UpdateDate = GlobalFunction.Timezone(date);
        }

        public Int64 UserId()
        {
            string id = HttpContext.Current.Request.Cookies["UserInfo"]?["UserId"];
            if (id != null)
            {
                Int64 userid = Convert.ToInt64(EncryptDecrypt.Decrypt(id));
                return userid;
            }
            return 0; //Default 
        }
    }
}