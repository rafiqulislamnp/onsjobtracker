﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ONSJobTracker.CustomizedFunction;

namespace ONSJobTracker.Models
{
    public class xOssContext : DbContext
    {
        //-----------------------------------------Ons Job Tracker
        public xOssContext()
            : base("name=xOssContext")
        {
            Database.SetInitializer<xOssContext>(new CreateDatabaseIfNotExists<xOssContext>());
        }

        public DbSet<User> User { get; set; }
        public DbSet<EmployeeInfo> EmployeeInfo { get; set; }
        public DbSet<EmployeeDesignation> EmployeeDesignation { get; set; }
        public DbSet<EmployeeDepartment> EmployeeDepartment { get; set; }
        public DbSet<WorkType> WorkType { get; set; }
        public DbSet<ProjectType> ProjectType { get; set; }
        public DbSet<ProjectStatus> ProjectStatus { get; set; }
        public DbSet<Cluster> Cluster { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<District> District { get; set; }
        public DbSet<WorkStatus> WorkStatus { get; set; }
        public DbSet<ReportStatus> ReportStatus { get; set; }
        public DbSet<PacStatus> PacStatus { get; set; }
        public DbSet<FacStatus> FacStatus { get; set; }
        public DbSet<WorkStation> WorkStation { get; set; }
        public DbSet<Project> Project { get; set; }
        public DbSet<PoMaster> PoMaster { get; set; }
        public DbSet<PoDetails> PoDetails { get; set; }
        public DbSet<InvoiceStatus> InvoiceStatus { get; set; }
        public DbSet<PayementMilestone> PayementMilestone { get; set; }
        public DbSet<ClientType> ClientType { get; set; }
        public DbSet<ClientInfo> ClientInfo { get; set; }
        public DbSet<DeleteDataLog> DeleteDataLog { get; set; }
        public DbSet<MenuMaster> MenuMaster { get; set; }
        public DbSet<MenuDetails> MenuDetails { get; set; }
        public DbSet<MenuRole> MenuRole { get; set; }
        public DbSet<Planning> Planning { get; set; }
        public DbSet<PlanningMaster> PlanningMaster { get; set; }
        public DbSet<Transport> Transport { get; set; }
        public DbSet<OtherPlanCost> OtherPlanCost { get; set; }
        public DbSet<WorkUpdate> WorkUpdate { get; set; }
        public DbSet<WorkTeamMember> WorkTeamMember { get; set; }
        public DbSet<DailyCost> DailyCost { get; set; }
        public DbSet<SiteMilestone> SiteMilestone { get; set; }
        public DbSet<Payment> Payment { get; set; }


        //Statuses
        public DbSet<WorkDeliveryStatus> WorkDeliveryStatus { get; set; }
        public DbSet<ReportEngineerWorkStatus> ReportEngineerWorkStatus { get; set; }
        public DbSet<AcceptanceStatus> AcceptanceStatus { get; set; }
        public DbSet<InvoiceMaster> InvoiceMaster { get; set; }
        public DbSet<InvoiceDetails> InvoiceDetail { get; set; }
        public DbSet<AccetanceMilestone> AccetanceMilestone { get; set; }
        public DbSet<InvoiceMilestone> InvoiceMilestone { get; set; }

        public DbSet<VendorInfo> VendorInfo { get; set; }
        public DbSet<CarInfo> CarInfo { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            SetDecimalPrecisions(modelBuilder);
            SetForeignKeys(modelBuilder);
        }

        private void SetDecimalPrecisions(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Entity<Mkt_POSlave>().Property(x => x.Consumption).HasPrecision(18, 5);
        }

        //public System.Data.Entity.DbSet<Oss.Romo.Models.One> One { get; set; }

        private void SetForeignKeys(DbModelBuilder modelBuilder)
        {
           
        }
    }
}


