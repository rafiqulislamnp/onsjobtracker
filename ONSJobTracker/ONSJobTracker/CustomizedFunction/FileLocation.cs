﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ONSJobTracker.CustomizedFunction
{
    public static class FileLocation
    {
        public static string LogFile = "/OnsJobTracker/Content/Log_File/";
        //public static string LogFile = "/Content/Log_File/";
        public static int GetRandomNumber()
        {
            Random rnd = new Random();
            int month = rnd.Next(1, 101);
            return month;
        }
    }
    
}