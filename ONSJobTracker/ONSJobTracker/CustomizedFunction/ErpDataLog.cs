﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ONSJobTracker.Models;

namespace ONSJobTracker.CustomizedFunction
{
    [Table("DeleteDataLog")]
    public class DeleteDataLog : BaseModel
    {
        public String LogName { get; set; }
        public String TableName { get; set; }
        public String Logdetails { get; set; }
    }

    public static class SaveLog
    {
        public static void Save(xOssContext db, string details, string tableName, string logName)
        {
            //   Save Log Data Start
            try
            {
                DeleteDataLog erpDataLog = new DeleteDataLog
                {
                    LogName = logName,
                    TableName = tableName,
                    Logdetails = details
                };
                erpDataLog.AddReady(true, DateTime.Now);
                db.DeleteDataLog.Add(erpDataLog);
            }
            catch (Exception e)
            {
                //
            }
            //   Save Log Data End
        }
    }
}