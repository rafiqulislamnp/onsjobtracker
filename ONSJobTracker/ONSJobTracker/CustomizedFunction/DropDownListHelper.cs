﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ONSJobTracker.Models;

namespace ONSJobTracker.CustomizedFunction
{
    public class DropDownListHelper
    {
        //public static IList<SelectListItem> GetGiftTypeItems()
        //{
        //    IList<SelectListItem> result = new List<SelectListItem>();
        //    result.Add(new SelectListItem { Value = "SOLD", Text = "General" });
        //    result.Add(new SelectListItem { Value = "GIFT", Text = "Sample" });
        //    return result;
        //}

        public static IList<SelectListItem> GetEmployeeDesignation()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var designationlist = db.EmployeeDesignation.ToList();
                foreach (var designation in designationlist)
                {
                    result.Add(new SelectListItem { Value = designation.Id.ToString(), Text = designation.Name });
                }
            }
            return result;
        }
        public static IList<SelectListItem> GetEmployeeDepartment()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var departmentlist = db.EmployeeDepartment.ToList();
                foreach (var department in departmentlist)
                {
                    result.Add(new SelectListItem { Value = department.Id.ToString(), Text = department.Name });
                }
            }
            return result;
        }



        // District List Dropdown------ 
        public static List<object> GetDistrictList()
        {
            var List = new List<object>();
            using (var db = new xOssContext())
            {
                foreach (var list in db.District.Where(d => d.Status == true))
                {
                    List.Add(new { Text = list.Name, Value = list.Id });
                }
                return List;
            }
        }

        // Client type Dropdown
        public static List<object> GetClientTypeList()
        {
            var List = new List<object>();
            using (var db = new xOssContext())
            {
                foreach (var list in db.ClientType.Where(d => d.Status == true))
                {
                    List.Add(new { Text = list.Type, Value = list.Id });
                }
                return List;
            }
        }
        public static IList<SelectListItem> GetUserTypes()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "Admin", Text = "Admin" });
            result.Add(new SelectListItem { Value = "User Admin", Text = "User Admin" });
            result.Add(new SelectListItem { Value = "User", Text = "User" });
            return result;
        }
        public static IList<SelectListItem> GetEmployees()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var employeelist = from t1 in db.EmployeeInfo
                                   join t2 in db.EmployeeDesignation on t1.EmployeeDesignationFk equals t2.Id
                                   where t2.Name.Contains("Engineer") || t2.Name.Contains("Technician")
                                   select new
                                   {
                                       Id = t1.Id,
                                       Name = t1.Name,
                                       Designation = t2.Name
                                   };
                foreach (var employee in employeelist)
                {
                    result.Add(new SelectListItem { Value = employee.Id.ToString(), Text = employee.Name + " - " + employee.Designation });
                }
            }
            return result;
        }

        //Load Report Engineer from Planning 
        //public static IList<SelectListItem> GetReportEng()
        //{
        //    IList<SelectListItem> result = new List<SelectListItem>();
        //    using (var db = new xOssContext())
        //    {
        //        var employeelist = from t1 in db.PlanningMaster
        //            join t2 in db.Planning on t1.Id equals t2.PlanningMasterFk
        //            join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id
        //            join t4 in db.EmployeeDesignation on t3.EmployeeDesignationFk equals t4.Id
        //                           where t2.Type == 2 and 
        //            select new
        //                           {
        //                Id = t3.Id,
        //                Name = t3.Name,
        //                Designation = t4.Name
        //            };
        //        foreach (var employee in employeelist)
        //        {
        //            result.Add(new SelectListItem { Value = employee.Id.ToString(), Text = employee.Name + " - " + employee.Designation });
        //        }
        //    }
        //    return result;
        //}


        public static IList<SelectListItem> GetMasterMenu()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var menulist = db.MenuMaster.OrderBy(x => x.ModuleSl).ToList();
                foreach (var menu in menulist)
                {
                    result.Add(new SelectListItem { Value = menu.Id.ToString(), Text = menu.ModuleName });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetMenuTypes()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "Form", Text = "Form" });
            result.Add(new SelectListItem { Value = "Report", Text = "Report" });
            return result;
        }

        public static IList<SelectListItem> GetUsers()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var userlist = (from t1 in db.User
                                join t2 in db.EmployeeInfo on t1.EmployeeInfoFk equals t2.Id
                                select new
                                {
                                    value = t1.Id,
                                    text = t2.Name + " || " + t2.CId
                                });
                foreach (var user in userlist)
                {
                    result.Add(new SelectListItem { Value = user.value.ToString(), Text = user.text });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetClientInfo(int typeid)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var clientlist = db.ClientInfo.Where(x => x.ClientTypeFk == typeid).ToList();
                foreach (var client in clientlist)
                {
                    result.Add(new SelectListItem { Value = client.Id.ToString(), Text = client.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetProjectType()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.ProjectType.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetPoMaster()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.PoMaster.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.PoNo + ":" + type.ProjectCid });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetWorkType()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.WorkType.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetCluster()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.Cluster.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetRegion()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.Region.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetWorkStation()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.WorkStation.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetProjects()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.Project.Where(x => x.Status).ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.ProjectName });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetSites()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = (from t1 in db.WorkUpdate
                                join t2 in db.PlanningMaster on t1.PlanningMasterFk equals t2.Id
                                join t3 in db.PoDetails on t2.PoDetailsFk equals t3.Id
                                select new
                                {
                                    Id = t3.Id,
                                    Name = t3.Name,
                                    Cid = t3.Cid
                                }).Distinct();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Cid + "-" + type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetWorkStatus()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.WorkStatus.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetReportStatus()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.ReportStatus.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetPacStatus()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.PacStatus.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetFacStatus()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.FacStatus.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetClientInfo()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.ClientInfo.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetInvoiceStatus()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.InvoiceStatus.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.Name });
                }
            }
            return result;
        }
        public static IList<SelectListItem> GetEngineer(int eid)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var engineerlist = db.EmployeeInfo.Where(x => x.EmployeeDepartmentFk == eid).ToList();
                foreach (var engineer in engineerlist)
                {
                    result.Add(new SelectListItem { Value = engineer.Id.ToString(), Text = engineer.Name + " || " + engineer.CId });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetPoes()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = db.PoMaster.ToList();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.Id.ToString(), Text = type.PoNo });
                }
            }
            return result;
        }


        public static IList<SelectListItem> GetPlans()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var planlist = db.PlanningMaster.ToList();
                foreach (var plan in planlist)
                {
                    result.Add(new SelectListItem { Value = plan.Id.ToString(), Text = plan.PlanReference });
                }
            }
            return result;
        }
        public static IList<SelectListItem> GetCar()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "1", Text = "Own Car" });
            result.Add(new SelectListItem { Value = "2", Text = "Instant Basis" });
            return result;
        }
        public static IList<SelectListItem> GetfuelType()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "1", Text = "Gas" });
            result.Add(new SelectListItem { Value = "2", Text = "Oil" });
            result.Add(new SelectListItem { Value = "3", Text = "Gas + Oil" });
            return result;
        }
        public static IList<SelectListItem> GetPlanEngineerType()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "1", Text = "Field" });
            result.Add(new SelectListItem { Value = "2", Text = "Report" });
            return result;
        }
        public static IList<SelectListItem> GetSitesForAcceptance()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var typelist = (from t1 in db.WorkDeliveryStatus
                                join t2 in db.PoDetails on t1.SiteFk equals t2.Id
                                select new
                                {
                                    value = t2.Id,
                                    text = t2.Name + " || " + t2.Cid
                                }).Distinct();
                foreach (var type in typelist)
                {
                    result.Add(new SelectListItem { Value = type.value.ToString(), Text = type.text });
                }
            }
            return result;
        }

        //Milestone dropdown

        public static List<object> GetPaymentMilstoneList()
        {
            var List = new List<object>();
            using (var db = new xOssContext())
            {
                foreach (var list in db.PayementMilestone.Where(d => d.Status == true))
                {
                    List.Add(new { Text = list.Name, Value = list.Id });
                }
                return List;
            }
        }
        public static IList<SelectListItem> GetResourceType()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "1", Text = "Engineer" });
            result.Add(new SelectListItem { Value = "2", Text = "Own Car" });
            result.Add(new SelectListItem { Value = "3", Text = "Hired Car" });
            result.Add(new SelectListItem { Value = "4", Text = "Other" });
            return result;
        }

        public static IList<SelectListItem> GetPoMasterForInvoice()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var list = (from t1 in db.AcceptanceStatus
                            join t2 in db.PoMaster on t1.PoFk equals t2.Id
                            select new
                            {
                                value = t2.Id,
                                text = t2.PoNo
                            }).Distinct();
                foreach (var po in list)
                {
                    result.Add(new SelectListItem { Value = po.value.ToString(), Text = po.text });
                }
            }
            return result;
        }
        public static IList<SelectListItem> GetCarType()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "1", Text = "Own Car" });
            result.Add(new SelectListItem { Value = "2", Text = "Hired Car" });
            return result;
        }
        public static IList<SelectListItem> GetEngineerByPlan(int pid)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var engineerlist = (from t1 in db.Planning
                                    join t2 in db.EmployeeInfo on t1.EmployeeInfoFk equals t2.Id
                                    where t1.PlanningMasterFk == pid
                                    select new
                                    {
                                        value = t2.Id,
                                        text = t2.Name + " || " + t2.CId
                                    });
                foreach (var engineer in engineerlist)
                {
                    result.Add(new SelectListItem { Value = engineer.value.ToString(), Text = engineer.text });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetAllEmployee()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var employeelist = db.EmployeeInfo.ToList();
                foreach (var employee in employeelist)
                {
                    result.Add(new SelectListItem { Value = employee.Id.ToString(), Text = employee.CId + "||" + employee.Name });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetProjectsOfAssignedEmployee(int id)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var projectlist = (from t1 in db.Planning
                                   join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                                   join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                                   join t4 in db.Project on t3.ProjectFk equals t4.Id
                                   join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                                   join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                                   where t5.Id == id
                                   select new
                                   {
                                       value = t4.Id,
                                       text = t4.ProjectName + " (" + t6.Name + ")"
                                   }).Distinct();
                foreach (var project in projectlist)
                {
                    result.Add(new SelectListItem { Value = project.value.ToString(), Text = project.text });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetProjectsOfAssignedReportEngineer(int id)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var projectlist = (from t1 in db.Planning
                                   join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                                   join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                                   join t4 in db.Project on t3.ProjectFk equals t4.Id
                                   join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                                   join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                                   where t5.Id == id && t1.Type == 2
                                   select new
                                   {
                                       value = t4.Id,
                                       text = t4.ProjectName + " (" + t6.Name + ")"
                                   }).Distinct();
                foreach (var project in projectlist)
                {
                    result.Add(new SelectListItem { Value = project.value.ToString(), Text = project.text });
                }
            }
            return result;
        }
        public static IList<SelectListItem> GetPoByProjectIdOfAssignedEmployee(Int64 pid)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var polist = (from t1 in db.Planning
                              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                              join t4 in db.Project on t3.ProjectFk equals t4.Id
                              join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                              join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                              where t4.Id == pid && t5.Id == employeeId
                              select new
                              {
                                  value = t3.Id,
                                  text = t3.PoNo
                              }).Distinct().ToList();
                foreach (var project in polist)
                {
                    result.Add(new SelectListItem { Value = project.value.ToString(), Text = project.text });
                }

            }
            return result;
        }

        public static IList<SelectListItem> GetSitePoIdOfAssignedEmployee(Int64 poid)
        {
            int employeeId = Convert.ToInt32(EncryptDecrypt.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserInfo"]?["EmployeeId"]));
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var polist = (from t1 in db.Planning
                              join t2 in db.PoDetails on t1.PoDetailsFk equals t2.Id
                              join t3 in db.PoMaster on t2.PoMasterFk equals t3.Id
                              join t4 in db.Project on t3.ProjectFk equals t4.Id
                              join t5 in db.EmployeeInfo on t1.EmployeeInfoFk equals t5.Id
                              join t6 in db.ClientInfo on t4.ClientInfoFk equals t6.Id
                              where t3.Id == poid && t5.Id == employeeId
                              select new
                              {
                                  value = t2.Id,
                                  text = t2.Name
                              }).Distinct().ToList();
                foreach (var project in polist)
                {
                    result.Add(new SelectListItem { Value = project.value.ToString(), Text = project.text });
                }
            }
            return result;
        }
        public static IList<SelectListItem> GetProjectsForProjectManager(int empid)
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            using (var db = new xOssContext())
            {
                var projects = (from t1 in db.ReportEngineerWorkStatus
                                join t5 in db.PoDetails on t1.SiteFk equals t5.Id
                                join t6 in db.PoMaster on t5.PoMasterFk equals t6.Id
                                join t2 in db.Project on t6.ProjectFk equals t2.Id
                                join t3 in db.EmployeeInfo on t2.EmployeeInfoFk equals t3.Id
                                join t4 in db.ClientInfo on t2.ClientInfoFk equals t4.Id
                                where t3.Id == empid && t1.IsReadyForAcceptance
                                select new
                                {
                                    value = t2.Id,
                                    text = t2.ProjectName + "(" + t4.Name + ")"
                                }).Distinct().ToList();
                foreach (var project in projects)
                {
                    result.Add(new SelectListItem { Value = project.value.ToString(), Text = project.text });
                }
            }
            return result;
        }

        public static IList<SelectListItem> GetCurrentWorkStatus()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "1", Text = "Pending" });
            result.Add(new SelectListItem { Value = "2", Text = "In Progress" });
            result.Add(new SelectListItem { Value = "3", Text = "Finished" });
            return result;
        }
        public static IList<SelectListItem> GetTeamleaderStatus()
        {
            IList<SelectListItem> result = new List<SelectListItem>();
            result.Add(new SelectListItem { Value = "1", Text = "In Progress" });
            result.Add(new SelectListItem { Value = "2", Text = "Done" });
            return result;
        }
        public static List<object> GetVendorList()
        {
            var List = new List<object>();
            using (var db = new xOssContext())
            {
                foreach (var list in db.VendorInfo.Where(d => d.Status == true))
                {
                    List.Add(new { Text = list.Name, Value = list.Id });
                }
                return List;
            }
        }
    }
}