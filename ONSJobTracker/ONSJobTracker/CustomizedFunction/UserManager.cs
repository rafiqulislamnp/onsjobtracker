﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using ONSJobTracker.Models;
using ONSJobTracker.ViewModels;

namespace ONSJobTracker.CustomizedFunction
{
    public class SessionCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Request.Cookies["UserInfo"]?["UserId"] == null &&
                string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["UserInfo"]?["UserId"]))
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                        {{"controller", "Account"}, {"action", "Login"}});
            }
            else
            {
                UserManager manager=new UserManager();
                HttpSessionStateBase session = filterContext.HttpContext.Session;
                if (session["MenuMaster"] == null)
                {
                    manager.GetUserMenu();
                }
                string actionName = filterContext.ActionDescriptor.ActionName.ToLower();
                string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();
                //var parameters = filterContext.ActionDescriptor.GetParameters();
                var menuMaster = (List<MenuModels>)HttpContext.Current.Session["MenuMaster"];
                bool authorized =
                    menuMaster.Exists(x => x.Controller.ToLower() == controllerName && x.ActionResult.ToLower() == actionName);
                if (!authorized)
                {
                    filterContext.Result = new RedirectToRouteResult(
                           new RouteValueDictionary
                               {{"controller", "Account"}, {"action", "AccessDenied"}});

                }
            }
            base.OnActionExecuting(filterContext);
        }  
    }
    public class UserManager
    {
        public LoginUser IsValid(string loginid, string password)
        {
            
            using (var db = new xOssContext()) // use your DbConext
            {
                //string a = EncryptDecrypt.Decrypt("3pEI5X2ifVe8P8XaDQMqIg%3d%3d");
                string pass = EncryptDecrypt.Encrypt(password);
                var user = (from t1 in db.User
                    join t2 in db.EmployeeInfo on t1.EmployeeInfoFk equals t2.Id
                    join t3 in db.EmployeeDesignation on t2.EmployeeDesignationFk equals t3.Id
                    where t1.LoginId == loginid && t1.Password ==pass && t1.Status
                    select new LoginUser
                    {
                        UserId = t1.Id,
                        EmployeeId = t2.Id,
                        UserName = t2.Name,                       
                        UserImage = t2.ImageLink,
                        UserEmail = t2.Email,
                        UserDesignation = t3.Name,
                        DesignationId=t3.Id
                    }).FirstOrDefault();
                return user;
            }
        }
        public void GetUserMenu()
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                string id = HttpContext.Current.Request.Cookies["UserInfo"]?["UserId"];
                if (id != null)
                {
                    Int64 userid =Convert.ToInt64(EncryptDecrypt.Decrypt(id));                    
                    var menus = (
                        from t1 in db.MenuMaster
                        join t2 in db.MenuDetails on t1.Id equals t2.MenuMasterFk
                        join t3 in db.MenuRole on new { UserFk = userid, t2.MenuMasterFk, MenuDetailsFk = t2.Id }
                            equals new { t3.UserFk, t3.MenuMasterFk, t3.MenuDetailsFk }
                        where t3.Status orderby t1.ModuleSl
                        select new MenuModels
                        {
                            ModuleId = t1.Id,
                            ModuleName = t1.ModuleName,
                            MenuId = t2.Id,
                            MenuName = t2.MenuName,
                            MenuSl = t2.MenuSl,
                            ActionResult = t2.ActionName,
                            Controller = t2.ControllerName,
                            IsMenu = t2.IsMenu

                        }).ToList(); //Get the Menu details from entity and bind it in MenuModels list.  FormsAuthentication.SetAuthCookie(_loginCredentials.UserName, false); // set the formauthentication cookie  

                    HttpContext.Current.Session["MenuMaster"] = menus; //Bind the _menus list to MenuMaster session  
                }
            }
        }
        public void AssignMenuForNewUser(Int64 userId)
        {
            
            using (var db = new xOssContext()) // use your DbConext
            {
                var menus = db.MenuDetails.ToList();
                foreach (var menu in menus)
                {
                    MenuRole role = new MenuRole();

                    role.UserFk = userId;
                    role.MenuMasterFk = menu.MenuMasterFk;
                    role.MenuDetailsFk = menu.Id;

                    role.AddReady(false, DateTime.Now);
                    db.MenuRole.Add(role);
                    db.SaveChanges();
                }

            }
        }

        public void DeleteMenuForNewUser(Int64 userId)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                db.MenuRole.RemoveRange(db.MenuRole.Where(m => m.UserFk == userId));
                db.SaveChanges();
            }
        }
        public void AssignRoleForMenuDetails(Int64 detailsmenuid, Int64 mastermenuid)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                var users = db.User.Select(x => x.Id).ToList();
                foreach (var user in users)
                {
                    MenuRole model = new MenuRole
                    {
                        UserFk = user,
                        MenuDetailsFk = detailsmenuid,
                        MenuMasterFk = mastermenuid
                    };
                    model.AddReady(false, DateTime.Now);
                    db.MenuRole.Add(model);
                    db.SaveChanges();
                }
            }
        }

        public void DeleteAssignedRoleForMenuDetails(Int64 detailsmenuid)
        {
            using (var db = new xOssContext()) // use your DbConext
            {
                db.MenuRole.RemoveRange(db.MenuRole.Where(x => x.MenuDetailsFk == detailsmenuid));
                db.SaveChanges();
            }
        }
    }

    
}