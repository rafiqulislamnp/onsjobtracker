﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ONSJobTracker.CustomizedFunction
{
    public static class GlobalFunction
    {
        public static DateTime Timezone(DateTime datetime)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central Asia Standard Time");
            DateTime printDate = TimeZoneInfo.ConvertTime(datetime, timeZoneInfo);
            return printDate;
        }
        public static DateTime Timezone()
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central Asia Standard Time");
            DateTime printDate = TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);
            return printDate;
        }
        public static int DateToInt(DateTime dateTime)
        {
            var date = dateTime.ToString("ddMMyyyy");
            int dateint = Convert.ToInt32(date);
            return dateint;
        }
    }
}